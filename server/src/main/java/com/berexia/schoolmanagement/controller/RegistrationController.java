package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Registration;
import com.berexia.schoolmanagement.service.RegistrationServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class RegistrationController {

    @Autowired
    private RegistrationServiceI registrationService;

    @GetMapping("/registrations")
    public List<Registration> getRegistrations() {
        return this.registrationService.getRegistrations();
    }

    @PostMapping("/registrations")
    public Registration addRegistration(@RequestBody Registration newRegistration) {
        return this.registrationService.addRegistration(newRegistration);
    }

    @PutMapping(path = "/registrations")
    public Registration editRegistration(@Valid @RequestBody Registration newRegistration) {
        return this.registrationService.editRegistration(newRegistration);
    }

    @GetMapping(path = "/registrations/{id}")
    public Optional<Registration> getByIdRegistration(@PathVariable(value = "id") Integer id) {
        return this.registrationService.getByIdRegistration(id);
    }

    @DeleteMapping(path = "/registrations/{id}")
    public void deleteRegistration(@PathVariable(value = "id") Integer id) {
        this.registrationService.deleteRegistration(id);
    }

}
