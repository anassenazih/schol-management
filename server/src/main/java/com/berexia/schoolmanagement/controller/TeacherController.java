package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Teacher;
import com.berexia.schoolmanagement.service.TeacherServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class TeacherController {

    @Autowired
    private TeacherServiceI teacherService;

    @GetMapping("/teachers")
    public List<Teacher> getTeachers() {
        return this.teacherService.getTeachers();
    }

    @PostMapping("/teachers")
    public Teacher addTeacher(@RequestBody Teacher newTeacher) {
        return this.teacherService.addTeacher(newTeacher);
    }

    @PutMapping(path = "/teachers")
    public Teacher editTeacher(@Valid @RequestBody Teacher newTeacher) {
        return this.teacherService.editTeacher(newTeacher);
    }

    @GetMapping(path = "/teachers/{id}")
    public Optional<Teacher> getByIdTeacher(@PathVariable(value = "id") Integer id) {
        return this.teacherService.getByIdTeacher(id);
    }

    @DeleteMapping(path = "/teachers/{id}")
    public void deleteTeacher(@PathVariable(value = "id") Integer id) {
        this.teacherService.deleteTeacher(id);
    }

}
