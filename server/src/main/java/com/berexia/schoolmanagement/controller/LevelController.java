package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Level;
import com.berexia.schoolmanagement.service.LevelServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@CrossOrigin("*")
@RestController
public class LevelController {

    @Autowired
    private LevelServiceI levelService;

    @GetMapping("/levels")
    public List<Level> getLevels() {
        return this.levelService.getLevels();
    }

    @PostMapping("/levels")
    public Level addLevel(@RequestBody Level newLevel) {
        return this.levelService.addLevel(newLevel);
    }

    @PutMapping(path = "/levels")
    public Level editLevel(@Valid @RequestBody Level newLevel) {
        return this.levelService.editLevel(newLevel);
    }

    @GetMapping(path = "/levels/{id}")
    public Optional<Level> getByIdLevel(@PathVariable(value = "id") Integer id) {
        return this.levelService.getByIdLevel(id);
    }

    @DeleteMapping(path = "/levels/{id}")
    public void deleteLevel(@PathVariable(value = "id") Integer id) {
        this.levelService.deleteLevel(id);
    }

}
