package com.berexia.schoolmanagement.controller;

import com.berexia.schoolmanagement.dto.QuestionRequestDTO;
import com.berexia.schoolmanagement.model.*;
import com.berexia.schoolmanagement.service.CourseServiceI;
import com.berexia.schoolmanagement.service.RegistrationServiceI;
import com.berexia.schoolmanagement.service.StudentServiceI;
import com.berexia.schoolmanagement.service.UserServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
//
@CrossOrigin("*")
@RestController
public class CourseController {
    @Autowired
    private CourseServiceI courseService;
    @Autowired
    private RegistrationServiceI registrationService;


    @GetMapping("/courses")
    //@PreAuthorize("hasRole('USER') or hasRole('STUDENT') or hasRole('ADMIN')")
    public List<Course> getCourses(){
        return this.courseService.getCourses();

    }

    @PostMapping("/courses")
    public Course addCourses(@RequestBody Course newCourse) {

        return this.courseService.addCourse(newCourse);

    }

    @PutMapping(path = "/courses")
    public  Course editCourse(@Valid @RequestBody Course newCourse){
        return this.courseService.editCourse(newCourse);
    }

    @GetMapping(path = "/courses/{id}")
    public Optional<Course> getByIdCourse(@PathVariable(value = "id") Integer id) {
       return this.courseService.getByIdCourse(id);
    }

    @DeleteMapping(path = "/courses/{id}")
    public void deleteCourse(@PathVariable(value = "id") Integer id) {
        this.courseService.deleteCourse(id);
    }

    @GetMapping("/questionsByIdCourse/{id}")
    public List<QuestionRequestDTO> getQuestionsByIdCourse(@PathVariable(value = "id") Integer id) {
        return this.courseService.getQuestionsByIdCourse(id);
    }
    @PutMapping("/courses/inviteUser/{email}")
    public Registration inviteUser(@PathVariable(value = "email") String email,
                                      @RequestBody Course course){
        return courseService.inviteUser(email, course);
    }

    @GetMapping(path = "/getcourses/{id}")
    public Course getByIdCourseCustomized(@PathVariable(value = "id") Integer id) {
        return this.courseService.getByIdCourseCustomized(id);
    }

    @GetMapping("/coursesByIdTeacher/{id}")
    public List<Course> getCoursesByIdTeacher(@PathVariable(value = "id") Integer id) {
        return this.courseService.getCoursesByIdTeacher(id);
    }

   @GetMapping("/courseUsersByIdTeacher/{id}")
    public List<User> getUsersByIdTeacher(@PathVariable(value = "id") Integer id){
        return registrationService.getUsersByIdTeacher(id);
    }

    @GetMapping("/coursesByIdForumAndIdUser/{idForum}/{idUser}")
    public List<Course> getCoursesByIdForumAndIdUser(@PathVariable(value = "idForum") Integer idForum,
                                                     @PathVariable(value = "idUser") Integer idUser) {
        return this.courseService.getCoursesByIdForumAndIdUser(idForum,idUser);
    }
}
