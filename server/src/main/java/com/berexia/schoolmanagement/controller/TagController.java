package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Tag;
import com.berexia.schoolmanagement.service.TagServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class TagController {

    @Autowired
    private TagServiceI tagService;

    @GetMapping("/tags")
    public List<Tag> getTags() {
        return this.tagService.getTags();
    }

    @PostMapping("/tags")
    public Tag addTag(@RequestBody Tag newTag) {
        return this.tagService.addTag(newTag);
    }

    @PutMapping(path = "/tags")
    public Tag editTag(@Valid @RequestBody Tag newTag) {
        return this.tagService.editTag(newTag);
    }

    @GetMapping(path = "/tags/{id}")
    public Optional<Tag> getByIdTag(@PathVariable(value = "id") Integer id) {
        return this.tagService.getByIdTag(id);
    }

    @DeleteMapping(path = "/tags/{id}")
    public void deleteTag(@PathVariable(value = "id") Integer id) {
        this.tagService.deleteTag(id);
    }

}
