package com.berexia.schoolmanagement.controller;

import com.berexia.schoolmanagement.model.Administrator;
import com.berexia.schoolmanagement.model.Forum;
import com.berexia.schoolmanagement.model.User;
import com.berexia.schoolmanagement.service.AdministratorServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
//
@CrossOrigin("*")
@RestController
public class AdministratorController {
    @Autowired
    private AdministratorServiceI administratorService;

    @GetMapping("/administrators")
    public List<Administrator> getAdministrators(){
        return this.administratorService.getAdministrators();

    }

    @PostMapping("/administrators")
    public Administrator addAdministrators(@RequestBody Administrator newAdministrator) {

        return this.administratorService.addAdministrator(newAdministrator);

    }

    @PutMapping(path = "/administrators")
    public  Administrator editAdministrator(@Valid @RequestBody Administrator newAdministrator){
        return this.administratorService.editAdministrator(newAdministrator);
    }

    @GetMapping(path = "/administrators/{id}")
    public Optional<Administrator> getByIdAdministrator(@PathVariable(value = "id") Integer id) {
       return this.administratorService.getByIdAdministrator(id);
    }

    @DeleteMapping(path = "/administrators/{id}")
    public void deleteAdministrator(@PathVariable(value = "id") Integer id) {
        this.administratorService.deleteAdministrator(id);
    }


}
