package com.berexia.schoolmanagement.controller;

import com.berexia.schoolmanagement.dto.NoteRequestDto;
import com.berexia.schoolmanagement.model.Note;
import com.berexia.schoolmanagement.service.NoteServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class NoteController {
    @Autowired
    private NoteServiceI noteService;

   @GetMapping("/notes")
    public List<Note> getNotes() {
        return this.noteService.getNotes();
    }

    @PostMapping("/notes")
    public Note addNote(@RequestBody Note newNote) {
        return this.noteService.addNote(newNote);
    }

    @PutMapping(path = "/notes")
    public Note editNote(@Valid @RequestBody Note newNote) {
        return this.noteService.editNote(newNote);
    }

    @GetMapping(path = "/notes/{id}")
    public Optional<Note> getByIdNote(@PathVariable(value = "id") Integer id) {
        return this.noteService.getByIdNote(id);
    }

    @DeleteMapping(path = "/notes/{id}")
    public void deleteNote(@PathVariable(value = "id") Integer id) {
        this.noteService.deleteNote(id);
    }

   /*@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/notes/student/{studentId}")
    public List<Note> getStudentNotes(@PathVariable(value = "studentId") Integer studentId) {
        return this.noteService.getNotesByStudent(studentId);
    }

    //@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/notes/group/{groupId}")
    public List<Note> getGroupNotes(@PathVariable(value = "groupId") Integer groupId) {
        return this.noteService.getNotesByGroup(groupId);
    }

   @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/notes/parent/{parentId}")
    public List<Note> getParentNotes(@PathVariable(value = "parentId") Integer parentId) {
        return this.noteService.getNotesByParent(parentId);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/notes/teacher/{teacherId}")
    public List<Note> getTeacherNotes(@PathVariable(value = "teacherId") Integer teacherId) {
        return this.noteService.getNotesByTeacher(teacherId);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/notes/administrator/{adminId}")
    public List<Note> getAdministratorNotes(@PathVariable(value = "adminId") Integer adminId) {
        return this.noteService.getNotesByAdministrator(adminId);
    }*/

    @GetMapping(path = "/notesByIdTeacher/{id}")
    public List<Note> getNotesByIdTeacher(@PathVariable(value = "id")Integer id) {
        return noteService.getNotesByIdTeacher(id);
    }

   @GetMapping(path = "/notesByIdLevel/{id}")
    public List<Note> getNotesByIdLevel( @PathVariable(value = "id")Integer id) {
        return noteService.getNotesByIdLevel(id);
    }
    @GetMapping(path = "/notesByIdCourse/{id}")
    public List<Note> getNotesByIdCourse(@PathVariable(value = "id")Integer id) {
        return noteService.getNotesByIdCourse(id);
    }

    @GetMapping(path = "/notesByIdStudent/{id}")
    public List<Note> getNotesByIdStudent(@PathVariable(value = "id")Integer id) {
        return noteService.getNotesByIdStudent(id);
    }
    @GetMapping(path = "/notesByIdGroup/{id}")
    public List<Note> getNotesByIdGroup(@PathVariable(value = "id")Integer id) {
        return noteService.getNotesByIdGroup(id);
    }

    @GetMapping(path = "/notesByIdParent/{idParent}")
    public List<Note> getNotesByIdParent(@PathVariable(value = "idParent")Integer idParent) {
        return noteService.getNotesByIdParent(idParent);
    }

    @PostMapping("/addNoteCustomized")
    public Note addNoteCustomized(@RequestBody NoteRequestDto noteRequest){
        return noteService.addNoteCustomized(noteRequest.getIdNote(), noteRequest.getIdStudent(), noteRequest.getIdCourse(),noteRequest.getNote(),noteRequest.getRemarque());
    }
}
