package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Schedule;
import com.berexia.schoolmanagement.model.Schedule;
import com.berexia.schoolmanagement.service.ScheduleServiceI;

import com.berexia.schoolmanagement.upload_files.FileStorageService;
import com.berexia.schoolmanagement.upload_files.UploadFileResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin("*")
@RestController
public class ScheduleController {
    @Autowired
    private ScheduleServiceI scheduleService;
   // private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/schedules")
    public List<Schedule> getSchedules(){
      return this.scheduleService.getSchedules();

    }

    @PostMapping("/schedules")
    public Schedule addSchedules(@RequestBody Schedule newSchedule) {
        return this.scheduleService.addSchedule(newSchedule);
    }

    @PutMapping(path = "/schedules")
    public  Schedule editSchedule(@Valid @RequestBody Schedule newSchedule){
        return this.scheduleService.editSchedule(newSchedule);
    }

    @GetMapping(path = "/schedules/{id}")
    public Optional<Schedule> getByIdSchedule(@PathVariable(value = "id") Integer id) {
       return this.scheduleService.getByIdSchedule(id);
    }

    @DeleteMapping(path = "/schedules/{id}")
    public void deleteSchedule(@PathVariable(value = "id") Integer id) {
        this.scheduleService.deleteSchedule(id);
    }

    @PostMapping("/uploadScheduleFile")
    public UploadFileResponse uploadScheduleFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadScheduleFile/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @PostMapping("/uploadMultipleScheduleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadScheduleFile(file))
                .collect(Collectors.toList());
    }

    @GetMapping("/downloadScheduleFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadScheduleFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            //logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
