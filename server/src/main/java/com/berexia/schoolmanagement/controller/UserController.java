package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Registration;
import com.berexia.schoolmanagement.model.Role;
import com.berexia.schoolmanagement.model.User;
import com.berexia.schoolmanagement.service.UserServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin("*")
@RestController
public class UserController {
    @Autowired
    private UserServiceI userService;

    @GetMapping("/users")
    public List<User> getUsers(){
         return this.userService.getUsers();
    }

    @PostMapping("/users")
    public User addUsers(@RequestBody User newUser) {
        return this.userService.addUser(newUser);
    }

    @PutMapping(path = "/users")
    public  User editUser(@Valid @RequestBody User newUser){
        return this.userService.editUser(newUser);
    }

    @GetMapping(path = "/users/{id}")
    public Optional<User> getByIdUser(@PathVariable(value = "id") Integer id) {
       return this.userService.getByIdUser(id);
    }

    @DeleteMapping(path = "/users/{id}")
    public void deleteUser(@PathVariable(value = "id") Integer id) {
        this.userService.deleteUser(id);
    }


   @GetMapping(path = "/user/roles/{idUser}")
    public List<Role> getUserRoles(@PathVariable(value = "idUser")Integer idUser){
        return this.userService.getUserRoles(idUser);
    }


    @PutMapping("/users/addrole/{idUser}")
    public User addNewRoleToUser(@PathVariable(value = "idUser")Integer idUser,@RequestBody Role role) {
        return this.userService.addNewRoleToUser(idUser,role);

    }

    @GetMapping(path = "/getusers/{id}")
    public User getByIdUserCustomized(@PathVariable(value = "id") Integer id) {
        return this.userService.getByIdUserCustomized(id);
    }

   /* @PutMapping("/users/acceptInvitation")
    public Registration acceptInvitation(@RequestBody Registration registration){
       return this.userService.acceptInvitation(registration);
    }*/
}


