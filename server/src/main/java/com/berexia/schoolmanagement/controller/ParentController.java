package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Parent;
import com.berexia.schoolmanagement.model.Student;
import com.berexia.schoolmanagement.service.ParentServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class ParentController {

    @Autowired
    private ParentServiceI parentService;

    @GetMapping("/parents")
    public List<Parent> getParents() {
        return this.parentService.getParents();
    }

    @PostMapping("/parents")
    public Parent addParent(@RequestBody Parent newParent) {
        return this.parentService.addParent(newParent);
    }

    @PutMapping(path = "/parents")
    public Parent editParent(@Valid @RequestBody Parent newParent) {
        return this.parentService.editParent(newParent);
    }

    @GetMapping(path = "/parents/{id}")
    public Optional<Parent> getByIdParent(@PathVariable(value = "id") Integer id) {
        return this.parentService.getByIdParent(id);
    }

    @DeleteMapping(path = "/parents/{id}")
    public void deleteParent(@PathVariable(value = "id") Integer id) {
        this.parentService.deleteParent(id);
    }

    @GetMapping("/studentsByIdParent/{id}")
    public List<Student> getStudentsByIdParent(@PathVariable(value = "id") Integer id) {
        return this.parentService.getStudentsByIdParent(id);
    }
}
