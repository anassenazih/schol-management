package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Role;
import com.berexia.schoolmanagement.service.RoleServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class RoleController {

    @Autowired
    private RoleServiceI roleService;

    @GetMapping("/roles")
    public List<Role> getRoles() {
        return this.roleService.getRoles();
    }

    @PostMapping("/roles")
    public Role addRole(@RequestBody Role newRole) {
        return this.roleService.addRole(newRole);
    }

    @PutMapping(path = "/roles")
    public Role editRole(@Valid @RequestBody Role newRole) {
        return this.roleService.editRole(newRole);
    }

    @GetMapping(path = "/roles/{id}")
    public Optional<Role> getByIdRole(@PathVariable(value = "id") Integer id) {
        return this.roleService.getByIdRole(id);
    }

    @DeleteMapping(path = "/roles/{id}")
    public void deleteRole(@PathVariable(value = "id") Integer id) {
        this.roleService.deleteRole(id);
    }

}
