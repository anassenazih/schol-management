package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Answer;
import com.berexia.schoolmanagement.model.Question;
import com.berexia.schoolmanagement.service.QuestionServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class QuestionController {

    @Autowired
    private QuestionServiceI questionService;

    @GetMapping("/questions")
    public List<Question> getQuestions() {
        return this.questionService.getQuestions();
    }

    @PostMapping("/questions")
    public Question addQuestion(@RequestBody Question newQuestion) {
        return this.questionService.addQuestion(newQuestion);
    }

    @PutMapping(path = "/questions")
    public Question editQuestion(@Valid @RequestBody Question newQuestion) {
        return this.questionService.editQuestion(newQuestion);
    }

    @GetMapping(path = "/questions/{id}")
    public Optional<Question> getByIdQuestion(@PathVariable(value = "id") Integer id) {
        return this.questionService.getByIdQuestion(id);
    }

    @DeleteMapping(path = "/questions/{id}")
    public void deleteQuestion(@PathVariable(value = "id") Integer id) {
        this.questionService.deleteQuestion(id);
    }

   /* @GetMapping(path = "/answersToQuestion")
    public List<Answer> answersToQuestion(@RequestBody Question question) {
        return question.getAnswers();
    }

    @PutMapping(path = "/addAnswerToQuestion")
    public void addAnswerToQuestion(@RequestBody Question question,Answer answer) {
         question.getAnswers().add(answer);
    }*/

    @GetMapping("/answersByIdQuestion/{id}")
    public List<Answer> getAnswersByIdQuestion(@PathVariable(value = "id") Integer id) {
        return this.questionService.getQuestionsByIdCourse(id);
    }

}
