package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Subject;
import com.berexia.schoolmanagement.service.SubjectServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class SubjectController {

    @Autowired
    private SubjectServiceI subjectService;

    @GetMapping("/subjects")
    public List<Subject> getSubjects() {
        return this.subjectService.getSubjects();
    }

    @PostMapping("/subjects")
    public Subject addSubject(@RequestBody Subject newSubject) {
        return this.subjectService.addSubject(newSubject);
    }

    @PutMapping(path = "/subjects")
    public Subject editSubject(@Valid @RequestBody Subject newSubject) {
        return this.subjectService.editSubject(newSubject);
    }

    @GetMapping(path = "/subjects/{id}")
    public Optional<Subject> getByIdSubject(@PathVariable(value = "id") Integer id) {
        return this.subjectService.getByIdSubject(id);
    }

    @DeleteMapping(path = "/subjects/{id}")
    public void deleteSubject(@PathVariable(value = "id") Integer id) {
        this.subjectService.deleteSubject(id);
    }

}
