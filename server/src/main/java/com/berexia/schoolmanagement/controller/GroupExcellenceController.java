package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.GroupExcellence;
import com.berexia.schoolmanagement.service.GroupExcellenceServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@CrossOrigin("*")
@RestController
public class GroupExcellenceController {

    @Autowired
    private GroupExcellenceServiceI groupsExcellenceService;

    @GetMapping("/groupss")
    public List<GroupExcellence> getGroupExcellences() {
        return this.groupsExcellenceService.getGroups();
    }

    @PostMapping("/groups")
    public GroupExcellence addGroupExcellence(@RequestBody GroupExcellence newGroupExcellence) {
        return this.groupsExcellenceService.addGroup(newGroupExcellence);
    }

    @PutMapping(path = "/groups")
    public GroupExcellence editGroupExcellence(@Valid @RequestBody GroupExcellence newGroupExcellence) {
        return this.groupsExcellenceService.editGroup(newGroupExcellence);
    }

    @GetMapping(path = "/groups/{id}")
    public Optional<GroupExcellence> getByIdGroupExcellence(@PathVariable(value = "id") Integer id) {
        return this.groupsExcellenceService.getByIdGroup(id);
    }

    @DeleteMapping(path = "/groups/{id}")
    public void deleteGroupExcellence(@PathVariable(value = "id") Integer id) {
        this.groupsExcellenceService.deleteGroup(id);
    }

    @GetMapping("/groupsByIdTeacher/{id}")
    public List<GroupExcellence> getGroupsByIdTeacher(@PathVariable(value = "id") Integer id) {
        return this.groupsExcellenceService.getGroupsByIdTeacher(id);
    }
}
