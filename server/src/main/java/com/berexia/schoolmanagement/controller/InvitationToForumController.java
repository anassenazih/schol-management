package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Course;
import com.berexia.schoolmanagement.model.InvitationToForum;
import com.berexia.schoolmanagement.model.Registration;
import com.berexia.schoolmanagement.service.InvitationToForumServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class InvitationToForumController {

    @Autowired
    private InvitationToForumServiceI invitationToForumService;

    @GetMapping("/invitationToForums")
    public List<InvitationToForum> getInvitationToForums() {
        return this.invitationToForumService.getInvitationToForums();
    }

    @PostMapping("/invitationToForums")
    public InvitationToForum addInvitationToForum(@RequestBody InvitationToForum newInvitationToForum) {
        return this.invitationToForumService.addInvitationToForum(newInvitationToForum);
    }

    @PutMapping(path = "/invitationToForums")
    public InvitationToForum editInvitationToForum(@Valid @RequestBody InvitationToForum newInvitationToForum) {
        return this.invitationToForumService.editInvitationToForum(newInvitationToForum);
    }

    @GetMapping(path = "/invitationToForums/{id}")
    public Optional<InvitationToForum> getByIdInvitationToForum(@PathVariable(value = "id") Integer id) {
        return this.invitationToForumService.getByIdInvitationToForum(id);
    }

    @DeleteMapping(path = "/invitationToForums/{id}")
    public void deleteInvitationToForum(@PathVariable(value = "id") Integer id) {
        this.invitationToForumService.deleteInvitationToForum(id);
    }

    @PutMapping("/inviteUserToForum/{email}/{idForum}")
    public InvitationToForum inviteUserToForum(@PathVariable(value = "email") String email,
                                   @PathVariable(value = "idForum") Integer idForum){
        return  this.invitationToForumService.inviteUserToForum(email, idForum);
    }

}
