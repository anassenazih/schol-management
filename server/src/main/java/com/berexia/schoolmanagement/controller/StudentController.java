package com.berexia.schoolmanagement.controller;
//
import com.berexia.schoolmanagement.model.Student;
import com.berexia.schoolmanagement.service.StudentServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class StudentController {

    @Autowired
    private StudentServiceI studentService;

    @GetMapping("/students")
    public List<Student> getStudents() {
        return this.studentService.getStudents();
    }

    @PostMapping("/students")
    public Student addStudent(@RequestBody Student newStudent) {
        return this.studentService.addStudent(newStudent);
    }

    @PutMapping(path = "/students")
    public Student editStudent(@Valid @RequestBody Student newStudent) {
        return this.studentService.editStudent(newStudent);
    }

    @GetMapping(path = "/students/{id}")
    public Optional<Student> getByIdStudent(@PathVariable(value = "id") Integer id) {
        return this.studentService.getByIdStudent(id);
    }

    @DeleteMapping(path = "/students/{id}")
    public void deleteStudent(@PathVariable(value = "id") Integer id) {
        this.studentService.deleteStudent(id);
    }



}
