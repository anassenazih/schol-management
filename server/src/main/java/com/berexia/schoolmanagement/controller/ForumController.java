package com.berexia.schoolmanagement.controller;

import com.berexia.schoolmanagement.model.Course;
import com.berexia.schoolmanagement.model.Forum;
import com.berexia.schoolmanagement.service.ForumServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
//
@CrossOrigin("*")
@RestController
public class ForumController {

    @Autowired
    private ForumServiceI forumService;


    @GetMapping("/forums")
    public List<Forum> getForums() {
        return this.forumService.getForums();
    }

    @PostMapping("/forums")
    public Forum addForum(@RequestBody Forum newForum) {
        return this.forumService.addForum(newForum);
    }

    @PutMapping(path = "/forums")
    public Forum editForum(@Valid @RequestBody Forum newForum) {
        return this.forumService.editForum(newForum);
    }

    @GetMapping(path = "/forums/{id}")
    public Optional<Forum> getByIdForum(@PathVariable(value = "id") Integer id) {
        return this.forumService.getByIdForum(id);
    }

    @DeleteMapping(path = "/forums/{id}")
    public void deleteForum(@PathVariable(value = "id") Integer id) {
        this.forumService.deleteForum(id);
    }

    @GetMapping("/forums/courses/{id}")
    public List<Course> getCoursesByIdForum(@PathVariable(value = "id") Integer id) {
        return this.forumService.getCoursesByIdForum(id);
    }

    @GetMapping(path = "/getForums/{id}")
    public Forum getByIdForumCustomized(@PathVariable(value = "id") Integer id) {
        return this.forumService.getByIdForumCustomized(id);
    }

    @GetMapping("/forumsByIdUser/{id}")
    public List<Forum> getForumsByIdUser(@PathVariable(value = "id") Integer id) {
        return this.forumService.getForumsByIdUser(id);
    }
}
