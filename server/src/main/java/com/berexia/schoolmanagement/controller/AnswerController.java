package com.berexia.schoolmanagement.controller;

import com.berexia.schoolmanagement.model.Answer;
import com.berexia.schoolmanagement.service.AnswerServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
//
@CrossOrigin("*")
@RestController
public class AnswerController {
    @Autowired
    private AnswerServiceI answerService;

    @GetMapping("/answers")
    public List<Answer> getAnswers(){
      return this.answerService.getAnswers();

    }

    @PostMapping("/answers")
    public Answer addAnswers(@RequestBody Answer newAnswer) {
        return this.answerService.addAnswer(newAnswer);
    }

    @PutMapping(path = "/answers")
    public  Answer editAnswer(@Valid @RequestBody Answer newAnswer){
        return this.answerService.editAnswer(newAnswer);
    }

    @GetMapping(path = "/answers/{id}")
    public Optional<Answer> getByIdAnswer(@PathVariable(value = "id") Integer id) {
       return this.answerService.getByIdAnswer(id);
    }

    @DeleteMapping(path = "/answers/{id}")
    public void deleteAnswer(@PathVariable(value = "id") Integer id) {
        this.answerService.deleteAnswer(id);
    }
}
