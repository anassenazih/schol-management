package com.berexia.schoolmanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
@AllArgsConstructor
@Getter
public class NoteRequestDto {
    Integer idNote;
    Integer idStudent;
    Integer idCourse;
    Float   note;
    String remarque;

}
