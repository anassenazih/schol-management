package com.berexia.schoolmanagement.dto;

import com.berexia.schoolmanagement.model.Question;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerDTO {
    private String questionQuestion;

}
