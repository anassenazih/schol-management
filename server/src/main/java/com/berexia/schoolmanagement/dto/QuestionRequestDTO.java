package com.berexia.schoolmanagement.dto;

import com.berexia.schoolmanagement.model.Answer;
import com.berexia.schoolmanagement.model.User;

import java.util.Date;
import java.util.List;

public interface QuestionRequestDTO {

    public Integer getIdQuestion();

    public String getQuestion();

    public Date getDateQuestion();

    public User getUser();

    public List<Answer> getAnswers();

    public String getUrl();

    public String getType();

    public String getName();
}