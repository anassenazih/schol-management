package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.LevelDTO;

import com.berexia.schoolmanagement.model.Level;



public interface LevelServiceI {
	public List<Level> getLevels();
	public Level addLevel(Level newLevel);
	public Level editLevel(Level newLevel);
	public Optional<Level> getByIdLevel(Integer id);
	public void deleteLevel(Integer id);
	public LevelDTO convertToDto(Level level);
	public Level convertToEntity(LevelDTO levelDTO);
}
