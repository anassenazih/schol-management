package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.AnswerDTO;
import com.berexia.schoolmanagement.model.Answer;


public interface AnswerServiceI {
	public List<Answer> getAnswers();
	public Answer addAnswer(Answer newAnswer);
	public Answer editAnswer(Answer newAnswer);
	public Optional<Answer> getByIdAnswer(Integer id);
	public void deleteAnswer(Integer id);
	public AnswerDTO convertToDto(Answer answer);
	public Answer convertToEntity(AnswerDTO answerDTO);
}
