package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.AdministratorDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.berexia.schoolmanagement.model.Administrator;
import com.berexia.schoolmanagement.repository.AdministratorRepository;
import com.berexia.schoolmanagement.service.AdministratorServiceI;


import org.springframework.stereotype.Service;

@Service
public class AdministratorServiceImpl implements AdministratorServiceI{
	@Autowired
	private AdministratorRepository administratorRepository;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<Administrator> getAdministrators() {

		return this.administratorRepository.findAll();
	}

	@Override
	public Administrator addAdministrator(Administrator newAdministrator) {
	
		return this.administratorRepository.save(newAdministrator);
	}

	@Override
	public Administrator editAdministrator(Administrator newAdministrator) {
		return this.administratorRepository.save(newAdministrator);
	}

	@Override
	public Optional<Administrator> getByIdAdministrator(Integer id) {
		
		return this.administratorRepository.findById(id);
	}

	@Override
	public void deleteAdministrator(Integer id) {
		this.administratorRepository.deleteById(id);
		
	}

	@Override
	public AdministratorDTO convertToDto(Administrator administrator) {
		AdministratorDTO administratorDTO = modelMapper.map(administrator, AdministratorDTO.class);
		return administratorDTO;
	}

	public Administrator convertToEntity(AdministratorDTO administratorDTO) {
		Administrator administrator = modelMapper.map(administratorDTO, Administrator.class);
		return administrator;
	}
}
