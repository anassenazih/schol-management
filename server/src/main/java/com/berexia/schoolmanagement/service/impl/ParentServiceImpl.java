package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.ParentDTO;
import com.berexia.schoolmanagement.model.Parent;
import com.berexia.schoolmanagement.model.Student;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.berexia.schoolmanagement.repository.ParentRepository;
import com.berexia.schoolmanagement.service.ParentServiceI;
import org.springframework.stereotype.Service;

@Service
public class ParentServiceImpl implements ParentServiceI{
	@Autowired
	private ParentRepository parentRepository;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<Parent> getParents() {
		// TODO Auto-generated method stub
		return this.parentRepository.findAll();
	}

	@Override
	public Parent addParent(Parent newParent) {
		// TODO Auto-generated method stub
		return this.parentRepository.save(newParent);
	}

	@Override
	public Parent editParent(Parent newParent) {
		return this.parentRepository.save(newParent);
	}

	@Override
	public Optional<Parent> getByIdParent(Integer id) {
		// TODO Auto-generated method stub
		return this.parentRepository.findById(id);
	}

	@Override
	public void deleteParent(Integer id) {
		// TODO Auto-generated method stub
		this.parentRepository.deleteById(id);
	}

	@Override
	public ParentDTO convertToDto(Parent parent) {
		ParentDTO parentDTO = modelMapper.map(parent, ParentDTO.class);
		return parentDTO;
	}

	@Override
	public Parent convertToEntity(ParentDTO parentDTO) {
		Parent parent = modelMapper.map(parentDTO, Parent.class);
		return parent;
	}

	@Override
	public List<Student> getStudentsByIdParent(Integer id) {
		return this.parentRepository.getStudentsByIdParent(id);
	}

}
