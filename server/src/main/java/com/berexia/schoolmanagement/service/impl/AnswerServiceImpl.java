package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.model.Answer;
import com.berexia.schoolmanagement.dto.AnswerDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.berexia.schoolmanagement.repository.AnswerRepository;
import com.berexia.schoolmanagement.service.AnswerServiceI;
import org.springframework.stereotype.Service;

@Service
public class AnswerServiceImpl implements AnswerServiceI{
	@Autowired
	private AnswerRepository answerRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<Answer> getAnswers() {
		
		return this.answerRepository.findAll();
	}

	@Override
	public Answer addAnswer(Answer newAnswer) {
		// TODO Auto-generated method stub
		return this.answerRepository.save(newAnswer);
	}

	@Override
	public Answer editAnswer(Answer newAnswer) {
		return this.answerRepository.save(newAnswer);
	}

	@Override
	public Optional<Answer> getByIdAnswer(Integer id) {
		// TODO Auto-generated method stub
		return this.answerRepository.findById(id);
	}

	@Override
	public void deleteAnswer(Integer id) {
		// TODO Auto-generated method stub
		this.answerRepository.deleteById(id);;
		
	}

	@Override
	public AnswerDTO convertToDto(Answer answer) {
		AnswerDTO answerDTO = modelMapper.map(answer, AnswerDTO.class);
		return answerDTO;
	}

	@Override
	public Answer convertToEntity(AnswerDTO answerDTO) {
		Answer answer = modelMapper.map(answerDTO, Answer.class);
		return answer;
	}

}
