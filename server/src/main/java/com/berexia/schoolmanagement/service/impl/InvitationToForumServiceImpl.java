package com.berexia.schoolmanagement.service.impl;
//

import com.berexia.schoolmanagement.model.InvitationToForum;
import com.berexia.schoolmanagement.repository.InvitationToForumRepository;
import com.berexia.schoolmanagement.service.ForumServiceI;
import com.berexia.schoolmanagement.service.InvitationToForumServiceI;

import com.berexia.schoolmanagement.service.UserServiceI;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class InvitationToForumServiceImpl implements InvitationToForumServiceI {
	@Autowired
	private InvitationToForumRepository invitationToForumRepository;
	@Autowired
	private UserServiceI userService;
	@Autowired
	private ForumServiceI forumService;


	@Override
	public List<InvitationToForum> getInvitationToForums() {
		// TODO Auto-generated method stub
		return invitationToForumRepository.findAll();
	}

	@Override
	public InvitationToForum addInvitationToForum(InvitationToForum newInvitationToForum) {
		// TODO Auto-generated method stub
		return this.invitationToForumRepository.save(newInvitationToForum);
	}

	@Override
	public InvitationToForum editInvitationToForum(InvitationToForum newInvitationToForum) {

		return this.invitationToForumRepository.save(newInvitationToForum);
	}

	@Override
	public Optional<InvitationToForum> getByIdInvitationToForum(Integer id) {
		// TODO Auto-generated method stub
		return this.invitationToForumRepository.findById(id);
	}

	@Override
	public void deleteInvitationToForum(Integer id) {
		// TODO Auto-generated method stub
		this.invitationToForumRepository.deleteById(id);
	}

	@Override
	public InvitationToForum inviteUserToForum(String email, Integer idForum) {
		InvitationToForum invitation=new InvitationToForum();
		invitation.setUser(userService.getByEmailUser(email));
		invitation.setInvitationDate(new Date());
		invitation.setForum(forumService.getByIdForumCustomized(idForum));
		return this.invitationToForumRepository.save(invitation);
	}

}
