package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.SubjectDTO;
import com.berexia.schoolmanagement.model.Subject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.berexia.schoolmanagement.repository.SubjectRepository;
import com.berexia.schoolmanagement.service.SubjectServiceI;
import org.springframework.stereotype.Service;

@Service
public class SubjectServiceImpl implements SubjectServiceI{
	@Autowired
	private SubjectRepository subjectRepository;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<Subject> getSubjects() {
		// TODO Auto-generated method stub
		return this.subjectRepository.findAll();
	}

	@Override
	public Subject addSubject(Subject newSubject) {
		// TODO Auto-generated method stub
		return this.subjectRepository.save(newSubject);
	}

	@Override
	public Subject editSubject(Subject newSubject) {

		return this.subjectRepository.save(newSubject);
	}

	@Override
	public Optional<Subject> getByIdSubject(Integer id) {
		// TODO Auto-generated method stub
		return this.subjectRepository.findById(id);
	}

	@Override
	public void deleteSubject(Integer id) {
		// TODO Auto-generated method stub
		this.subjectRepository.deleteById(id);
	}
	@Override
	public SubjectDTO convertToDto(Subject subject) {
		SubjectDTO subjectDTO = modelMapper.map(subject, SubjectDTO.class);
		return subjectDTO;
	}

	@Override
	public Subject convertToEntity(SubjectDTO subjectDTO) {
		Subject subject = modelMapper.map(subjectDTO, Subject.class);
		return subject;
	}

}
