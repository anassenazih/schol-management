package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.AdministratorDTO;
import com.berexia.schoolmanagement.model.Administrator;

public interface AdministratorServiceI {
	public List<Administrator> getAdministrators();

	public Administrator addAdministrator(Administrator newAdministrator);

	public Administrator editAdministrator(Administrator newAdministrator);

	public Optional<Administrator> getByIdAdministrator(Integer id);

	public void deleteAdministrator(Integer id);

	public AdministratorDTO convertToDto(Administrator administrator);

	public Administrator convertToEntity(AdministratorDTO administratorDTO);
}