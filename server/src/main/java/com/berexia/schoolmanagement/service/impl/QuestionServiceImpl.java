package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.QuestionDTO;
import com.berexia.schoolmanagement.model.Answer;
import com.berexia.schoolmanagement.model.Question;
import com.berexia.schoolmanagement.repository.QuestionRepository;
import com.berexia.schoolmanagement.service.QuestionServiceI;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//
@Service
public class QuestionServiceImpl implements QuestionServiceI{
    @Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<Question> getQuestions() {
		// TODO Auto-generated method stub
		return this.questionRepository.getQuesions();
	}

	@Override
	public Question addQuestion(Question newQuestion) {
		// TODO Auto-generated method stub
		return this.questionRepository.save(newQuestion);
	}

	@Override
	public Question editQuestion(Question newQuestion) {

		return this.questionRepository.save(newQuestion);
	}

	@Override
	public Optional<Question> getByIdQuestion(Integer id) {
		// TODO Auto-generated method stub
		return this.questionRepository.findById(id);
	}

	@Override
	public void deleteQuestion(Integer id) {
		// TODO Auto-generated method stub
		this.questionRepository.deleteById(id);;
	}

	@Override
	public QuestionDTO convertToDto(Question question) {
		QuestionDTO questionDTO = modelMapper.map(question, QuestionDTO.class);
		return questionDTO;
	}

	@Override
	public Question convertToEntity(QuestionDTO questionDTO) {
		Question question = modelMapper.map(questionDTO, Question.class);
		return question;
	}

	@Override
	public List<Answer> getQuestionsByIdCourse(Integer id) {
		return questionRepository.getAnswersByIdQuestion(id);
	}

}
