package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.TeacherDTO;
import com.berexia.schoolmanagement.model.Teacher;


public interface TeacherServiceI {
	public List<Teacher> getTeachers();
	public Teacher addTeacher(Teacher newTeacher);
	public Teacher editTeacher(Teacher newTeacher);
	public Optional<Teacher> getByIdTeacher(Integer id);
	public void deleteTeacher(Integer id);
	public TeacherDTO convertToDto(Teacher teacher);
	public Teacher convertToEntity(TeacherDTO teacherDTO);
}
