package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.TagDTO;
import com.berexia.schoolmanagement.model.Tag;



public interface TagServiceI {
	public List<Tag> getTags();
	public Tag addTag(Tag newTag);
	public Tag editTag(Tag newTag);
	public Optional<Tag> getByIdTag(Integer id);
	public void deleteTag(Integer id);
	public TagDTO convertToDto(Tag tag);
	public Tag convertToEntity(TagDTO tagDTO);
}
