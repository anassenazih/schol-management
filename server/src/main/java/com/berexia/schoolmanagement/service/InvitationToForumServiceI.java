package com.berexia.schoolmanagement.service;
//

import com.berexia.schoolmanagement.model.InvitationToForum;

import java.util.List;
import java.util.Optional;

public interface InvitationToForumServiceI {
	public List<InvitationToForum> getInvitationToForums();
	public InvitationToForum addInvitationToForum(InvitationToForum newInvitationToForum);
	public InvitationToForum editInvitationToForum(InvitationToForum newInvitationToForum);
	public Optional<InvitationToForum> getByIdInvitationToForum(Integer id);
	public void deleteInvitationToForum(Integer id);

    InvitationToForum inviteUserToForum(String email, Integer idForum);
}
