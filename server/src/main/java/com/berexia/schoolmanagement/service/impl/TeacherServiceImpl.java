package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.TeacherDTO;
import com.berexia.schoolmanagement.model.Teacher;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.berexia.schoolmanagement.repository.TeacherRepository;
import com.berexia.schoolmanagement.service.TeacherServiceI;
import org.springframework.stereotype.Service;

@Service
public class TeacherServiceImpl implements TeacherServiceI{
	@Autowired
	private TeacherRepository teacherRepository;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<Teacher> getTeachers() {
		// TODO Auto-generated method stub
		return this.teacherRepository.findAll();
	}

	@Override
	public Teacher addTeacher(Teacher newTeacher) {
		// TODO Auto-generated method stub
		return this.teacherRepository.save(newTeacher);
	}

	@Override
	public Teacher editTeacher(Teacher newTeacher) {

		return this.teacherRepository.save(newTeacher);
	}

	@Override
	public Optional<Teacher> getByIdTeacher(Integer id) {
		// TODO Auto-generated method stub
		return this.teacherRepository.findById(id);
	}

	@Override
	public void deleteTeacher(Integer id) {
		// TODO Auto-generated method stub
		this.teacherRepository.deleteById(id);
	}
	@Override
	public TeacherDTO convertToDto(Teacher teacher) {
		TeacherDTO teacherDTO = modelMapper.map(teacher, TeacherDTO.class);
		return teacherDTO;
	}

	@Override
	public Teacher convertToEntity(TeacherDTO teacherDTO) {
		Teacher teacher = modelMapper.map(teacherDTO, Teacher.class);
		return teacher;
	}

}
