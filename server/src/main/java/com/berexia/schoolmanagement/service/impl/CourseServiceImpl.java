package com.berexia.schoolmanagement.service.impl;
//
import com.berexia.schoolmanagement.dto.CourseDTO;

import com.berexia.schoolmanagement.dto.QuestionRequestDTO;
import com.berexia.schoolmanagement.model.Course;
import com.berexia.schoolmanagement.model.Question;
import com.berexia.schoolmanagement.model.Registration;
import com.berexia.schoolmanagement.repository.CourseRepository;
import com.berexia.schoolmanagement.repository.QuestionRepository;
import com.berexia.schoolmanagement.service.CourseServiceI;
import com.berexia.schoolmanagement.service.RegistrationServiceI;
import com.berexia.schoolmanagement.service.UserServiceI;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseServiceI {
	@Autowired
	private CourseRepository courseRepository;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private UserServiceI userService;
	@Autowired
	private RegistrationServiceI registrationService;



	@Override
	public List<Course> getCourses() {

		return this.courseRepository.findAll();
	}

	@Override
	public Course addCourse(Course newCourse) {
	
		return this.courseRepository.save(newCourse);

	}

	@Override
	public Course editCourse(Course newCourse) {
		return this.courseRepository.save(newCourse);
	}

	@Override
	public Optional<Course> getByIdCourse(Integer id) {
		
		return this.courseRepository.findById(id);
	}

	@Override
	public void deleteCourse(Integer id) {
		this.courseRepository.deleteById(id);
		
	}

	@Override
	public CourseDTO convertToDto(Course course) {
		CourseDTO courseDTO = modelMapper.map(course, CourseDTO.class);
		return courseDTO;
	}

	public Course convertToEntity(CourseDTO courseDTO) {
		Course course = modelMapper.map(courseDTO, Course.class);
		return course;
	}

	@Override
	public List<QuestionRequestDTO> getQuestionsByIdCourse(Integer id) {
		return this.courseRepository.getQuestionsByIdCourse(id);
	}

	@Override
	public Course getByIdCourseCustomized(Integer id) {
		return courseRepository.getByIdCourseCustomized(id);
	}

	@Override
	public Registration inviteUser(String email, Course course) {

		Registration registration=new Registration();
		registration.setUser(userService.getByEmailUser(email));
		registration.setCourse(course);
		return registrationService.addRegistration(registration);
	}

	@Override
	public List<Course> getCoursesByIdTeacher(Integer id) {
		return courseRepository.getCoursesByIdTeacher(id);
	}

	@Override
	public List<Course> getCoursesByIdForumAndIdUser(Integer idForum, Integer idUser) {
		return this.courseRepository.getCoursesByIdForumAndIdUser(idForum,idUser);
	}
	@Override
	public Course getCourseByName(String courseName){
		return this.courseRepository.getCourseByName(courseName);
	}
}
