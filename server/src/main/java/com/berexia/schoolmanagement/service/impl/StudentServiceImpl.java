package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.StudentDTO;
import com.berexia.schoolmanagement.model.Student;
import com.berexia.schoolmanagement.model.User;
import com.berexia.schoolmanagement.repository.StudentRepository;
import com.berexia.schoolmanagement.service.StudentServiceI;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentServiceI{
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<Student> getStudents() {
		// TODO Auto-generated method stub
		return this.studentRepository.findAll();
	}

	@Override
	public Student addStudent(Student newStudent) {
		// TODO Auto-generated method stub
		return this.studentRepository.save(newStudent);
	}

	@Override
	public Student editStudent(Student newStudent) {

		return this.studentRepository.save(newStudent);
	}

	@Override
	public Optional<Student> getByIdStudent(Integer id) {
		// TODO Auto-generated method stub
		return this.studentRepository.findById(id);
	}

	@Override
	public void deleteStudent(Integer id) {
		// TODO Auto-generated method stub
	}
	@Override
	public StudentDTO convertToDto(Student student) {
		StudentDTO studentDTO = modelMapper.map(student, StudentDTO.class);
		return studentDTO;
	}

	@Override
	public Student convertToEntity(StudentDTO studentDTO) {
		Student student = modelMapper.map(studentDTO, Student.class);
		return student;
	}

	@Override
	public Student getStudentByCne(String cne) {
		return studentRepository.getStudentByCne(cne);
	}


}
