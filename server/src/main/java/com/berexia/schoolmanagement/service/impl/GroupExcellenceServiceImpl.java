package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.GroupExcellenceDTO;
import com.berexia.schoolmanagement.model.GroupExcellence;
import com.berexia.schoolmanagement.repository.GroupRepository;
import com.berexia.schoolmanagement.service.GroupExcellenceServiceI;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GroupExcellenceServiceImpl implements GroupExcellenceServiceI {

    @Autowired
	private GroupRepository groupRepository;
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public List<GroupExcellence> getGroups() {
		// TODO Auto-generated method stub
		return this.groupRepository.findAll();
	}

	@Override
	public GroupExcellence addGroup(GroupExcellence newGroupExcellence) {
		// TODO Auto-generated method stub
		return this.groupRepository.save(newGroupExcellence);
	}

	@Override
	public GroupExcellence editGroup(GroupExcellence newGroupExcellence) {

		return this.groupRepository.save(newGroupExcellence);
	}

	@Override
	public Optional<GroupExcellence> getByIdGroup(Integer id) {
		// TODO Auto-generated method stub
		return this.groupRepository.findById(id);
	}

	@Override
	public void deleteGroup(Integer id) {
		// TODO Auto-generated method stub
		this.groupRepository.deleteById(id);
	}

	@Override
	public GroupExcellenceDTO convertToDto(GroupExcellence groupExcellence) {
		GroupExcellenceDTO groupExcellenceDTO = modelMapper.map(groupExcellence, GroupExcellenceDTO.class);
		return groupExcellenceDTO;
	}

	@Override
	public GroupExcellence convertToEntity(GroupExcellenceDTO groupExcellenceDTO) {
		GroupExcellence groupExcellence = modelMapper.map(groupExcellenceDTO, GroupExcellence.class);
		return groupExcellence;
	}

	@Override
	public List<GroupExcellence> getGroupsByIdTeacher(Integer id) {
		return this.groupRepository.getGroupsByIdTeacher(id);
	}

}
