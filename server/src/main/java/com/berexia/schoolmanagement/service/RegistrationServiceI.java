package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.RegistrationDTO;

import com.berexia.schoolmanagement.model.Registration;
import com.berexia.schoolmanagement.model.User;
import org.springframework.data.repository.query.Param;
//
public interface RegistrationServiceI {
	public List<Registration> getRegistrations();
	public Registration addRegistration(Registration newRegistration);
	public Registration editRegistration(Registration newRegistration);
	public Optional<Registration> getByIdRegistration(Integer id);
	public void deleteRegistration(Integer id);
	public RegistrationDTO convertToDto(Registration registration);
	public Registration convertToEntity(RegistrationDTO registrationDTO);

	public List<User> getUsersByIdTeacher(Integer id);
}
