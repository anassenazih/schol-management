package com.berexia.schoolmanagement.service;
//

import com.berexia.schoolmanagement.model.Schedule;

import java.util.List;
import java.util.Optional;


public interface ScheduleServiceI {
	public List<Schedule> getSchedules();
	public Schedule addSchedule(Schedule newSchedule);
	public Schedule editSchedule(Schedule newSchedule);
	public Optional<Schedule> getByIdSchedule(Integer id);
	public void deleteSchedule(Integer id);

}
