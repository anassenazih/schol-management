package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.TagDTO;
import com.berexia.schoolmanagement.model.Tag;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.berexia.schoolmanagement.repository.TagRepository;
import com.berexia.schoolmanagement.service.TagServiceI;
import org.springframework.stereotype.Service;

@Service
public class TagServiceImpl implements TagServiceI
{
	@Autowired
	private TagRepository tagRepository;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<Tag> getTags() {
		// TODO Auto-generated method stub
		return this.tagRepository.findAll();
	}

	@Override
	public Tag addTag(Tag newTag) {
		// TODO Auto-generated method stub
		return this.tagRepository.save(newTag);
	}

	@Override
	public Tag editTag(Tag newTag) {

		return this.tagRepository.save(newTag);
	}

	@Override
	public Optional<Tag> getByIdTag(Integer id) {
		// TODO Auto-generated method stub
		return this.tagRepository.findById(id);
	}

	@Override
	public void deleteTag(Integer id) {
		// TODO Auto-generated method stub
		this.tagRepository.deleteById(id);
	}

	@Override
	public TagDTO convertToDto(Tag tag) {
		TagDTO tagDTO = modelMapper.map(tag, TagDTO.class);
		return tagDTO;
	}

	@Override
	public Tag convertToEntity(TagDTO tagDTO) {
		Tag tag = modelMapper.map(tagDTO, Tag.class);
		return tag;
	}

}
