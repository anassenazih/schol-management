package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.ForumDTO;
import com.berexia.schoolmanagement.model.Course;
import com.berexia.schoolmanagement.model.Forum;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.berexia.schoolmanagement.repository.ForumRepository;
import com.berexia.schoolmanagement.service.ForumServiceI;
import org.springframework.stereotype.Service;

@Service
public class ForumServiceImpl implements ForumServiceI{
	@Autowired
	private ForumRepository forumRepository;
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<Forum> getForums() {
		// TODO Auto-generated method stub
		return this.forumRepository.findAll();
	}

	@Override
	public Forum addForum(Forum newForum) {
		// TODO Auto-generated method stub
		return this.forumRepository.save(newForum);
	}

	@Override
	public Forum editForum(Forum newForum) {

		return this.forumRepository.save(newForum);
	}

	@Override
	public Optional<Forum> getByIdForum(Integer id) {
		// TODO Auto-generated method stub
		return this.forumRepository.findById(id);
	}

	@Override
	public void deleteForum(Integer id) {
		// TODO Auto-generated method stub
		this.forumRepository.deleteById(id);
	}
	@Override
	public ForumDTO convertToDto(Forum forum) {
		ForumDTO forumDTO = modelMapper.map(forum, ForumDTO.class);
		return forumDTO;
	}

	@Override
	public Forum convertToEntity(ForumDTO forumDTO) {
		Forum forum = modelMapper.map(forumDTO, Forum.class);
		return forum;
	}


	@Override
	public List<Course> getCoursesByIdForum(Integer id) {
		return this.forumRepository.getCoursesByIdForum(id);
	}

	@Override
	public Forum getByIdForumCustomized(Integer id) {
		return this.forumRepository.getByIdForumCustomized(id);
	}

	@Override
	public List<Forum> getForumsByIdUser(Integer id) {
		return this.forumRepository.getForumsByIdUser(id);
	}


}
