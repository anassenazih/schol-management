package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.RegistrationDTO;
import com.berexia.schoolmanagement.model.Registration;
import com.berexia.schoolmanagement.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.berexia.schoolmanagement.repository.RegistrationRepository;
import com.berexia.schoolmanagement.service.RegistrationServiceI;
import org.springframework.stereotype.Service;
//
@Service
public class RegistrationServiceImpl implements RegistrationServiceI{
	@Autowired
	private RegistrationRepository registrationRepository;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<Registration> getRegistrations() {
		// TODO Auto-generated method stub
		return registrationRepository.findAll();
	}

	@Override
	public Registration addRegistration(Registration newRegistration) {
		// TODO Auto-generated method stub
		return this.registrationRepository.save(newRegistration);
	}

	@Override
	public Registration editRegistration(Registration newRegistration) {

		return this.registrationRepository.save(newRegistration);
	}

	@Override
	public Optional<Registration> getByIdRegistration(Integer id) {
		// TODO Auto-generated method stub
		return this.registrationRepository.findById(id);
	}

	@Override
	public void deleteRegistration(Integer id) {
		// TODO Auto-generated method stub
		this.registrationRepository.deleteById(id);;
	}

	@Override
	public RegistrationDTO convertToDto(Registration registration) {
		RegistrationDTO registrationDTO = modelMapper.map(registration, RegistrationDTO.class);
		return registrationDTO;
	}

	@Override
	public Registration convertToEntity(RegistrationDTO registrationDTO) {
		Registration registration = modelMapper.map(registrationDTO, Registration.class);
		return registration;
	}

	@Override
	public List<User> getUsersByIdTeacher(Integer id) {
		return registrationRepository.getUsersByIdTeacher(id);
	}

}
