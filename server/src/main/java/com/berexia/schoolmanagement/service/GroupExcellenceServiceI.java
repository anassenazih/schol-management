package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.GroupExcellenceDTO;
import com.berexia.schoolmanagement.model.GroupExcellence;



public interface GroupExcellenceServiceI {

	public List<GroupExcellence> getGroups();
	public GroupExcellence addGroup(GroupExcellence newGroupExcellence);
	public GroupExcellence editGroup(GroupExcellence newGroupExcellence);
	public Optional<GroupExcellence> getByIdGroup(Integer id);
	public void deleteGroup(Integer id);
	public GroupExcellenceDTO convertToDto(GroupExcellence groupExcellence);
	public GroupExcellence convertToEntity(GroupExcellenceDTO groupExcellenceDTO);

    List<GroupExcellence> getGroupsByIdTeacher(Integer id);
}
