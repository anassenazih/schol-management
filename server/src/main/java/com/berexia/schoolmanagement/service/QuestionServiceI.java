package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.QuestionDTO;
import com.berexia.schoolmanagement.model.Answer;
import com.berexia.schoolmanagement.model.Question;

//
public interface QuestionServiceI {
	public List<Question> getQuestions();
	public Question addQuestion(Question newQuestion);
	public Question editQuestion(Question newQuestion);
	public Optional<Question> getByIdQuestion(Integer id);
	public void deleteQuestion(Integer id);
	public QuestionDTO convertToDto(Question question);
	public Question convertToEntity(QuestionDTO questionDTO);

    List<Answer> getQuestionsByIdCourse(Integer id);
}
