package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.LevelDTO;
import com.berexia.schoolmanagement.model.Level;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.berexia.schoolmanagement.repository.LevelRepository;
import com.berexia.schoolmanagement.service.LevelServiceI;
import org.springframework.stereotype.Service;

@Service
public class LevelServiceImpl implements LevelServiceI
{
	@Autowired
	private LevelRepository levelRepository;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<Level> getLevels() {
		// TODO Auto-generated method stub
		return this.levelRepository.findAll();
	}

	@Override
	public Level addLevel(Level newLevel) {
		// TODO Auto-generated method stub
		return this.levelRepository.save(newLevel);
	}

	@Override
	public Level editLevel(Level newLevel) {

		return this.levelRepository.save(newLevel);
	}

	@Override
	public Optional<Level> getByIdLevel(Integer id) {
		// TODO Auto-generated method stub
		return this.levelRepository.findById(id);
	}

	@Override
	public void deleteLevel(Integer id) {
		// TODO Auto-generated method stub
		this.levelRepository.deleteById(id);
	}

	@Override
	public LevelDTO convertToDto(Level level) {
		LevelDTO levelDTO = modelMapper.map(level, LevelDTO.class);
		return levelDTO;
	}

	@Override
	public Level convertToEntity(LevelDTO levelDTO) {
		Level level = modelMapper.map(levelDTO, Level.class);
		return level;
	}
}
