package com.berexia.schoolmanagement.service.impl;
//
import java.util.*;

import com.berexia.schoolmanagement.dto.UserDTO;
import com.berexia.schoolmanagement.model.Registration;
import com.berexia.schoolmanagement.model.Role;
import com.berexia.schoolmanagement.model.User;
import com.berexia.schoolmanagement.service.RegistrationServiceI;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.berexia.schoolmanagement.repository.UserRepository;
import com.berexia.schoolmanagement.service.UserServiceI;
import org.springframework.stereotype.Service;
//
@Service
public class UserServiceImpl implements UserServiceI{
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RegistrationServiceI registrationService;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<User> getUsers() {
		// TODO Auto-generated method stub
		return this.userRepository.findAll();
	}

	@Override
	public User addUser(User newUser) {
		// TODO Auto-generated method stub
		return this.userRepository.save(newUser);
	}

	@Override
	public User editUser(User newUser) {
		return this.userRepository.save(newUser);
	}

	@Override
	public Optional<User> getByIdUser(Integer id) {
		// TODO Auto-generated method stub
		return this.userRepository.findById(id);
	}

	@Override
	public void deleteUser(Integer id) {
		// TODO Auto-generated method stub
		this.userRepository.deleteById(id);
	}

	@Override
	public UserDTO convertToDto(User user) {
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		return userDTO;
	}

	@Override
	public User convertToEntity(UserDTO userDTO) {
		User user = modelMapper.map(userDTO, User.class);
		return user;
	}

	@Override
	public User addNewRoleToUser(Integer id,Role role) {
	    User user= userRepository.getByIdUserCustomized(id);
	    Set<Role> roles= user.getRoles();
		roles.add(role);
		user.setRoles(roles);

		return  userRepository.save(user);


	}

	@Override
	public List<Role> getUserRoles(Integer idUser) {
		return this.userRepository.getRolesByIdUser(idUser);

	}

	@Override
	public User getByIdUserCustomized(Integer id) {
		return userRepository.getByIdUserCustomized(id);
	}

	/*@Override
	public Registration acceptInvitation(Registration registration) {
		registration.setStatus("accepted");
		return registrationService.editRegistration(registration) ;
	}*/

	@Override
	public User getByEmailUser(String email) {
		return userRepository.getByEmailUser(email);
	}

}
