package com.berexia.schoolmanagement.service;
//
import com.berexia.schoolmanagement.dto.NoteDTO;
import com.berexia.schoolmanagement.model.Note;

import java.util.List;
import java.util.Optional;
//
public interface NoteServiceI {
    public List<Note> getNotes();

    public Note addNote(Note newNote);

    public Note editNote(Note newNote);

    public Optional<Note> getByIdNote(Integer id);

    public void deleteNote(Integer id);

    /*public List<Note> getNotesByStudent(Integer studentId);

    public List<Note> getNotesByGroup(Integer groupId);

    public List<Note> getNotesByParent(Integer parentId);

  public List<Note> getNotesByTeacher(Integer teacherId);

    public List<Note> getNotesByAdministrator(Integer teacherId);*/

    public NoteDTO convertToDto(Note note);

    public Note convertToEntity(NoteDTO noteDTO);


    public List<Note> getNotesByIdTeacher(Integer idTeacher);

    public List<Note> getNotesByIdLevel(Integer idLevel);


    public List<Note> getNotesByIdCourse(Integer idCourse);

    List<Note> getNotesByIdStudent(Integer id);

    List<Note> getNotesByIdGroup(Integer id);

    List<Note> getNotesByIdParent(Integer idParent);

    Note addNoteCustomized(Integer idNote,Integer idStudent, Integer idCourse, Float note, String remarque);
}
