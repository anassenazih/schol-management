package com.berexia.schoolmanagement.service.impl;
//
import com.berexia.schoolmanagement.dto.NoteDTO;
import com.berexia.schoolmanagement.model.Note;
import com.berexia.schoolmanagement.repository.CourseRepository;
import com.berexia.schoolmanagement.repository.NoteRepository;
import com.berexia.schoolmanagement.repository.StudentRepository;
import com.berexia.schoolmanagement.service.NoteServiceI;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
//
@Service
public class NoteServiceImpl implements NoteServiceI {
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private CourseRepository courseRepository;// for addNoteByIdCustomized
    @Autowired
    private StudentRepository studentRepository;// for addNoteByIdCustomized
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<Note> getNotes() {
        return this.noteRepository.findAll();
    }

    @Override
    public Note addNote(Note newNote) {

        return this.noteRepository.save(newNote);
    }

    @Override
    public Note editNote(Note newNote) {

        return this.noteRepository.save(newNote);
    }

    @Override
    public Optional<Note> getByIdNote(Integer id) {
        return this.noteRepository.findById(id);
    }

    @Override
    public void deleteNote(Integer id) {
        this.noteRepository.deleteById(id);
    }

    @Override
    public NoteDTO convertToDto(Note note) {
        NoteDTO noteDTO = modelMapper.map(note, NoteDTO.class);
        return noteDTO;
    }

    public Note convertToEntity(NoteDTO noteDTO) {
        Note note = modelMapper.map(noteDTO, Note.class);
        return note;
    }


   @Override
    public List<Note> getNotesByIdTeacher(Integer idTeacher) {
        return noteRepository.getNotesByIdTeacher(idTeacher);
    }

     @Override
    public List<Note> getNotesByIdLevel(Integer idLevel) {
        return noteRepository.getNotesByIdLevel(idLevel);
    }

    @Override
    public List<Note> getNotesByIdCourse(Integer idCourse) {
        return noteRepository.getNotesByIdCourse(idCourse);
    }

    @Override
    public List<Note> getNotesByIdStudent(Integer id) {
        return noteRepository.getNotesByIdStudent(id);
    }

    @Override
    public List<Note> getNotesByIdGroup(Integer id) {
        return noteRepository.getNotesByIdGroup(id);
    }

    @Override
    public List<Note> getNotesByIdParent(Integer idParent) {
        return noteRepository.getNotesByIdParent(idParent);
    }

    public Note getByIdNoteCustomized(Integer id){
        return noteRepository.getByIdNoteCustomized(id);
    }

    @Override
    public Note addNoteCustomized(Integer idNote,Integer idStudent, Integer idCourse, Float note, String remarque) {
        Note noteObject;

        if(getByIdNoteCustomized(idNote)!=null){
            noteObject=getByIdNoteCustomized(idNote);
        }
        else {
            noteObject=new Note();
        }
        noteObject.setCourse(courseRepository.getByIdCourseCustomized(idCourse));
        noteObject.setStudent(studentRepository.getByIdStudentCustomized(idStudent));
        noteObject.setNote(note);
        noteObject.setRemarque(remarque);
        return noteRepository.save(noteObject);
    }


}
