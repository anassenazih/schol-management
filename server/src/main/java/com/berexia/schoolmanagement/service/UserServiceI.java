package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.UserDTO;
import com.berexia.schoolmanagement.model.Registration;
import com.berexia.schoolmanagement.model.Role;
import com.berexia.schoolmanagement.model.User;

//
public interface UserServiceI {

	public List<User> getUsers();
	public User addUser(User newUser);
	public User editUser(User newUser);
	public Optional<User> getByIdUser(Integer id);
	public void deleteUser(Integer id);
	public UserDTO convertToDto(User user);
	public User convertToEntity(UserDTO userDTO);
	public User addNewRoleToUser(Integer id,Role role);
	public List<Role> getUserRoles(Integer idUser);

    User getByIdUserCustomized(Integer id);

    /*Registration acceptInvitation(Registration registration);*/

	User getByEmailUser(String email);

}
