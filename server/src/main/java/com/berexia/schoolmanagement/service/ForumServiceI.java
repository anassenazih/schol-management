package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;


import com.berexia.schoolmanagement.dto.ForumDTO;
import com.berexia.schoolmanagement.model.Course;
import com.berexia.schoolmanagement.model.Forum;


public interface ForumServiceI {
	public List<Forum> getForums();

	public Forum addForum(Forum newForum);

	public Forum editForum(Forum newForum);

	public Optional<Forum> getByIdForum(Integer id);

	public void deleteForum(Integer id);

	public ForumDTO convertToDto(Forum forum);

	public Forum convertToEntity(ForumDTO forumDTO);

	List<Course> getCoursesByIdForum(Integer id);

    Forum getByIdForumCustomized(Integer id);

    List<Forum> getForumsByIdUser(Integer id);
}
