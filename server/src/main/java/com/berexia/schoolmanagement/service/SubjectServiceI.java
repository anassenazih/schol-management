package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.SubjectDTO;
import com.berexia.schoolmanagement.model.Subject;


public interface SubjectServiceI {
	public List<Subject> getSubjects();
	public Subject addSubject(Subject newSubject);
	public Subject editSubject(Subject newSubject);
	public Optional<Subject> getByIdSubject(Integer id);
	public void deleteSubject(Integer id);
	public SubjectDTO convertToDto(Subject subject);
	public Subject convertToEntity(SubjectDTO subjectDTO);
}
