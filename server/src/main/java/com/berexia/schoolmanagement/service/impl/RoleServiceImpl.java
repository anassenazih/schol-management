package com.berexia.schoolmanagement.service.impl;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.RoleDTO;
import com.berexia.schoolmanagement.model.Role;
import com.berexia.schoolmanagement.repository.RoleRepository;
import com.berexia.schoolmanagement.service.RoleServiceI;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleServiceI{
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private ModelMapper modelMapper;


	@Override
	public List<Role> getRoles() {
		// TODO Auto-generated method stub
		return this.roleRepository.findAll();
	}

	@Override
	public Role addRole(Role newRole) {
		// TODO Auto-generated method stub
		return this.roleRepository.save(newRole);
	}

	@Override
	public Role editRole(Role newRole) {
		return this.roleRepository.save(newRole);
	}

	@Override
	public Optional<Role> getByIdRole(Integer id) {
		// TODO Auto-generated method stub
		return this.roleRepository.findById(id);
	}

	@Override
	public void deleteRole(Integer id) {
		// TODO Auto-generated method stub
		this.roleRepository.deleteById(id);
	}
	@Override
	public RoleDTO convertToDto(Role role) {
		RoleDTO roleDTO = modelMapper.map(role, RoleDTO.class);
		return roleDTO;
	}

	@Override
	public Role convertToEntity(RoleDTO roleDTO) {
		Role role = modelMapper.map(roleDTO, Role.class);
		return role;
	}

}
