package com.berexia.schoolmanagement.service.impl;
//
import com.berexia.schoolmanagement.model.Schedule;
//import com.berexia.schoolmanagement.repository.ScheduleRepository;
import com.berexia.schoolmanagement.repository.ScheduleRepository;
import com.berexia.schoolmanagement.service.ScheduleServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ScheduleServiceImpl implements ScheduleServiceI {
	@Autowired
	private ScheduleRepository scheduleRepository;


	@Override
	public List<Schedule> getSchedules() {
		
		return this.scheduleRepository.findAll();
	}

	@Override
	public Schedule addSchedule(Schedule newSchedule) {
		// TODO Auto-generated method stub
		return this.scheduleRepository.save(newSchedule);
	}

	@Override
	public Schedule editSchedule(Schedule newSchedule) {

		return this.scheduleRepository.save(newSchedule);
	}

	@Override
	public Optional<Schedule> getByIdSchedule(Integer id) {
		// TODO Auto-generated method stub
		return this.scheduleRepository.findById(id);
	}

	@Override
	public void deleteSchedule(Integer id) {
		// TODO Auto-generated method stub
		this.scheduleRepository.deleteById(id);
	}

}
