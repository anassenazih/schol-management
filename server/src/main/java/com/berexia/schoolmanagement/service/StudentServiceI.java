package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.StudentDTO;
import com.berexia.schoolmanagement.model.Student;
import com.berexia.schoolmanagement.model.User;
import org.springframework.data.repository.query.Param;

public interface StudentServiceI {
	public List<Student> getStudents();
	public Student addStudent(Student newStudent);
	public Student editStudent(Student newStudent);
	public Optional<Student> getByIdStudent(Integer id);
	public void deleteStudent(Integer id);
	public StudentDTO convertToDto(Student student);
	public Student convertToEntity(StudentDTO studentDTO);
	public Student getStudentByCne(String cne);
}
