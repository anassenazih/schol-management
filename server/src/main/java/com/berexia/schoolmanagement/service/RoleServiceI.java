package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.RoleDTO;
import com.berexia.schoolmanagement.model.Role;


public interface RoleServiceI {
	public List<Role> getRoles();
	public Role addRole(Role newRole);
	public Role editRole(Role newRole);
	public Optional<Role> getByIdRole(Integer id);
	public void deleteRole(Integer id);
	public RoleDTO convertToDto(Role role);
	public Role convertToEntity(RoleDTO roleDTO);
}
