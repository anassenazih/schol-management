package com.berexia.schoolmanagement.service;
//
import com.berexia.schoolmanagement.dto.CourseDTO;
import com.berexia.schoolmanagement.dto.QuestionRequestDTO;
import com.berexia.schoolmanagement.model.Course;
import com.berexia.schoolmanagement.model.Question;
import com.berexia.schoolmanagement.model.Registration;

import java.util.List;
import java.util.Optional;

public interface CourseServiceI {
	public List<Course> getCourses();

	public Course addCourse(Course newCourse);

	public Course editCourse(Course newCourse);

	public Optional<Course> getByIdCourse(Integer id);

	public void deleteCourse(Integer id);

	public CourseDTO convertToDto(Course administrator);

	public Course convertToEntity(CourseDTO administratorDTO);

    List<QuestionRequestDTO> getQuestionsByIdCourse(Integer id);

	public Course getByIdCourseCustomized(Integer id);

	public Registration inviteUser( String email,Course course);

    List<Course> getCoursesByIdTeacher(Integer id);

    List<Course> getCoursesByIdForumAndIdUser(Integer idForum, Integer idUser);

	public Course getCourseByName(String courseName);
}