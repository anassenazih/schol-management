package com.berexia.schoolmanagement.service;
//
import java.util.List;
import java.util.Optional;

import com.berexia.schoolmanagement.dto.ParentDTO;
import com.berexia.schoolmanagement.model.Parent;
import com.berexia.schoolmanagement.model.Student;


public interface ParentServiceI {
	public List<Parent> getParents();
	public Parent addParent(Parent newParent);
	public Parent editParent(Parent newParent);
	public Optional<Parent> getByIdParent(Integer id);
	public void deleteParent(Integer id);
	public ParentDTO convertToDto(Parent parent);
	public Parent convertToEntity(ParentDTO parentDTO);

    List<Student> getStudentsByIdParent(Integer id);
}
