package com.berexia.schoolmanagement.authentification;


import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.berexia.schoolmanagement.authentification.payload.request.LoginRequest;
import com.berexia.schoolmanagement.authentification.payload.request.SignupRequest;
import com.berexia.schoolmanagement.authentification.payload.response.JwtResponse;
import com.berexia.schoolmanagement.authentification.payload.response.MessageResponse;
import com.berexia.schoolmanagement.authentification.security.jwt.JwtUtils;
import com.berexia.schoolmanagement.authentification.security.services.UserDetailsImpl;
import com.berexia.schoolmanagement.model.*;
import com.berexia.schoolmanagement.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;
	@Autowired
	AdministratorRepository administratorRepository;
	@Autowired
	ParentRepository parentRepository;
	@Autowired
	StudentRepository studentRepository;
	@Autowired
	TeacherRepository teacherRepository;
	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	String jwt= "test";
	UserDetailsImpl userDetails= new UserDetailsImpl(1,"test","test@gmail.com"
	,"test",null);

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		//String
				jwt = jwtUtils.generateJwtToken(authentication);
		
		//UserDetailsImpl
				userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());


		return ResponseEntity.ok(new JwtResponse(jwt,
												 userDetails.getId(),
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()),
				             signUpRequest.getFirstName(), signUpRequest.getLastName(),
							 signUpRequest.getPhoneNumber(),signUpRequest.getAddress(),
							 signUpRequest.getGender(),signUpRequest.getDateOfBirth());

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		// set user roles

		if (strRoles == null) {
			Role userRole = roleRepository.findByRole(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "ROLE_ADMIN":
					Role adminRole = roleRepository.findByRole(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "ROLE_STUDENT":
					Role studentRole = roleRepository.findByRole(ERole.ROLE_STUDENT)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(studentRole);

					break;
				case "ROLE_TEACHER":
						Role teacherRole = roleRepository.findByRole(ERole.ROLE_TEACHER)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(teacherRole);

						break;
				case "ROLE_PARENT":
						Role parentRole = roleRepository.findByRole(ERole.ROLE_PARENT)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(parentRole);

						break;
				default:
					Role userRole = roleRepository.findByRole(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		// persist user as Teacher,Student ...., User otherwise

		if (strRoles == null) {
			user.setRoles(roles);
			userRepository.save(user);

		} else {
			strRoles.forEach(role -> {
						switch (role) {
							case "ROLE_ADMIN":
								Administrator admin = new Administrator(user.getUsername(), user.getEmail(),
								user.getPassword(),user.getFirstName(),user.getLastName(),user.getPhoneNumber(),
								user.getAddress(),user.getGender(),user.getDateOfBirth());
								admin.setRoles(roles);
								administratorRepository.save(admin);
								break;
							case "ROLE_STUDENT":
								Student student = new Student(user.getUsername(), user.getEmail(),
										user.getPassword(),user.getFirstName(),user.getLastName(),user.getPhoneNumber(),
										user.getAddress(),user.getGender(),user.getDateOfBirth());
								student.setRoles(roles);
								studentRepository.save(student);
								break;
							case "ROLE_TEACHER":
								Teacher teacher = new Teacher(user.getUsername(), user.getEmail(),
										user.getPassword(),user.getFirstName(),user.getLastName(),user.getPhoneNumber(),
										user.getAddress(),user.getGender(),user.getDateOfBirth());
								teacher.setRoles(roles);
								teacherRepository.save(teacher);
								break;
							case "ROLE_PARENT":
								Parent parent = new Parent(user.getUsername(), user.getEmail(),
										user.getPassword(),user.getFirstName(),user.getLastName(),user.getPhoneNumber(),
										user.getAddress(),user.getGender(),user.getDateOfBirth());
								parent.setRoles(roles);
								parentRepository.save(parent);
								break;
							default:
								user.setRoles(roles);
								userRepository.save(user);
						}
					}

			);

		}
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@GetMapping("/logout")
	public void logout(HttpServletRequest request, HttpServletResponse response) {

		SecurityContextHolder.clearContext();

	   /*	Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		*/
	}
}