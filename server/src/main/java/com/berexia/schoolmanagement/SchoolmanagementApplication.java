package com.berexia.schoolmanagement;

import com.berexia.schoolmanagement.model.*;
import com.berexia.schoolmanagement.repository.*;

import com.berexia.schoolmanagement.upload_files.FileStorageProperties;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
@EnableSwagger2
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class SchoolmanagementApplication implements CommandLineRunner {
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private TeacherRepository teacherRepository;
	@Autowired
	private ParentRepository parentRepository;
	@Autowired
	private LevelRepository levelRepository;
	@Autowired
	private NoteRepository noteRepository;
	@Autowired
	private CourseRepository courseRepository;
	@Autowired
	private TagRepository tagRepository;
	@Autowired
	private SubjectRepository subjectRepository;
	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private ForumRepository forumRepository;
	@Autowired
	private AnswerRepository answerRepository;
	@Autowired
	private AdministratorRepository administratorRepository;
	@Autowired
	private GroupRepository groupRepository;
	@Autowired
	PasswordEncoder encoder;

	public static void main(String[] args) {
		ApplicationContext ctx=SpringApplication.run(SchoolmanagementApplication.class, args);
	}


	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}

	@Override
	public void run(String... args) throws Exception {
		Role roleAdmin=new Role(null,ERole.ROLE_ADMIN);
		Role roleStudent=new Role(null,ERole.ROLE_STUDENT);
		Role roleTeacher=new Role(null,ERole.ROLE_TEACHER);
		Role roleParent=new Role(null,ERole.ROLE_PARENT);

		Set<Role>  roles1=new HashSet<>();
		roles1.add(roleStudent);

		Set<Role>  roles2=new HashSet<>();
		roles2.add(roleAdmin);

		Set<Role>  roles3=new HashSet<>();
		roles3.add(roleTeacher);

		Set<Role>  roles4=new HashSet<>();
		roles4.add(roleParent);

		Level levelTc=new Level(null,"TC Scientifique",null);
		Parent parent=new Parent(null,"parent","parent","060088775544","parent.parent@gmail.com",
				"Casa",null,new Date(),"parent nz",encoder.encode("12345678"),"parent",roles4,null);
		GroupExcellence group1=new GroupExcellence(null,"group 1",25,null,null);
        User user=new User("user","user","user");
        Student najat=new Student(null,"najat","elhabti","060088775544","najat.elhabti@gmail.com",
				"Casa",null,new Date(),"najat el",encoder.encode("12345678"),"student",roles1,null,
				"1513030190",levelTc, parent,group1);

		Administrator anasse =new Administrator(null,"anasse","nazih","060088775544","anasse.nazih@gmail.com",
				"Casa",null,new Date(),"anasse nz",encoder.encode("12345678"),"admin",roles2,null);
		Teacher jamila=new Teacher(null,"Jamila","akharraz","060088775544","Jamila.akharraz@gmail.com",
				"Casa",null,new Date(),"Jamila akh",encoder.encode("12345678"),"teacher",roles3,null);


		Forum forum=new Forum(null,"Maths",anasse,null,null);
		Course arithmetique=new Course(null,"arithmétique dans z",forum,null,null,
				null,null,jamila,null,null);
		Question question=new Question(null,"this is a question",new Date(),null,arithmetique,null,null,null,najat);
		Answer answer=new Answer(null,"this is an answer",new Date(),question,null,null,null ,najat);

		Note note1=new Note(null,(float)16,"tres bien",najat,arithmetique);
		Schedule arithmetiqueSchedule=new Schedule(null,new Date(), (double) 2,group1,null,null,null);

		/*roleRepository.save(roleAdmin);
		roleRepository.save(roleStudent);
		roleRepository.save(roleTeacher);
		roleRepository.save(roleParent);
		levelRepository.save(levelTc);
		parentRepository.save(parent);

		groupRepository.save(group1);
		studentRepository.save(najat);
		administratorRepository.save(anasse);
		teacherRepository.save(jamila);

		forumRepository.save(forum);
		courseRepository.save(arithmetique);

		questionRepository.save(question);
		answerRepository.save(answer);
		noteRepository.save(note1);*/
	}
}
