package com.berexia.schoolmanagement.excel_utils.service;

import com.berexia.schoolmanagement.model.Note;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ImportExcelServiceI {
    public List<Note> importExcelFile(MultipartFile files) throws IOException;
}
