package com.berexia.schoolmanagement.excel_utils.conrtoller;

import com.berexia.schoolmanagement.excel_utils.service.ImportExcelServiceI;
import com.berexia.schoolmanagement.model.Note;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin("*")
@RestController
public class ImportExcelController {
    @Autowired
    private ImportExcelServiceI importExcelService;

   @PostMapping(value = "/import-excel")
    public ResponseEntity<List<Note>> importExcelFile(@RequestParam("file") MultipartFile files) throws IOException {
       HttpStatus status = HttpStatus.OK;

       return new ResponseEntity<>(importExcelService.importExcelFile(files), status);
    }
}

