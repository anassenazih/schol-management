package com.berexia.schoolmanagement.excel_utils.service;

import com.berexia.schoolmanagement.model.Note;
import com.berexia.schoolmanagement.service.CourseServiceI;
import com.berexia.schoolmanagement.service.NoteServiceI;
import com.berexia.schoolmanagement.service.StudentServiceI;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImportExcelServiceImpl implements ImportExcelServiceI{
    @Autowired
    private CourseServiceI courseService;
    @Autowired
    private StudentServiceI studentService;
    @Autowired
    private NoteServiceI noteService;

    @Override
    public List<Note> importExcelFile(MultipartFile files) throws IOException {
        List<Note> notes = new ArrayList<>();

        XSSFWorkbook workbook = new XSSFWorkbook(files.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 0; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                Note note = new Note();

                XSSFRow row = worksheet.getRow(index);

                note.setNote((float) row.getCell(0).getNumericCellValue());
                note.setRemarque(row.getCell(1).getStringCellValue());
                Integer cne=(int) row.getCell(2).getNumericCellValue();
                note.setStudent(studentService.getStudentByCne(cne.toString()));
                note.setCourse(courseService.getCourseByName(row.getCell(3).getStringCellValue()));
                notes.add(note);
            }
        }

        return notes;
    }
}
