package com.berexia.schoolmanagement.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Question implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idQuestion;
	private String question;
	private Date dateQuestion;
	@JsonIgnore
	@OneToMany(mappedBy = "question",cascade = CascadeType.ALL)
	private List<Answer> answers;

	@ManyToOne
	@JoinColumn
	private Course course;
	private String url;
	private String type;
	private String name;

	@ManyToOne
	@JoinColumn
	private User user;
	

}
