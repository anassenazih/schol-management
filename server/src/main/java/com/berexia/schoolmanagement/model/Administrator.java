package com.berexia.schoolmanagement.model;

import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
//@AllArgsConstructor
@Data
public class Administrator extends User{
    public Administrator(Integer idUser, String firstName, String lastName, String phoneNumber, @NotBlank @Size(max = 50) @Email String email, String address, String gender, Date dateOfBirth, @NotBlank @Size(max = 20) String username, @NotBlank @Size(max = 120) String password, String occupation, Set<Role> roles, List<Registration> registrations) {
        super(idUser, firstName, lastName, phoneNumber, email, address, gender, dateOfBirth, username, password, occupation, roles, registrations,null,null,null);
    }

    public Administrator(String username, String email, String password) {
        super(username, email, password);
    }

    public Administrator(String username, String email, String password, String firstName, String lastName, String phoneNumber, String address, String gender, Date dateOfBirth) {
        super(username, email, password, firstName, lastName, phoneNumber, address, gender, dateOfBirth);
    }
}
