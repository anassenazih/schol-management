package com.berexia.schoolmanagement.model;
//
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Teacher extends User{

	@JsonIgnore
	@OneToMany(mappedBy = "teacher",cascade = CascadeType.ALL)
	private List<Course> courses;

/*	@ManyToMany
	private List<GroupExcellence> groupExcellences;
*/

	public Teacher(Integer idUser, String firstName, String lastName, String phoneNumber, @NotBlank @Size(max = 50) @Email String email, String address, String gender, Date dateOfBirth, @NotBlank @Size(max = 20) String username, @NotBlank @Size(max = 120) String password, String occupation, Set<Role> roles, List<Registration> registrations) {
		super(idUser, firstName, lastName, phoneNumber, email, address, gender, dateOfBirth, username, password, occupation, roles, registrations,null,null,null);
	}

	public Teacher(String username, String email, String password) {
		super(username, email, password);
	}

	public Teacher(String username, String email, String password, String firstName, String lastName, String phoneNumber, String address, String gender, Date dateOfBirth) {
		super(username, email, password, firstName, lastName, phoneNumber, address, gender, dateOfBirth);
	}
}
