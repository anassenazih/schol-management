package com.berexia.schoolmanagement.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Registration implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idRegistration;
	private Date registrationDate;

	//private String status;

	@ManyToOne
	@JoinColumn
	private User user;

	/*@ManyToOne
	@JoinColumn
	private Student student;*/

	@ManyToOne
	@JoinColumn
	private Course course;

}
