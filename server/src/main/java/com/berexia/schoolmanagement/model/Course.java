package com.berexia.schoolmanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Course implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCourse;
	private String courseName;

	@ManyToOne
	@JoinColumn
	private Forum forum;

	@JsonIgnore
	@OneToMany(mappedBy="course",cascade = CascadeType.ALL)
	private List<Tag> tags;

	@JsonIgnore
	@OneToMany(mappedBy="course",cascade = CascadeType.ALL)
	private List<Question> questions;

	@ManyToOne
	@JoinColumn
	private Subject subject;


	@JsonIgnore
	@OneToMany(mappedBy="course",cascade = CascadeType.ALL)
	private List<Registration> registrations;

	/*@OneToOne
	private Note note;*/

	@ManyToOne
	@JoinColumn
	private Teacher teacher;

	@JsonIgnore
	@OneToMany(mappedBy="course",cascade = CascadeType.ALL)
	private List<Note> notes;

	private String youtubeLink;

   /*@ManyToMany
	private List<Student> students;*/
  /* @JsonIgnore
   @OneToMany
   private List<Schedule> schedules;*/
}
