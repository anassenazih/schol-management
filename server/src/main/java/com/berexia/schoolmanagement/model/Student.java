package com.berexia.schoolmanagement.model;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
//@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Student extends User{

	private String CNE;

	@ManyToOne
	@JoinColumn
	private Level level;

	/*@ManyToMany
	private List<Forum> forums;*/

	@ManyToOne
	@JoinColumn(name = "parent_id_user")
	private Parent parent;

	@ManyToOne
	@JoinColumn
	private GroupExcellence groupExcellence;

	/*@JsonIgnore
	@OneToMany(mappedBy = "student",cascade = CascadeType.ALL)
	private List<Registration> registrations;*/

/*	@ManyToMany
	private List<Course> courses;
*/  @JsonIgnore
    @OneToMany(mappedBy = "student",cascade = CascadeType.ALL)
    private List<Note> notes;

	public Student(Integer idUser, String firstName, String lastName, String phoneNumber, @NotBlank @Size(max = 50) @Email String email, String address, String gender, Date dateOfBirth, @NotBlank @Size(max = 20) String username, @NotBlank @Size(max = 120) String password,
				   String occupation, Set<Role> roles, List<Registration> registrations) {
		super(idUser, firstName, lastName, phoneNumber, email, address, gender, dateOfBirth, username, password, occupation, roles, registrations,null,null,null);
	}

	public Student(Integer idUser, String firstName, String lastName, String phoneNumber, @NotBlank @Size(max = 50) @Email String email, String address, String gender, Date dateOfBirth, @NotBlank @Size(max = 20) String username, @NotBlank @Size(max = 120) String password, String occupation, Set<Role> roles, List<Registration> registrations, String CNE, Level level, Parent parent, GroupExcellence groupExcellence) {
		super(idUser, firstName, lastName, phoneNumber, email, address, gender, dateOfBirth, username, password, occupation, roles, registrations,null,null,null);
		this.CNE = CNE;
		this.level = level;
		this.parent = parent;
		this.groupExcellence = groupExcellence;
	}

	public Student(String username, String email, String password) {
		super(username, email, password);
	}

	public Student(String username, String email, String password, String firstName, String lastName, String phoneNumber, String address, String gender, Date dateOfBirth) {
		super(username, email, password, firstName, lastName, phoneNumber, address, gender, dateOfBirth);
	}
}
