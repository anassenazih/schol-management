package com.berexia.schoolmanagement.model;

public enum ERole {
	ROLE_USER,
    //ROLE_MODERATOR,
    ROLE_ADMIN,
    ROLE_STUDENT,
    ROLE_PARENT,
    ROLE_TEACHER
}