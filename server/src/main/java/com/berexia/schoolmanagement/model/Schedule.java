package com.berexia.schoolmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Schedule implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSchedule;
    private Date startOfCourse;
    private Double lengthOfCourse;
   /* @ManyToOne
    private Course course;*/
   @ManyToOne
   @JoinColumn
   private GroupExcellence groupExcellence;
   private String url;
   private String type;
    private String name;
}
