package com.berexia.schoolmanagement.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Subject implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSubject;
	private String subjectName;
	@JsonIgnore
	@OneToMany(mappedBy = "subject",cascade = CascadeType.ALL)
	private List<Course> courses;
    
}
