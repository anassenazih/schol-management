package com.berexia.schoolmanagement.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class GroupExcellence implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idGroup;
	private String name;
	private Integer nbrOfStudents;

	@JsonIgnore
	@OneToMany(mappedBy="groupExcellence",cascade = CascadeType.ALL)
	private List<Student> students;

	@JsonIgnore
	@OneToMany(mappedBy="groupExcellence",cascade = CascadeType.ALL)
	private List<Schedule> schedules;

}
