package com.berexia.schoolmanagement.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Entity
public class Answer implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idAnswer;
	private String answer;
	private Date dateAnswer;

	@ManyToOne
	@JoinColumn
	private Question question;
	private String url;
	private String type;
	private String name;

	@ManyToOne
	@JoinColumn
	private User user;

	public Answer() {
	}

}
