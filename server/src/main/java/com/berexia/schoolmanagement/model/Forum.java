package com.berexia.schoolmanagement.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Forum implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idForum;
	private String forumName;

	@ManyToOne
	@JoinColumn
	private Administrator administrator;

	@JsonIgnore
	@OneToMany(mappedBy="forum",cascade = CascadeType.ALL)
	private List<Course> courses;

	@JsonIgnore
	@OneToMany(mappedBy = "forum",cascade = CascadeType.ALL)
	private List<InvitationToForum> invitationToForums;

}
