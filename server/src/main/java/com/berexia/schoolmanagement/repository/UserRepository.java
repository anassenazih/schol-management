package com.berexia.schoolmanagement.repository;
//
//
import com.berexia.schoolmanagement.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import com.berexia.schoolmanagement.model.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer>{

    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);

    @Query(value="SELECT u.roles FROM  User u  WHERE u.idUser= :idUser")
    public List<Role> getRolesByIdUser(@Param("idUser") Integer idUser);

    @Query(value="FROM  User u  WHERE u.idUser= :id")
    public User getByIdUserCustomized(@Param("id") Integer id);

    @Query(value="FROM  User u  WHERE u.email= :email")
    public User getByEmailUser(@Param("email") String email);

}
