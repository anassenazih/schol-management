package com.berexia.schoolmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Level;

public interface LevelRepository extends JpaRepository<Level, Integer>{

}
