package com.berexia.schoolmanagement.repository;
///
import com.berexia.schoolmanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Registration;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RegistrationRepository extends JpaRepository<Registration, Integer>{

   // @Query(value="SELECT r.user FROM  Registration r  WHERE r.course.teacher.idUser=:id and r.status like '%accepted%' ")
   @Query(value="SELECT r.user FROM  Registration r  WHERE r.course.teacher.idUser=:id ")
    public List<User> getUsersByIdTeacher(@Param("id") Integer id);

}
