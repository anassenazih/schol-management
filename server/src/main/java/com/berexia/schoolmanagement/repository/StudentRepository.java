package com.berexia.schoolmanagement.repository;
//
import com.berexia.schoolmanagement.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StudentRepository extends JpaRepository<Student, Integer>{
    @Query(value="FROM  Student s  WHERE s.idUser= :id")
    public Student getByIdStudentCustomized(@Param("id") Integer id);

    @Query(value="FROM  Student s  WHERE s.CNE= :cne")
    public Student getStudentByCne(@Param("cne") String cne);
}
