package com.berexia.schoolmanagement.repository;
//
//
//
import com.berexia.schoolmanagement.model.ERole;
import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer>{
    Optional<Role> findByRole(ERole role);

}
