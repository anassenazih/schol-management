package com.berexia.schoolmanagement.repository;
//
import com.berexia.schoolmanagement.model.InvitationToForum;
import com.berexia.schoolmanagement.model.Registration;
import com.berexia.schoolmanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InvitationToForumRepository extends JpaRepository<InvitationToForum, Integer>{

}
