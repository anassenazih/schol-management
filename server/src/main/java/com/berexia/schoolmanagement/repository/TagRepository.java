package com.berexia.schoolmanagement.repository;
//
//
import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Tag;

public interface TagRepository extends JpaRepository<Tag, Integer>{

}
