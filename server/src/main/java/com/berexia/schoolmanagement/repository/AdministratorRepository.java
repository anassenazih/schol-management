package com.berexia.schoolmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Administrator;

public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

}
