package com.berexia.schoolmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Integer>{

}
