package com.berexia.schoolmanagement.repository;

import com.berexia.schoolmanagement.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Forum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ForumRepository extends JpaRepository<Forum, Integer>{

   @Query(value="SELECT c FROM  Course c   where c.forum.idForum = :id")
    public List<Course> getCoursesByIdForum(@Param("id") Integer id);

    @Query(value="FROM  Forum f   WHERE f.idForum= :id")
    public Forum getByIdForumCustomized(@Param("id") Integer id);

    @Query(value="SELECT invita.forum FROM  InvitationToForum invita   WHERE invita.user.idUser= :id")
    public List<Forum> getForumsByIdUser(@Param("id") Integer id);


}
