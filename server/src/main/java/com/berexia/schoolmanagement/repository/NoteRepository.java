package com.berexia.schoolmanagement.repository;

import com.berexia.schoolmanagement.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NoteRepository extends JpaRepository<Note, Integer> {

   /*  @Query(value="SELECT n FROM  Note n JOIN n.student s JOIN s.forums f where f.administrator.idUser = :adminId")
    public List<Note> getNotesByTeacher(@Param("adminId") Integer adminId);*/


   @Query(value="SELECT n FROM  Note n where n.course.teacher.idUser = :idTeacher")
    public List<Note> getNotesByIdTeacher(@Param("idTeacher") Integer idTeacher);

    @Query(value="SELECT n FROM  Note n where n.student.level.idLevel = :idLevel")
    public List<Note> getNotesByIdLevel(@Param("idLevel") Integer idLevel);

    @Query(value="SELECT n FROM  Note n where n.course.idCourse= :idCourse")
    public List<Note> getNotesByIdCourse(@Param("idCourse") Integer idCourse);

    @Query(value="SELECT n FROM  Note n where n.student.idUser= :idStudent")
    public List<Note> getNotesByIdStudent(@Param("idStudent")Integer idStudent);

    @Query(value="SELECT n FROM  Note n where n.student.groupExcellence.idGroup= :idGroup")
    public List<Note> getNotesByIdGroup(@Param("idGroup")Integer idGroup);

   @Query(value="SELECT n FROM  Note n where n.student.parent.idUser= :idParent")
    public List<Note> getNotesByIdParent(@Param("idParent")Integer idParent);

    @Query(value="SELECT n FROM  Note n where n.idNote= :id")
    public Note getByIdNoteCustomized(@Param("id")Integer id);
}
