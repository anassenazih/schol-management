package com.berexia.schoolmanagement.repository;
//
//
import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Integer>{

}
