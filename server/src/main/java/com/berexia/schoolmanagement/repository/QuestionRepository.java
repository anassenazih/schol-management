package com.berexia.schoolmanagement.repository;

import com.berexia.schoolmanagement.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Question;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Integer>{


    @Query(value="SELECT a FROM  Answer a  WHERE a.question.idQuestion= :id order by a.dateAnswer asc ")
    public List<Answer> getAnswersByIdQuestion(@Param("id") Integer id);
    @Query(value = " SELECt q FROM Question q order by q.dateQuestion desc")
    public List<Question> getQuesions();

}
