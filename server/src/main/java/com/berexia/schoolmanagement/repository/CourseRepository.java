package com.berexia.schoolmanagement.repository;
//

import com.berexia.schoolmanagement.dto.QuestionRequestDTO;
import com.berexia.schoolmanagement.model.Course;
import com.berexia.schoolmanagement.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Integer> {

    /*@Query(value="SELECT q.idQuestion as idQuestion,q.question as question,q.dateQuestion as dateQuestion," +
            "q.user as user,q.answers as answers FROM  Question q " +
            " WHERE q.course.idCourse= :id order by q.dateQuestion desc")*/
    @Query(value="SELECT q from Question q  WHERE q.course.idCourse= :id order by q.dateQuestion desc")
    public List<QuestionRequestDTO> getQuestionsByIdCourse(@Param("id") Integer id);

    @Query(value="FROM  Course c   WHERE c.idCourse= :id")
    public Course getByIdCourseCustomized(@Param("id") Integer id);

    @Query(value="SELECT c FROM  Course c  WHERE c.teacher.idUser= :id")
    public List<Course> getCoursesByIdTeacher(@Param("id") Integer id);

    @Query(value="SELECT r.course FROM  Registration r  WHERE r.course.forum.idForum=:idForum and r.user.idUser= :idUser")
    public List<Course> getCoursesByIdForumAndIdUser(@Param("idForum") Integer idForum,@Param("idUser") Integer idUser);

    @Query(value="SELECT c FROM  Course c  WHERE c.courseName= :courseName")
    public Course getCourseByName(@Param("courseName") String courseName);

}
