package com.berexia.schoolmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Integer>{

}
