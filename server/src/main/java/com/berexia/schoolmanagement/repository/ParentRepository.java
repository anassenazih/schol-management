package com.berexia.schoolmanagement.repository;

import com.berexia.schoolmanagement.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import com.berexia.schoolmanagement.model.Parent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ParentRepository extends JpaRepository<Parent, Integer>{
    @Query(value="FROM  Student s   WHERE s.parent.idUser= :id")
    public List<Student> getStudentsByIdParent(@Param("id") Integer id);
}
