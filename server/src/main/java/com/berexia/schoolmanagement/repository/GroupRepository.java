package com.berexia.schoolmanagement.repository;
//
import com.berexia.schoolmanagement.model.GroupExcellence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface GroupRepository extends JpaRepository<GroupExcellence, Integer>{
    @Query(value="SELECT distinct n.student.groupExcellence FROM  Note n  WHERE" +
            " n.course.teacher.idUser= :id ")
    List<GroupExcellence> getGroupsByIdTeacher(@Param("id") Integer id);
}
