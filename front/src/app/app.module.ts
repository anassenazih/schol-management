// Angular
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GestureConfig, MatProgressSpinnerModule } from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
// Angular in memory
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
// Perfect Scroll bar
import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
// SVG inline
import { InlineSVGModule } from 'ng-inline-svg';
// Env
import { environment } from '../environments/environment';
// Hammer JS
import 'hammerjs';
// NGX Permissions
import { NgxPermissionsModule } from 'ngx-permissions';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
// State
import { metaReducers, reducers } from './core/reducers';
// Copmponents
import { AppComponent } from './app.component';
// Modules
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './views/theme/theme.module';
// Partials
import { PartialsModule } from './views/partials/partials.module';
// Layout Services
import {
	DataTableService,
	FakeApiService,
	KtDialogService,
	LayoutConfigService,
	LayoutRefService,
	MenuAsideService,
	MenuConfigService,
	MenuHorizontalService,
	PageConfigService,
	SplashScreenService,
	SubheaderService
} from './core/_base/layout';
// Auth

// CRUD
import { HttpUtilsService, LayoutUtilsService, TypesUtilsService } from './core/_base/crud';
// Config
import { LayoutConfig } from './core/_config/layout.config';
// Highlight JS
import { HIGHLIGHT_OPTIONS, HighlightLanguage } from 'ngx-highlightjs';
import * as typescript from 'highlight.js/lib/languages/typescript';
import * as scss from 'highlight.js/lib/languages/scss';
import * as xml from 'highlight.js/lib/languages/xml';
import * as json from 'highlight.js/lib/languages/json';
import {AdminComponent} from './admin/admin.component';
import {UserServiceService} from "./services/user-service.service";
import { PostsComponent } from './forums/posts/posts.component';
import {MatIconModule} from "@angular/material/icon";
import { EmploiTempsComponent } from './emploi-temps/emploi-temps.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatButtonModule} from "@angular/material/button";
import { AddEmploiComponent } from './emploi-temps/add-emploi/add-emploi.component';
import {MatSelectModule} from "@angular/material/select";
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from "@angular/material/dialog";
import {NgxAgoraModule} from "ngx-agora";
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {DialogModule} from "primeng/dialog";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {AuthModule} from "./auth/auth.module";
import {AuthService} from "./services/auth.service";
import { StudentComponent } from './student/student.component';
import { TeacherComponent } from './teacher/teacher.component';
import { ParentComponent } from './parent/parent.component';
import {authInterceptorProviders} from "../_helpers/auth.interceptor";
import {ForumsComponent} from "./forums/forums.component";
import {
	ActionNotificationComponent,
	DeleteEntityDialogComponent,
	UpdateStatusDialogComponent
} from "./views/partials/content/crud";
import {JWT_OPTIONS, JwtHelperService} from "@auth0/angular-jwt";
import { MatiereComponent } from './matiere/matiere.component';
import { GroupComponent } from './group/group.component';
import { EditGroupComponent } from './group/edit-group/edit-group.component';
import {AddGroupComponent} from "./group/add-group/add-group.component";
import {MatInputModule} from "@angular/material/input";
import { AddMatiereComponent } from './matiere/add-matiere/add-matiere.component';
import { EditMatiereComponent } from './matiere/edit-matiere/edit-matiere.component';
import { EditEmploiComponent } from './emploi-temps/edit-emploi/edit-emploi.component';
import {EmploiModule} from "./emploi-temps/emploi.module";
import {MatiereModule} from "./matiere/matiere.module";
import {GroupModule} from "./group/group.module";

// the second parameter 'fr-FR' is optional
registerLocaleData(localeFr, 'fr-FR');
// tslint:disable-next-line:class-name
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	wheelSpeed: 0.5,
	swipeEasing: true,
	minScrollbarLength: 40,
	maxScrollbarLength: 300,
};

export function initializeLayoutConfig(appConfig: LayoutConfigService) {
	// initialize app by loading default demo layout config
	return () => {
		if (appConfig.getConfig() === null) {
			appConfig.loadConfigs(new LayoutConfig().configs);
		}
	};
}

export function hljsLanguages(): HighlightLanguage[] {
	return [
		{name: 'typescript', func: typescript},
		{name: 'scss', func: scss},
		{name: 'xml', func: xml},
		{name: 'json', func: json}
	];
}

@NgModule({
	declarations: [AppComponent,AdminComponent, StudentComponent, TeacherComponent, ParentComponent,],
	imports: [
		BrowserAnimationsModule,
		BrowserModule,
		AppRoutingModule,
		NgxAgoraModule.forRoot({AppID: 'a3a38710848f412eb961e0024b2afee7'}),
		HttpClientModule,
		NgxPermissionsModule.forRoot(),
		PartialsModule,
		OverlayModule,
		StoreModule.forRoot(reducers, {metaReducers}),
		EffectsModule.forRoot([]),
		StoreRouterConnectingModule.forRoot({stateKey: 'router'}),
		StoreDevtoolsModule.instrument(),
		AuthModule.forRoot(),
		TranslateModule.forRoot(),
		MatProgressSpinnerModule,
		InlineSVGModule.forRoot(),
		ThemeModule,
		MatIconModule,
		MatFormFieldModule,
		FormsModule,
		MatTableModule,
		MatPaginatorModule,
		MatButtonModule,
		MatSelectModule,
		MatDialogModule,
		ReactiveFormsModule,
		DialogModule,
		PdfViewerModule,
		MatInputModule,
		EmploiModule,
		MatiereModule,
		GroupModule

	],
	exports: [],
	entryComponents: [
		ActionNotificationComponent,
		DeleteEntityDialogComponent,
		UpdateStatusDialogComponent,
	],
	providers: [
		AuthService,
		LayoutConfigService,
		LayoutRefService,
		MenuConfigService,
		PageConfigService,
		KtDialogService,
		DataTableService,
		SplashScreenService,
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		},
		{
			provide: HAMMER_GESTURE_CONFIG,
			useClass: GestureConfig
		},
		{
			// layout config initializer
			provide: APP_INITIALIZER,
			useFactory: initializeLayoutConfig,
			deps: [LayoutConfigService], multi: true
		},
		{
			provide: HIGHLIGHT_OPTIONS,
			useValue: {languages: hljsLanguages}
		},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'kt-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		// template services
		SubheaderService,
		MenuHorizontalService,
		MenuAsideService,
		HttpUtilsService,
		TypesUtilsService,
		LayoutUtilsService,
		authInterceptorProviders,
		{ provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
		JwtHelperService

	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
