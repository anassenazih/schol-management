// Angular
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Components
import {BaseComponent} from './views/theme/base/base.component';
import {ErrorPageComponent} from './views/theme/content/error-page/error-page.component';
// Auth
import {AdminComponent} from './admin/admin.component';
import {EmploiTempsComponent} from "./emploi-temps/emploi-temps.component";
import {AddEmploiComponent} from "./emploi-temps/add-emploi/add-emploi.component";
import {TeacherComponent} from "./teacher/teacher.component";
import {StudentComponent} from "./student/student.component";
import {ParentComponent} from "./parent/parent.component";
import {
	AuthGuardService as AuthGuard
} from "./services/auth-guard-service.service";
import {
	RoleGuardService as RoleGuard
} from './services/role-guard-service.service';
import {GroupComponent} from "./group/group.component";
import {MatiereComponent} from "./matiere/matiere.component";
const routes: Routes = [
	{path: 'auth', loadChildren: () => import('app/auth/auth.module').then(m => m.AuthModule)},
	{
		path: 'admin',
		canActivate: [RoleGuard],
		data: {
			expectedRole: 'ROLE_ADMIN'
		},
		component: AdminComponent,
		children: [
			{
				path: 'dashboard',
				loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
			},
			{
				path:'visio-conf',
				loadChildren: () => import('app/visio-conference/visio-conference.module').then(m => m.VisioConferenceModule),
			},
			{
				path: 'forums',
				loadChildren: () => import('app/forums/forums.module').then(m => m.ForumsModule),
			},
			{
				path: 'user-management',
				loadChildren: () => import('app/admin/user-management/user-management.module').then(m => m.UserManagementModule),
			},
			{
				path: 'notes',
				loadChildren: () => import('app/notes/notes.module').then(m => m.NotesModule),
			},
			{
				path: 'emploi',
				component:EmploiTempsComponent
			},
			{
				path: 'group',
				component: GroupComponent
			},
			{
				path: 'matiere',
				component: MatiereComponent

			},
		]
	},
	{
		path: 'teacher',
		canActivate: [RoleGuard],
		data: {
			expectedRole: 'ROLE_TEACHER'
		},
		component: TeacherComponent,
		children: [
			{
				path: 'dashboard',
				loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
			},
			{
				path: 'forums',
				loadChildren: () => import('app/forums/forums.module').then(m => m.ForumsModule),
			},
			{
				path: 'notes',
				loadChildren: () => import('app/notes/notes.module').then(m => m.NotesModule),
			},
			{
				path: 'emploi',
				loadChildren: () => import('app/emploi-temps/emploi.module').then(m => m.EmploiModule),
			},
		]
	},
	{
		path: 'student',
		canActivate: [RoleGuard],
		data: {
			expectedRole: 'ROLE_STUDENT'
		},
		component: StudentComponent,
		children: [
			{
				path: 'dashboard',
				loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
			},
			{
				path: 'forums',
				loadChildren: () => import('app/forums/forums.module').then(m => m.ForumsModule),
			},
			{
				path: 'notes',
				loadChildren: () => import('app/notes/notes.module').then(m => m.NotesModule),
			},
			{
				path: 'emploi',
				loadChildren: () => import('app/emploi-temps/emploi.module').then(m => m.EmploiModule),
			},
		]
	},
	{
		path: 'parent',
		canActivate: [RoleGuard],
		data: {
			expectedRole: 'ROLE_PARENT'
		},
		component: ParentComponent,
		children: [
			{
				path: 'dashboard',
				loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
			},
			{
				path: 'forums',
				loadChildren: () => import('app/forums/forums.module').then(m => m.ForumsModule),
			},
			{
				path: 'notes',
				loadChildren: () => import('app/notes/notes.module').then(m => m.NotesModule),
			},
			{
				path: 'emploi',
				loadChildren: () => import('app/emploi-temps/emploi.module').then(m => m.EmploiModule),
			},
		]
	},
	//{path: 'error/:type', component: ErrorPageComponent},
	//{path: '', redirectTo: 'dashboard', pathMatch: 'full'},
	//{path: '**', redirectTo: 'dashboard', pathMatch: 'full'},
	//  {
	// 	path: ' ',
	// 	component: BaseComponent,
	// 	canActivate: [AuthGuard],
	// 	children: [
	// 		{
	// 			path: 'dashboard',
	// 			loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
	// 		},
	// 		{
	// 			path: 'mail',
	// 			loadChildren: () => import('app/views/pages/apps/mail/mail.module').then(m => m.MailModule),
	// 		},
	// 		{
	// 			path: 'ecommerce',
	// 			loadChildren: () => import('app/views/pages/apps/e-commerce/e-commerce.module').then(m => m.ECommerceModule),
	// 		},
	// 		{
	// 			path: 'ngbootstrap',
	// 			loadChildren: () => import('app/views/pages/ngbootstrap/ngbootstrap.module').then(m => m.NgbootstrapModule),
	// 		},
	// 		{
	// 			path: 'material',
	// 			loadChildren: () => import('app/views/pages/material/material.module').then(m => m.MaterialModule),
	// 		},
	// 		{
	// 			path: 'user-management',
	// 			loadChildren: () => import('app/views/pages/user-management/user-management.module').then(m => m.UserManagementModule),
	// 		},
	// 		{
	// 			path: 'wizard',
	// 			loadChildren: () => import('app/views/pages/wizard/wizard.module').then(m => m.WizardModule),
	// 		},
	// 		{
	// 			path: 'builder',
	// 			loadChildren: () => import('app/views/theme/content/builder/builder.module').then(m => m.BuilderModule),
	// 		},
	//
	// 		{
	// 			path: 'error/403',
	// 			component: ErrorPageComponent,
	// 			data: {
	// 				type: 'error-v6',
	// 				code: 403,
	// 				title: '403... Access forbidden',
	// 				desc: 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator',
	// 			},
	// 		},
	// 		{path: 'error/:type', component: ErrorPageComponent},
	// 		{path: '', redirectTo: 'dashboard', pathMatch: 'full'},
	// 		{path: '**', redirectTo: 'dashboard', pathMatch: 'full'},
	// 	],
	// },



	{path: '**', redirectTo: 'error/403', pathMatch: 'full'},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {
}
