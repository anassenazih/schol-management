// NGRX
import { routerReducer } from '@ngrx/router-store';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from '../../../environments/environment';
import {notesreducer} from "../../notes/store/notes.reducer";
import {reducer} from "../../forums/store/forums.reducer";
import {coursreducer} from "../../forums/store/cours.reducer";
import {emploireducer} from "../../emploi-temps/store/emploi.reducer";
import {answerreducer} from "../../forums/store/answer.reducer";
import {questionreducer} from "../../forums/store/question.reducer";
import {groupreducer} from "../../group/store/group.reducer";
import {matierereducer} from "../../matiere/store/matiere.reducer";

// tslint:disable-next-line:no-empty-interface
export interface AppState {
    notes,
    forums,
    cours,
    emploi,
    answers,
    questions,
    groups,
    matiere,
    coursbyId
}

export const reducers: ActionReducerMap<AppState> = {
 notes: notesreducer,
    forums: reducer,
    cours:coursreducer,
    emploi:emploireducer,
    answers:answerreducer,
    questions:questionreducer,
    groups:groupreducer,
    matiere:matierereducer,
    coursbyId:coursreducer

};

export const metaReducers: Array<MetaReducer<AppState>> = !environment.production ? [storeFreeze] : [];
