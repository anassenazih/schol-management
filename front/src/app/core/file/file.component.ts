import {ChangeDetectorRef, Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FileServiceService} from "../../services/file-service.service";
import {Observable} from "rxjs";
import { HttpEventType, HttpResponse } from '@angular/common/http';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit {
  selectedFiles: FileList;
  currentFile: File;
  progress = 0;
  message = '';
  private file: any;
    private er: any;

  constructor(public dialogRef: MatDialogRef<FileComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private fileService: FileServiceService,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {

  }

  selectFile(event) {
      //this.selectedFiles = event.target.files;
      let reader = new FileReader();

      if (event.target.files && event.target.files.length) {
          const [file] = event.target.files;
          reader.readAsDataURL(file);
          console.log('file',file);
          reader.onload = () => {
              this.formGroup.patchValue({
                  file: file
              });
              // need to run CD since file load runs outside of zone
              this.cd.markForCheck();
            console.log(event.target.files.item(0));
          }
      }
  }

    formGroup = this.fb.group({
        file: [null, Validators.required]
    });


  upload() {
      console.log(this.formGroup.get('file').value);
      this.fileService.uploadFile(this.formGroup.get('file').value).subscribe(
              res => {
                  if(!res){
                      this.dialogRef.close({
                          data :  null,
                          type : null,
                          url  : null

                      });
                  }
                  this.er=res;
                  this.dialogRef.close({
                      data :  this.er.fileName,
                      type : this.er.fileType,
                      url  : this.er.fileDownloadUri

                  });
      }

              );

    // if (this.selectedFiles != null) {
    //   {
    //     this.currentFile = this.selectedFiles.item(0);
    //     console.log(this.currentFile);
    //     this.fileService.uploadFile(this.currentFile).subscribe(
    //         res => {
    //             this.er=res;
    //           this.dialogRef.close({
    //           data :  this.er.fileName,u
    //           type : this.er.fileType,
    //           url  : this.er.fileDownloadUri
    //
    //           });
    //         }
    //         );
    //   }
    //
    //   this.selectedFiles = undefined;
   // }
  }
}
