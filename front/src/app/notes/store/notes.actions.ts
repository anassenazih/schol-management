import { createAction, props } from '@ngrx/store';

export const loadNotes = createAction(
  '[Notes] Load Notes',
    props<{ request?: any }>()
);

export const loadNotesSuccess = createAction(
  '[Notes] Load Notes Success',
  props<{ notes: any }>()
);

export const loadNotesFailure = createAction(
  '[Notes] Load Notes Failure',
  props<{ error: any }>()
);

export const addNote = createAction(
  '[Notes] add note',
  props<{ request: any }>()
);

export const deleteNote = createAction(
  '[Notes] delete note',
  props<{ id: any }>()
);
