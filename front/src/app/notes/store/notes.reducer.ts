import { Action, createReducer, on } from '@ngrx/store';
import * as notesActions from './notes.actions';


export const notesFeatureKey = 'notes';

export interface State {
  notes: any;
  loaded: boolean;
}

export const initialState: State = {
  notes: [],
  loaded: false,
};

const notesReducer = createReducer(
    initialState,
    on(notesActions.loadNotes, (state, {request}) => ({
        ...state,
        request: request,
        loaded: false,
        loading: true
    })),
    on(notesActions.loadNotesSuccess, (state, {notes}) => ({
        ...state,
        notes: notes,
        loaded: false,
        loading: false
    })),
    on(notesActions.loadNotesFailure, (state, {error}) => ({
        notes: [],
        loaded: false,
        loading: false
    })),
);

export function notesreducer(state: State | undefined, action: Action) {
  return notesReducer(state, action);
}
