import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from "rxjs/operators";
import {of} from "rxjs/internal/observable/of";
import {loadNotes, loadNotesFailure, loadNotesSuccess} from "./notes.actions";
import {NotesService} from "../../services/notes.service";



@Injectable()
export class NotesEffects {



  constructor(private actions$: Actions, private notesService: NotesService) {}

    /**
     * ################## Notes effects  ##################
     */
    notesList$ = createEffect(() =>
        this.actions$.pipe(
            ofType('[Notes] Load Notes'),
            exhaustMap((action: any) => {
                    if (action.request) {
                        if (localStorage.getItem('role').includes('ROLE_ADMIN')){
                            return this.notesService.getNotesByCourse(action.request).pipe(
                                map(notes => loadNotesSuccess({notes: JSON.parse(JSON.stringify(notes))})),
                                catchError(error => of(loadNotesFailure({error: error}))
                                )
                            )
                        } else if (localStorage.getItem('role').includes('ROLE_STUDENT')){
                            return this.notesService.getNotesByCourse(action.request).pipe(
                                map(notes => loadNotesSuccess({notes: JSON.parse(JSON.stringify(notes))})),
                                catchError(error => of(loadNotesFailure({error: error}))
                                )
                            )
                        } else if (localStorage.getItem('role').includes('ROLE_TEACHER')){
                            return this.notesService.getNotesByCourse(action.request).pipe(
                                map(notes => loadNotesSuccess({notes: JSON.parse(JSON.stringify(notes))})),
                                catchError(error => of(loadNotesFailure({error: error}))
                                )
                            )
                        }
                        } else if (localStorage.getItem('role').includes('ROLE_PARENT')){
                            return this.notesService.getNotesByStudent(action.request).pipe(
                                map(notes => loadNotesSuccess({notes: JSON.parse(JSON.stringify(notes))})),
                                catchError(error => of(loadNotesFailure({error: error}))
                                )
                            )
                        }
                }
            )));

    addNote$ = createEffect(() =>
        this.actions$.pipe(
            ofType('[Notes] add note'),
            exhaustMap((action: any) =>
                this.notesService.addNote(action.request).pipe(
                    map(notes => {
                        console.log('note dans effect', notes)
                        return loadNotes({request: Object.assign(notes).course.idCourse})
                    })
                )
            )));

    removeTeam$ = createEffect(() =>
        this.actions$.pipe(
            ofType('[Notes] delete note'),
            exhaustMap(action =>
                this.notesService.deleteNote(Object.assign(action).id).pipe(
                    map(notes => {
                        return loadNotes({request: {}})
                    })
                )
            )));


}
