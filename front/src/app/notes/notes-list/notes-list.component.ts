import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort} from "@angular/material";
import {LayoutUtilsService, MessageType, QueryParamsModel} from "../../core/_base/crud";
import {SelectionModel} from "@angular/cdk/collections";

import { Subscription} from "rxjs/index";
import { Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";
import {MatTableDataSource} from "@angular/material/table";
import {RoleServiceService} from "../../services/role-service.service";
import {SubheaderService} from "../../core/_base/layout";
import {UserServiceService} from "../../services/user-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NotesService} from "../../services/notes.service";
import {AddNoteComponent} from "../add-note/add-note.component";
import {User} from "../../_models/user.model";
import {Role} from "../../_models/role.model";
import {loadNotes} from "../store/notes.actions";
import {ImportFromExcelComponent} from "../import-from-excel/import-from-excel.component";

@Component({
  selector: 'notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent implements OnInit {
    role = localStorage.getItem('role');
    courses = [];
    students: any[] = [];
    selectedCourse = '1';
    listData: MatTableDataSource<any>;
        displayedColumns = ['select', 'Course', 'Note' , 'remarque', 'actions'];
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild('sort1', {static: true}) sort: MatSort;
    // Filter fields
    @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
    lastQuery: QueryParamsModel;
    // Selection
    selection = new SelectionModel<User>(true, []);
    usersResult: User[] = [];
    studentsRef = [];
    allRoles: Role[] = [];
    hasItems = false;
    searchKey: string;
    // Subscriptions
    private subscriptions: Subscription[] = [];
    private users: User[];
    private notes: any[];
    private notesSource: any[] = [];
    private roles: Array<number>;
    private devicesJson: Object;
    private titles: string[] = [];


    /**
     *
     * @param activatedRoute: ActivatedRoute
     * @param store: Store<AppState>
     * @param router: Router
     * @param layoutUtilsService: LayoutUtilsService
     * @param subheaderService: SubheaderService
     */
    constructor(
        private userService : UserServiceService,
        private notesService : NotesService,
        private roleService : RoleServiceService,
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>,
        public dialog: MatDialog,
        private router: Router,
        private layoutUtilsService: LayoutUtilsService,
        private subheaderService: SubheaderService,
        private cdr: ChangeDetectorRef) {
        if(localStorage.getItem('role').includes('ROLE_ADMIN')){
            this.notesService.getCourses().subscribe(data => {
                this.courses = Object.assign(data);
                if(this.courses.length>0){
                }
            }, ()=>{}, ()=>{
            });
        }

        this.store.dispatch(loadNotes({request: this.selectedCourse}));

        this.store.select(state => state.notes).subscribe(data => {
            console.log('data ==', data.notes)
                this.studentsRef = JSON.parse(JSON.stringify(data.notes));
                this.students =[];
                this.studentsRef.forEach(e => {
                    this.students.push(
                        {firstName: e.student.firstName,
                            lastName:  e.student.lastName,
                            note: e.note,
                            remarque: e.remarque,
                            id: e.idNote
                        }
                    )
                })
            this.cdr.markForCheck();

        });
    }

    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */

    /**
     * On init
     */
    ngOnInit() {
        this.subheaderService.setTitle(' ');
    }

    /**
     * On Destroy
     */
    ngOnDestroy() {
        this.subscriptions.forEach(el => el.unsubscribe());
    }


    /** FILTRATION */
    filterConfiguration(): any {
        const filter: any = {};
        const searchText: string = this.searchInput.nativeElement.value;

        filter.lastName = searchText;

        filter.username = searchText;
        filter.email = searchText;
        filter.fillname = searchText;
        return filter;
    }



    onSearchClear(){
        this.searchKey="";
        this.applyFilter();
    }
    applyFilter(){
        this.listData.filter = this.searchKey.trim().toLowerCase();
    }
    /**
     * Fetch selected rows
     */
    fetchUsers() {
        const messages = [];
        this.selection.selected.forEach(elem => {
            messages.push({
                text: `${elem.lastName}, ${elem.email}`,
                id: elem.idUser.toString(),
                status: elem.username
            });
        });
        this.layoutUtilsService.fetchElements(messages);
    }

    /**
     * Check all rows are selected
     */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.usersResult.length;
        return numSelected === numRows;
    }

    /**
     * Toggle selection
     */
    masterToggle() {
        if (this.selection.selected.length === this.usersResult.length) {
            this.selection.clear();
        } else {
            this.usersResult.forEach(row => this.selection.select(row));
        }
    }


    addCours() {
        const _saveMessage = `Ajouté avec succès.`;
        const _messageType = MessageType.Create;
        const dialogRef = this.dialog.open(AddNoteComponent, {data: {}});
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }

           // this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

        });
    }

    importFromExcel() {
        const _saveMessage = `Ajouté avec succès.`;
        const _messageType = MessageType.Create;
        const dialogRef = this.dialog.open(ImportFromExcelComponent, {data: {}});
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }

           // this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);
        });
    }
    courseChanged(){
            this.store.dispatch(loadNotes({request: this.selectedCourse}));
    }

    onRowEditInit(car: any) {
    }

    onRowEditSave(car: any) {

    }

    onRowEditCancel(car: any, index: number) {

    }
}
