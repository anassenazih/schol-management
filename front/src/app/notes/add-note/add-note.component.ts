import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AppState} from "../../core/reducers";
import {FormControl, FormGroup} from "@angular/forms";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs/index";
import {CoursServiceService} from "../../services/cours-service.service";
import {Cours} from "../../_models/cours.model";
import {NotesService} from "../../services/notes.service";
import {addNote} from "../store/notes.actions";

@Component({
  selector: 'add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {
    selectedCourse = '';
    selectedGroupe = '';
    students: any[] = [];
    studentsRef = [];
    courses = [];
    groupes = [];
    cours: Cours;
    hasFormErrors = false;
    viewLoading = false;
    loadingAfterSubmit = false;
    private addedCours: Object;

    constructor(public dialogRef: MatDialogRef<AddNoteComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private store: Store<AppState>,
                private coursService : CoursServiceService,
                private noteService: NotesService
                ) {
        this.noteService.getCourses().subscribe(data => {
            this.courses = Object.assign(data);
            if(this.courses.length>0){
                // this.selectedCourse = this.courses[0].idCourse;
            }
        }, ()=>{}, ()=>{
        });

        this.noteService.getGroupes().subscribe(data => {
            this.groupes = Object.assign(data);
        }, ()=>{}, ()=>{
        });

        this.noteService.getStudents().subscribe(data => {
            this.studentsRef =Object.assign(data);
            this.studentsRef.forEach(e=>{
                console.log(e);
                this.students.push({id: e.idUser, firstName: e.firstName, lastName: e.lastName, note: '', remarque: ''})
            })
        });
    }

    ngOnInit() {
    }

    form : FormGroup = new FormGroup({
        idCourse: new FormControl(null),
        courseName: new FormControl(''),
    });

    /**
     * Save data
     */
    onSubmit() {
        const _cours = new Cours();
        _cours.idCourse = null;
        _cours.courseName = this.form.get('courseName').value;
        this.coursService.addCours(_cours).subscribe(data => {
            this.addedCours = data;
            this.viewLoading = false;
            this.coursService.callComponentMethod();
            this.dialogRef.close({
                isEdit: false
            });
        }, error1 => {
            console.log(error1)
        });
    }

    /**
     * Close alert
     *
     * @param $event: Event
     */
    onAlertClose($event) {
        this.hasFormErrors = false;
    }

    validateNotes(){
        this.students.forEach(e=> {

            this.store.dispatch(addNote({request: {
                    idNote: 0,
                    idStudent: e.id,
                    idCourse: this.selectedCourse,
                    note: e.note,
                    remarque: e.remarque
                }}))
        });
        //this.noteService.callComponentMethod();
        this.dialogRef.close({
            isEdit: false
        });

    }

}
