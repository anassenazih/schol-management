import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotesRoutingModule } from './notes-routing.module';
import { NotesListComponent } from './notes-list/notes-list.component';
import {
    MatAutocompleteModule, MatButtonModule, MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule, MatDialogModule, MatExpansionModule, MatIconModule,
    MatInputModule,
    MatMenuModule, MatNativeDateModule, MatPaginatorModule, MatProgressBarModule,
    MatProgressSpinnerModule, MatRadioModule,
    MatSelectModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule, MatTooltipModule
} from "@angular/material";
import {TranslateModule} from "@ngx-translate/core";
import {MatGridListModule} from "@angular/material/grid-list";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatToolbarModule} from "@angular/material/toolbar";
import {PartialsModule} from "../views/partials/partials.module";
import {NotesService} from "../services/notes.service";
import { AddNoteComponent } from './add-note/add-note.component';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import { EffectsModule } from '@ngrx/effects';
import { NotesEffects } from './store/notes.effects';
import { ImportFromExcelComponent } from './import-from-excel/import-from-excel.component';


@NgModule({
  declarations: [NotesListComponent, AddNoteComponent, ImportFromExcelComponent],
    providers: [NotesService],
  imports: [
    CommonModule,
    NotesRoutingModule,
      TableModule,
      PartialsModule,
      FormsModule,
      ReactiveFormsModule,
      TranslateModule.forChild(),
      MatButtonModule,
      MatMenuModule,
      MatSelectModule,
      MatInputModule,
      MatTableModule,
      MatAutocompleteModule,
      MatRadioModule,
      MatIconModule,
      MatNativeDateModule,
      MatProgressBarModule,
      MatDatepickerModule,
      MatCardModule,
      MatPaginatorModule,
      MatSortModule,
      MatCheckboxModule,
      MatProgressSpinnerModule,
      MatSnackBarModule,
      MatExpansionModule,
      MatTabsModule,
      MatTooltipModule,
      MatDialogModule,
      MatGridListModule,
      MatToolbarModule,
      DropdownModule,
      EffectsModule.forFeature([NotesEffects])
  ],
    entryComponents: [AddNoteComponent, ImportFromExcelComponent]
})
export class NotesModule { }
