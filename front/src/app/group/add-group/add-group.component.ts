import {Component, Inject, OnInit} from '@angular/core';
import {Role} from "../../_models/role.model";
import {Observable, Subscription} from "rxjs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";
import {RoleServiceService} from "../../services/role-service.service";
import {GroupServiceService} from "../../services/group-service.service";
import {Group} from "../../_models/group.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Cours} from "../../_models/cours.model";

@Component({
  selector: 'add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.scss']
})
export class AddGroupComponent implements OnInit {


  // Public properties
  group: Group;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  group$: Observable<any>;
  // Private properties
  private componentSubscriptions: Subscription;
  private addedGroup: any;


  /**
   * Component constructor
   *
   * @param dialogRef
   * @param data: any
   * @param store: Store<AppState>
   */
  constructor(public dialogRef: MatDialogRef<AddGroupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private groupService : GroupServiceService) {

  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    this.groupService.getAllGroup();


  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    if (this.componentSubscriptions) {
      this.componentSubscriptions.unsubscribe();
    }
  }



  form : FormGroup = new FormGroup({
    idGroup: new FormControl(null),
    name: new FormControl('',Validators.required),
    nbrOfStudents : new FormControl(''),
  });

  /**
   * Save data
   */
  onSubmit() {
    if (this.form.valid) {
      this.group = new Group();
      this.group.idGroup = null;
      this.group.name = this.form.get('name').value;
      this.group.nbrOfStudents = this.form.get('nbrOfStudents').value;
      this.groupService.addGroup(this.group).subscribe(data => {
        this.addedGroup = data;
        this.viewLoading = false;
        this.dialogRef.close({});

      }, error1 => {
        console.log(error1)
      });
      this.form.reset();
    }
  }



  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }



  /** UI */
  /**
   * Returns component title
   */
  getTitle(): string {

    return `Modifier group '${this.data.group}'`;

  }

  /**
   * Returns is title valid
   */
  isTitleValid(): boolean {
    return (this.data.group && this.data.group.length > 0);
  }

}
