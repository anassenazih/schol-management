import {Component, Inject, OnInit} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";
import {RoleServiceService} from "../../services/role-service.service";
import {Group} from "../../_models/group.model";
import {GroupServiceService} from "../../services/group-service.service";

@Component({
  selector: 'edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.scss']
})
export class EditGroupComponent implements OnInit {


  // Public properties
  group: Group;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  group$: Observable<any>;
  // Private properties
  private componentSubscriptions: Subscription;
  private editedGroup: Object;
  private editRole: any;

  /**
   * Component constructor
   *
   * @param dialogRef: MatDialogRef<RoleAddComponent>
   * @param data: any
   * @param store: Store<AppState>
   */
  constructor(public dialogRef: MatDialogRef<EditGroupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private groupService : GroupServiceService) {

  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    if (this.componentSubscriptions) {
      this.componentSubscriptions.unsubscribe();
    }
  }




  /**
   * Save data
   */
  onSubmit() {
    this.hasFormErrors = false;
    this.loadingAfterSubmit = false;
    /** check form */
    if (!this.isTitleValid()) {
      this.hasFormErrors = true;
      return;
    }
    this.group=new Group();
    this.group.idGroup=this.data.groupId;
    this.group.name= this.data.name;
    this.group.nbrOfStudents=this.data.nbrOfStudents;
    this.groupService.updateGroup(this.group).subscribe(data => {
      this.editedGroup = data;
      console.log('ediit',this.editedGroup);
      this.viewLoading = false;
      this.dialogRef.close({
        isEdit: true
      });
    }, error1 => {
      console.log(error1)
    });


  }





  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }



  /** UI */
  /**
   * Returns component title
   */
  getTitle(): string {

    return `Modifier group '${this.data.name}'`;

  }

  /**
   * Returns is title valid
   */
  isTitleValid(): boolean {
    return (this.data.name && this.data.name.length > 0);
  }

}
