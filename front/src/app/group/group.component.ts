import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {LayoutUtilsService, MessageType} from "../core/_base/crud";
import {SubjectServiceService} from "../services/subject-service.service";
import {TokenStorageService} from "../services/token-storage.service";
import {GroupServiceService} from "../services/group-service.service";
import {Cours} from "../_models/cours.model";
import {AddGroupComponent} from "./add-group/add-group.component";
import {Role} from "../_models/role.model";
import {Group} from "../_models/group.model";
import {EditGroupComponent} from "./edit-group/edit-group.component";
import {Store} from "@ngrx/store";
import {AppState} from "../core/reducers";
import {loadGroups} from "./store/group.actions";
import {loadMatieres} from "../matiere/store/matiere.actions";
import {loadEmplois} from "../emploi-temps/store/emploi.actions";


@Component({
  selector: 'group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  // Table fields
  listData:MatTableDataSource<any>;
  schedules=[];
  displayedColumns = [ 'groupe', 'nbr','actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  private searchKey: string;
  displayPdf = false;
  hasItems :boolean; // Need to show message: 'No records found'
  private file: any;
  private url: any;
  private role: any;
  private group=[];
  private allGroup=[];
  constructor(		public dialog: MatDialog,
                      public snackBar: MatSnackBar,
                      private layoutUtilsService: LayoutUtilsService,
                      private store: Store<AppState>,
                      private groupService : GroupServiceService,
                      private tokenStorageService:TokenStorageService,
                      private cdr: ChangeDetectorRef) {
    this.groupService.componentMethodCalled$.subscribe(
        () => {
          this.ngOnInit();
        }
    );
   // this.store.dispatch(loadGroups({request: this.group}));
  }

  ngOnInit() {
    this.store.dispatch(loadGroups({request: this.schedules}));
    this.store.select(state => state.groups).subscribe(data => {
      this.schedules = JSON.parse(JSON.stringify(data.groups));
      this.listData = new MatTableDataSource(this.schedules);
      this.listData.sort = this.sort;
      this.listData.paginator=this.paginator;
      this.cdr.markForCheck();
    });
    this.role = this.tokenStorageService.getUser().roles;
  }

  onSearchClear(){
    this.searchKey="";
    this.applyFilter();
  }
  applyFilter(){
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }



  editGroup(group: Group) {

    const _saveMessage = `Modifié avec succès.`;
    const _messageType =  MessageType.Update;
    const dialogRef = this.dialog.open(EditGroupComponent, { data: { groupId: group.idGroup, name : group.name,
        nbrOfStudents :group.nbrOfStudents} });
    dialogRef.afterClosed().subscribe(res => {
      this.groupService.callComponentMethod();
      if (!res) {
        return;
      }
      this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

    });

  }
  addGroup() 	{

    const _saveMessage = `Ajouté avec succès.`;
    const _messageType =  MessageType.Create;
    const dialogRef = this.dialog.open(AddGroupComponent, { data: {  } });
    dialogRef.afterClosed().subscribe(res => {
      this.groupService.callComponentMethod();
      if (!res) {
        return;
      }
      this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

    });

  }


  deleteGroup(idGroup: number){
    const _cours = 'Groupe' ;
    const _description = 'Vous êtes sûr de vouloir supprimer ce groupe ?';
    const _waitDesciption = 'Suppression de groupe  ...';
    const _deleteMessage = `le groupe a été supprimé`;

    const dialogRef = this.layoutUtilsService.deleteElement(_cours , _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {

      if (res) {
        this.groupService.deleteGroup(idGroup).subscribe(data=>(this.ngOnInit()));
        this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
      }


    });
  }


}
