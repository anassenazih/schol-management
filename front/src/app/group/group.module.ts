// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// Translate
import { TranslateModule } from '@ngx-translate/core';
import { PartialsModule } from '../views/partials/partials.module';

// Services
import { HttpUtilsService, TypesUtilsService, InterceptService, LayoutUtilsService} from '../core/_base/crud';
// Shared
// Components
import {DialogModule} from 'primeng/dialog';

import { PdfViewerModule } from 'ng2-pdf-viewer';

// Material
import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatProgressBarModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTabsModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import {
  ActionNotificationComponent,
  DeleteEntityDialogComponent,
  UpdateStatusDialogComponent
} from '../views/partials/content/crud';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MaterialPreviewModule} from "../views/partials/content/general/material-preview/material-preview.module";
import {MatListModule} from "@angular/material/list";
import {MatBottomSheet, MatBottomSheetModule} from "@angular/material/bottom-sheet";
import {NgbootstrapModule} from "../views/pages/ngbootstrap/ngbootstrap.module";
import {ScheduleServiceService} from "../services/schedule-service.service";
import {AddGroupComponent} from "./add-group/add-group.component";
import {EditGroupComponent} from "./edit-group/edit-group.component";
import {GroupComponent} from "./group.component";
import { GroupEffects } from './store/group.effects';


const routes: Routes = [
  {
    path: '',
    component: GroupComponent,
    children: [
      {
        path: '',
        component: GroupComponent
      },     {
        path: 'group/add',
        component: AddGroupComponent
      },
      {
        path: 'group/add:id',
        component: AddGroupComponent
      },
      {
        path: 'group/edit',
        component: EditGroupComponent
      },
      {
        path: 'group/edit/:id',
        component: EditGroupComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    PdfViewerModule,
    PartialsModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatTabsModule,
    MatTooltipModule,
    MatDialogModule,
    MatGridListModule,
    MatToolbarModule,
    MaterialPreviewModule,
    MatListModule,
    MatBottomSheetModule,
    NgbootstrapModule,
    DialogModule,
    EffectsModule.forFeature([GroupEffects]),
  ],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'kt-mat-dialog-container__wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    HttpUtilsService,
    TypesUtilsService,
    LayoutUtilsService,
    ScheduleServiceService
  ],
  entryComponents: [
    ActionNotificationComponent,
    AddGroupComponent,
    EditGroupComponent,
    DeleteEntityDialogComponent,
    UpdateStatusDialogComponent,

  ],
  declarations: [
    AddGroupComponent,
    EditGroupComponent,
      GroupComponent

  ]
})
export class GroupModule { }
