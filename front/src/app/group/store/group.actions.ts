import { createAction, props } from '@ngrx/store';

export const loadGroups = createAction(
  '[Group] Load Groups',
    props<{ request?: any }>()
);

export const loadGroupsSuccess = createAction(
  '[Group] Load Groups Success',
  props<{ groups: any }>()
);

export const loadGroupsFailure = createAction(
  '[Group] Load Groups Failure',
  props<{ error: any }>()
);
export const addGroup = createAction(
    '[Group] add group',
    props<{ request: any }>()
);
export const editGroup = createAction(
    '[Group] edit group',
    props<{ request: any }>()
);

export const deleteGroup = createAction(
    '[Group] delete group',
    props<{ id: any }>()
);
