import { Action, createReducer, on } from '@ngrx/store';
import * as groupActions from "../store/group.actions";


export const groupFeatureKey = 'group';

export interface State {
  groups: any;
  loaded: boolean;

}

export const initialState: State = {
  groups: [],
  loaded: false,
};

const groupReducer = createReducer(
  initialState,
    on(groupActions.loadGroups, (state, {request}) => ({
      ...state,
      request: request,
      loaded: false,
      loading: true
    })),
    on(groupActions.loadGroupsSuccess, (state, {groups}) => ({
      ...state,
      groups: groups,
      loaded: false,
      loading: false
    })),
    on(groupActions.loadGroupsFailure, (state, {error}) => ({
      groups: [],
      loaded: false,
      loading: false
    })),

);

export function groupreducer(state: State | undefined, action: Action) {
  return groupReducer(state, action);
}
