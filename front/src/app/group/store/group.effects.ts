import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from "rxjs/operators";
import {loadGroupsFailure,loadGroupsSuccess,loadGroups} from "../store/group.actions";
import {of} from "rxjs/internal/observable/of";
import {GroupServiceService} from "../../services/group-service.service";



@Injectable()
export class GroupEffects {



  constructor(private actions$: Actions,private groupService:GroupServiceService) {}
  groupsList$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Group] Load Groups'),
          exhaustMap((action: any) => {
                if (action.request) {
                  return this.groupService.getAllGroup().pipe(
                      map(groups =>loadGroupsSuccess({groups: JSON.parse(JSON.stringify(groups))})),
                      catchError(error => of(loadGroupsFailure({error: error}))
                      )
                  )
                }
              }
          )));

  addGroup$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Group] add group'),
          exhaustMap((action: any) =>
              this.groupService.addGroup(action.request).pipe(
                  map(groups => {
                    return loadGroups({request: {}})
                  })
              )
          )));

  removeTeam$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Group] delete group'),
          exhaustMap(action =>
              this.groupService.deleteGroup(Object.assign(action).id).pipe(
                  map(groups => {
                    return loadGroups({request: {}})
                  })
              )
          )));
  editGroup$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Group] edit group'),
          exhaustMap(action =>
              this.groupService.updateGroup(Object.assign(action).id).pipe(
                  map(groups => {
                    return loadGroups({request: {}})
                  })
              )
          )));


}
