import { BaseModel } from '../core/_base/crud';

export class Subject extends BaseModel {
	idSubject: number;
	subjectName: string;


	clear(): void {
		this.idSubject = undefined;
		this.subjectName = '';

	}
}
