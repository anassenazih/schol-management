import {BaseModel} from "../core/_base/crud";
import {Question} from "./question.model";
import {User} from "./user.model";

export class Answer extends BaseModel {
    idAnswer: number;
    answer: string;
    question : Question;
    dateAnswer:Date;
    url : string;
    type:string;
    name:string;
    user:User;


    clear(): void {
        this.idAnswer = undefined;
        this.answer = '';

    }
}