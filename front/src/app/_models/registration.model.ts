
import { BaseModel } from '../core/_base/crud';
import {Cours} from "./cours.model";
import {User} from "./user.model";

export class Registration extends BaseModel {
    idRegistration: number;
    registrationDate: Date;
    status : String;
    user : User;
    course : Cours

    clear(): void {
        this.idRegistration = null;
        this.registrationDate = null;
        this.status='';
    }
}
