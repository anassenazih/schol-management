import { BaseModel } from '../core/_base/crud';

export class User extends BaseModel {
    idUser: number;
    username: string;
    password: string;
    email: string;
    role: string[];
	firstName: string;
	lastName: string;
    occupation: string;
	address: string;
	phoneNumber: string;



    clear(): void {
        this.idUser = null;
        this.username = '';
        this.password = '';
        this.email = '';
        this.firstName = '';
        this.lastName= '';
        this.occupation = '';
        this.address = '';
        this.phoneNumber = '';

    }
}
