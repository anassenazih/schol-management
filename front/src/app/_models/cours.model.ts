import { BaseModel } from '../core/_base/crud';
import {Forum} from "./ForumM.model";
import {Subject} from "./Subject.model";
import {User} from "./user.model";

export class Cours extends BaseModel {
	idCourse: number;
	courseName: string;
	forum : Forum;
	subject : Subject;
	teacher:User


	clear(): void {
		this.idCourse = undefined;
		this.courseName = '';

	}
}
