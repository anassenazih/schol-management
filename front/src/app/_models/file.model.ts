
import { BaseModel } from '../core/_base/crud';

export class File extends BaseModel {
    idFile: number;
    fileName: string;
    fileDownloadUri : string;
    fileType:string;
    size: number;


    clear(): void {
        this.idFile = undefined;
        this.fileName = '';
        this.fileDownloadUri='';
        this.fileType='';

    }
}
