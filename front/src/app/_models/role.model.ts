import { BaseModel } from '../core/_base/crud';

export class Role extends BaseModel {
	idRole: number;
	role: string;
    clear(): void {
        this.idRole = undefined;
        this.role = '';

	}
}
