import { BaseModel } from '../core/_base/crud';
export class Group extends BaseModel {
    idGroup: number;
    name: string;
    nbrOfStudents: number;


    clear(): void {
        this.idGroup = undefined;
        this.name = '';

    }
}
