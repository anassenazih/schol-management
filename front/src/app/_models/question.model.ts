import {BaseModel} from "../core/_base/crud";
import {Forum} from "./ForumM.model";
import {Cours} from "./cours.model";
import {User} from "./user.model";
import {Answer} from "./answer.model";

export class Question extends BaseModel {
    idQuestion: number;
    question: string;
    course : Cours;
    dateQuestion:Date;
    url : string;
    type:string;
    name:string;
    user:User;
    answers:Answer[];


    clear(): void {
        this.idQuestion = undefined;
        this.question = '';

    }
}