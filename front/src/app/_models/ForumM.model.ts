import { BaseModel } from '../core/_base/crud';
import {User} from "./user.model";

export class Forum extends BaseModel {
	idForum: number;
	forumName: string;
	administrator : User;


	clear(): void {
		this.idForum = undefined;
		this.forumName = '';

	}
}
