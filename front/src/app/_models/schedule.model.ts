import { BaseModel } from '../core/_base/crud';
import {Cours} from "./cours.model";
import {Group} from "./group.model";

export class Schedule extends BaseModel {
    idSchedule: number;
    groupExcellence: Group;
    url : string;
    type:string;
    name:string;
    clear(): void {
        this.idSchedule = undefined;

    }
}