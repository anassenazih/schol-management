import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {LayoutUtilsService, MessageType} from "../core/_base/crud";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AddEmploiComponent} from "./add-emploi/add-emploi.component";
import {ScheduleServiceService} from "../services/schedule-service.service";
import {TokenStorageService} from "../services/token-storage.service";
import {EditEmploiComponent} from "./edit-emploi/edit-emploi.component";
import {loadEmplois} from "./store/emploi.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../core/reducers";
import {loadCourss} from "../forums/store/cours.actions";
import {AddGroupComponent} from "../group/add-group/add-group.component";

@Component({
  selector: 'emploi-temps',
  templateUrl: './emploi-temps.component.html',
  styleUrls: ['./emploi-temps.component.scss']
})
export class EmploiTempsComponent implements OnInit {
  // Table fields
  listData:MatTableDataSource<any>;
  emplois=[];
  displayedColumns = [ 'groupe', 'emploi','actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  private searchKey: string;
  displayPdf = false;
  hasItems :boolean; // Need to show message: 'No records found'
  private file: any;
  private url: any;
  private role: any;
  constructor(		public dialog: MatDialog,
                      public snackBar: MatSnackBar,
                      private store: Store<AppState>,
                      private layoutUtilsService: LayoutUtilsService,
                      private scheduleService : ScheduleServiceService,
                      private tokenStorageService:TokenStorageService,
                      private cdr: ChangeDetectorRef) {
    this.scheduleService.componentMethodCalled$.subscribe(
        () => {
          this.ngOnInit();
        }
    );
  }

  ngOnInit() {
    this.store.dispatch(loadEmplois({request: this.emplois}));
    this.store.select(state => state.emploi).subscribe(data => {
      this.emplois = data.emploi;
      console.log(data.emploi);
      this.listData = new MatTableDataSource(this.emplois);
      this.listData.sort = this.sort;
      this.listData.paginator=this.paginator;
      this.cdr.markForCheck();
    });
    this.role = this.tokenStorageService.getUser().roles;
  }

  onSearchClear(){
    this.searchKey="";
    this.applyFilter();
  }
  applyFilter(){
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }



  editEmploi(emploi: any) {
    const _saveMessage = `Modifié avec succès.`;
    const _messageType =  MessageType.Update;
    const dialogRef = this.dialog.open(EditEmploiComponent, { data: { emploi:emploi} });
    dialogRef.afterClosed().subscribe(res => {
      this.scheduleService.callComponentMethod();
      if (!res) {
        return;
      }
      this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

    });
  }
  addEmploi() 	{
    const _saveMessage = `Ajouté avec succès.`;
    const _messageType =  MessageType.Create;
  const dialogRef = this.dialog.open(AddEmploiComponent, { data: {  } });
  dialogRef.afterClosed().subscribe(res => {
    this.scheduleService.callComponentMethod();
  if (!res) {
  return;
}
    this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

  });



}


  deleteEmploi(idSchedule: number){
    const _cours = 'Schedule' ;
    const _description = 'Vous êtes sûr de vouloir supprimer cet emploi du temps?';
    const _waitDesciption = 'Suppression de l`emploi  ...';
    const _deleteMessage = `L'emploi a été supprimé`;

    const dialogRef = this.layoutUtilsService.deleteElement(_cours , _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {

      if (res) {
        this.scheduleService.deleteSchedule(idSchedule).subscribe(data=>(this.ngOnInit()));
        this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
      }


    });
  }
  showDialogPdf(){
    this.displayPdf = true;
  }

}

