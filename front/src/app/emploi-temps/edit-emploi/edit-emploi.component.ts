import {Component, Inject, OnInit} from '@angular/core';
import {Cours} from "../../_models/cours.model";
import {User} from "../../_models/user.model";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";
import {ForumServiceService} from "../../services/forum-service.service";
import {TokenStorageService} from "../../services/token-storage.service";
import {UserServiceService} from "../../services/user-service.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Forum} from "../../_models/ForumM.model";
import {Schedule} from "../../_models/schedule.model";
import {ScheduleServiceService} from "../../services/schedule-service.service";
import {Observable} from "rxjs";
import {Group} from "../../_models/group.model";
import {CoursServiceService} from "../../services/cours-service.service";
import {FileServiceService} from "../../services/file-service.service";
import {GroupServiceService} from "../../services/group-service.service";
import {FileComponent} from "../../core/file/file.component";
import {loadGroups} from "../../group/store/group.actions";

@Component({
  selector: 'edit-emploi',
  templateUrl: './edit-emploi.component.html',
  styleUrls: ['./edit-emploi.component.scss']
})
export class EditEmploiComponent implements OnInit {

  selectedFiles: FileList;
  currentFile: File;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  private addedCours: Object;
  private id2: any;
  private user: User;
  private cours=[];
  private file: string;
  private type: string;
  private name: string;
  private groupss: Group;

  constructor(public dialogRef: MatDialogRef<EditEmploiComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private scheduleService : ScheduleServiceService,
              private fileService: FileServiceService,
              public dialog: MatDialog,
              private groupService : GroupServiceService) { }


  ngOnInit() {
    this.store.dispatch(loadGroups({request: this.cours}));
    this.store.select(state => state.groups).subscribe(data => {
      this.cours = JSON.parse(JSON.stringify(data.groups));
    });
  }

  

  /**
   * Save data
   */
  onSubmit() {  const _emploi = new Schedule();
    this.id2=this.form.get('group').value;
    if(this.id2==this.data.emploi.groupExcellence.idGroup){
      _emploi.groupExcellence = this.data.emploi.groupExcellence;
    }else{
      this.groupService.getGroupById(this.id2).subscribe(data=> {
        this.groupss = data;
        _emploi.groupExcellence = this.groupss;
      });}
        _emploi.url=this.file;
        _emploi.type=this.type;
        _emploi.name=this.name;
        this.scheduleService.updateSchedule(_emploi).subscribe(data => {
          this.addedCours = data;
          this.file=null;
          this.type=null;
          this.name=null;
          this.dialogRef.close({
            isEdit: false
          });
        }, error1 => {
          console.log(error1)
        });
    }
  openBottomSheet(): void {
    const dialogRef = this.dialog.open(FileComponent, {data: {}});
    dialogRef.afterClosed().subscribe(res => {
      console.log(res.data);  console.log(res.type);
      this.file=res.url;
      this.type=res.type;
      this.name=res.data;
      if (!res) {
        return;
      }
    });
  }


  selectFile(event) {

    this.selectedFiles = event.target.files;

  }

  form = new FormGroup({
    group: new FormControl(),
  });



  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }



}
