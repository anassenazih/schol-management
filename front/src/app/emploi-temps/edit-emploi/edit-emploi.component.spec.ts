import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEmploiComponent } from './edit-emploi.component';

describe('EditEmploiComponent', () => {
  let component: EditEmploiComponent;
  let fixture: ComponentFixture<EditEmploiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditEmploiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEmploiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
