// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// Translate
import { TranslateModule } from '@ngx-translate/core';
import { PartialsModule } from '../views/partials/partials.module';

// Services
import { HttpUtilsService, TypesUtilsService, InterceptService, LayoutUtilsService} from '../core/_base/crud';
// Shared
// Components
import {DialogModule} from 'primeng/dialog';

import { PdfViewerModule } from 'ng2-pdf-viewer';

// Material
import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatProgressBarModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTabsModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import {
  ActionNotificationComponent,
  DeleteEntityDialogComponent,
  UpdateStatusDialogComponent
} from '../views/partials/content/crud';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MaterialPreviewModule} from "../views/partials/content/general/material-preview/material-preview.module";
import {MatListModule} from "@angular/material/list";
import {MatBottomSheet, MatBottomSheetModule} from "@angular/material/bottom-sheet";
import {NgbootstrapModule} from "../views/pages/ngbootstrap/ngbootstrap.module";
import {EmploiTempsComponent} from "./emploi-temps.component";
import {AddEmploiComponent} from "./add-emploi/add-emploi.component";
import {EditEmploiComponent} from "./edit-emploi/edit-emploi.component";
import {ScheduleServiceService} from "../services/schedule-service.service";
import { EmploiEffects } from './store/emploi.effects';


const routes: Routes = [
  {
    path: '',
    component: EmploiTempsComponent,
    children: [
        {
        path: '',
        component: EmploiTempsComponent
      },       {
        path: 'emploi/add',
        component: AddEmploiComponent
      },
      {
        path: 'emploi/add:id',
        component: AddEmploiComponent
      },
      {
        path: 'emploi/edit',
        component: EditEmploiComponent
      },
      {
        path: 'emploi/edit/:id',
        component: EditEmploiComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    PdfViewerModule,
    PartialsModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatTabsModule,
    MatTooltipModule,
    MatDialogModule,
    MatGridListModule,
    MatToolbarModule,
    MaterialPreviewModule,
    MatListModule,
    MatBottomSheetModule,
    NgbootstrapModule,
    DialogModule,
    EffectsModule.forFeature([EmploiEffects]),
  ],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'kt-mat-dialog-container__wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    HttpUtilsService,
    TypesUtilsService,
    LayoutUtilsService,
      ScheduleServiceService
  ],
  entryComponents: [
    ActionNotificationComponent,
    AddEmploiComponent,
    EditEmploiComponent,
    DeleteEntityDialogComponent,
    UpdateStatusDialogComponent,

  ],
  declarations: [
      AddEmploiComponent,
      EditEmploiComponent,
      EmploiTempsComponent

  ]
})
export class EmploiModule { }
