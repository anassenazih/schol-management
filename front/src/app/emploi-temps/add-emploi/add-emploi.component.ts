import {ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CoursServiceService} from "../../services/cours-service.service";
import {Cours} from "../../_models/cours.model";
import {Observable, Subject} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ScheduleServiceService} from "../../services/schedule-service.service";
import {Question} from "../../_models/question.model";
import {MessageType} from "../../core/_base/crud";
import {Schedule} from "../../_models/schedule.model";
import {FileServiceService} from "../../services/file-service.service";
import {GroupServiceService} from "../../services/group-service.service";
import {Group} from "../../_models/group.model";
import {loadGroups} from "../../group/store/group.actions";
import {MatTableDataSource} from "@angular/material/table";
import {Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";

@Component({
  selector: 'add-emploi',
  templateUrl: './add-emploi.component.html',
  styleUrls: ['./add-emploi.component.scss']
})
export class AddEmploiComponent implements OnInit {
    selectedFiles: FileList;
    currentFile: File;
    private cours = [];
    private addedSchedule: Object;
    private result: Group;
    viewLoading = false;
    private er: any;
    private unsubscribe: Subject<any>;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
                public dialogRef: MatDialogRef<AddEmploiComponent>,
                private coursService: CoursServiceService,
                private scheduleService: ScheduleServiceService,
                private fileService: FileServiceService,
                private groupService: GroupServiceService,
                private cd: ChangeDetectorRef,
                private store: Store<AppState>,
                private fb: FormBuilder) {
    }

    ngOnInit() {
        this.store.dispatch(loadGroups({request: this.cours}));
        this.store.select(state => state.groups).subscribe(data => {
            this.cours = JSON.parse(JSON.stringify(data.groups));
        });


    }


    selectFile(event) {
        this.selectedFiles = event.target.files;
        this.currentFile = this.selectedFiles.item(0);
        this.form.patchValue({
            file: this.currentFile
        });
    }

    form = new FormGroup({
        file: new FormControl(null, Validators.required),
        group: new FormControl(null, Validators.required),
    });

    onSubmit() {
        if (this.form.valid) {
            const id = this.form.get('group').value;
            this.groupService.getGroupById(id).subscribe(data => {
                this.result = data;
                this.form.patchValue({
                    file: this.currentFile
                });
                console.log(this.form.get('file').value);
                this.scheduleService.uploadFile(this.form.get('file').value).subscribe(
                    res => {
                        this.er = res;
                        const _sched = new Schedule();
                        _sched.idSchedule = null;
                        _sched.groupExcellence = this.result;
                        _sched.url = this.er.fileDownloadUri;
                        _sched.name = this.er.fileName;
                        _sched.type = this.er.fileType;
                        this.scheduleService.addSchedule(_sched).subscribe();
                        this.viewLoading = false;
                        this.dialogRef.close({});
                    });


        });
            this.form.reset();
    }
}


}
