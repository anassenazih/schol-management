import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from "rxjs/operators";
import {loadEmploisFailure,loadEmploisSuccess,loadEmplois} from "../store/emploi.actions";
import {of} from "rxjs/internal/observable/of";
import {ScheduleServiceService} from "../../services/schedule-service.service";



@Injectable()
export class EmploiEffects {



  constructor(private actions$: Actions,private scheduleService:ScheduleServiceService) {}
  emploiList$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Emploi] Load Emplois'),
          exhaustMap((action: any) => {
                if (action.request) {
                  return this.scheduleService.getAllSchedules().pipe(
                      map(emploi =>loadEmploisSuccess({emploi: JSON.parse(JSON.stringify(emploi))})),
                      catchError(error => of(loadEmploisFailure({error: error}))
                      )
                  )
                }
              }
          )));

  addEmploi$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Emploi] add emploi'),
          exhaustMap((action: any) =>
              this.scheduleService.addSchedule(action.request).pipe(
                  map(emploi => {
                    return loadEmplois({request: {}})
                  })
              )
          )));

  removeTeam$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Emploi] delete emploi'),
          exhaustMap(action =>
              this.scheduleService.deleteSchedule(Object.assign(action).id).pipe(
                  map(emploi => {
                    return loadEmplois({request: {}})
                  })
              )
          )));
  editEmploi$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Emploi] edit Emploi'),
          exhaustMap(action =>
              this.scheduleService.updateSchedule(Object.assign(action).id).pipe(
                  map(emploi => {
                    return loadEmplois({request: {}})
                  })
              )
          )));


}
