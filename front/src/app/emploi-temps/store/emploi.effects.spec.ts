import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { EmploiEffects } from './emploi.effects';

describe('EmploiEffects', () => {
  let actions$: Observable<any>;
  let effects: EmploiEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        EmploiEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<EmploiEffects>(EmploiEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
