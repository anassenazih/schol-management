import { createAction, props } from '@ngrx/store';

export const loadEmplois = createAction(
  '[Emploi] Load Emplois',
    props<{ request?: any }>()
);

export const loadEmploisSuccess = createAction(
  '[Emploi] Load Emplois Success',
  props<{ emploi: any }>()
);

export const loadEmploisFailure = createAction(
  '[Emploi] Load Emplois Failure',
  props<{ error: any }>()
);
export const addEmploi = createAction(
    '[Emploi] add emploi',
    props<{ request: any }>()
);
export const editEmploi = createAction(
    '[Emploi] edit emploi',
    props<{ request: any }>()
);

export const deleteEmploi = createAction(
    '[Emploi] delete emploi',
    props<{ id: any }>()
);
