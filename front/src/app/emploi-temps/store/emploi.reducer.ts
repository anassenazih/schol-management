import { Action, createReducer, on } from '@ngrx/store';
import * as emploiActions from "../store/emploi.actions";


export const emploiFeatureKey = 'emploi';

export interface State {
  emploi:any;
  loaded:boolean;

}

export const initialState: State = {
  emploi:[],
  loaded:false

};

const emploiReducer = createReducer(
  initialState,
    on(emploiActions.loadEmplois, (state,{request}) => ({
      ...state,
      request:request,
      loaded: false,
      loading: true
    })),
    on(emploiActions.loadEmploisSuccess, (state, {emploi}) => ({
      ...state,
      emploi: emploi,
      loaded: false,
      loading: false
    })),
    on(emploiActions.loadEmploisFailure, (state, {error}) => ({
      emploi: [],
      loaded: false,
      loading: false
    })),

);

export function emploireducer(state: State | undefined, action: Action) {
  return emploiReducer(state, action);
}
