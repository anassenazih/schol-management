import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisioConfComponent } from './visio-conf/visio-conf.component';
import {Routes} from "@angular/router";



const routes: Routes = [
    {
        path: '',
        component: VisioConfComponent
    }
];
@NgModule({
  declarations: [VisioConfComponent],
  imports: [
    CommonModule
  ]
})
export class VisioConferenceModule { }
