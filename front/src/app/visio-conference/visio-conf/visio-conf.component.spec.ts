import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisioConfComponent } from './visio-conf.component';

describe('VisioConfComponent', () => {
  let component: VisioConfComponent;
  let fixture: ComponentFixture<VisioConfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisioConfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisioConfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
