import { Component, OnInit } from '@angular/core';
import { ZoomMtg } from 'zoomus-jssdk';

ZoomMtg.setZoomJSLib('https://dmogdx0jrul3u.cloudfront.net/1.4.2/lib', '/av');
ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();
@Component({
  selector: 'visio-conf',
  templateUrl: './visio-conf.component.html',
  styleUrls: ['./visio-conf.component.scss']
})
export class VisioConfComponent implements OnInit {


    meetConfig = {
        apiKey: '5b62WdmqTie0MUivJcPjiQ',
        apiSecret: '83fMiJTNIYyoAKYvs9Vjpm2MGcUR8U3Elww4',
        meetingNumber: 889274905,
        userName: 'Anasse',
        passWord: "",
        leaveUrl: "https://zoom.us",
        role: 0
    };

    signature = ZoomMtg.generateSignature({
        meetingNumber: this.meetConfig.meetingNumber,
        apiKey: this.meetConfig.apiKey,
        apiSecret: this.meetConfig.apiSecret,
        role: this.meetConfig.role,
        success: function(res){
            console.log(res.result);
        }
    });

    constructor() {

    }

    ngOnInit() {
        ZoomMtg.init({
            leaveUrl: 'https://zoom.us',
            isSupportAV: true,
            success: (res) => {
                ZoomMtg.join({
                    meetingNumber: this.meetConfig.meetingNumber,
                    userName: this.meetConfig.userName,
                    signature: this.signature,
                    apiKey: this.meetConfig.apiKey,
                    userEmail: '',
                    passWord: this.meetConfig.passWord,
                    success: (res) => {
                        console.log('join meeting success');
                    },
                    error: (res) => {
                        console.log(res);
                    }
                });
            },
            error: (res) => {
                console.log(res);
            }
        });
    }

}
