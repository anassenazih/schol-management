// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// RxJS
import { Observable, Subject } from 'rxjs';
import {delay, finalize, takeUntil, tap} from 'rxjs/operators';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Store
import { Store } from '@ngrx/store';
// Auth
import {TokenStorageService} from "../../services/token-storage.service";
import {AuthService} from "../../services/auth.service";
import {AuthNoticeService} from "../auth-notice/auth-notice.service";

/**
 * ! Just example => Should be removed in development
 */

@Component({
	selector: 'kt-login',
	templateUrl: './login.component.html',
	encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy {
	form: any = {};
	isLoggedIn = false;
	isLoginFailed = false;
	errorMessage = ' ';

	// Public params
	loginForm: FormGroup;
	loading = false;
	isLoggedIn$: Observable<boolean>;
	errors: any = [];

	private unsubscribe: Subject<any>;

	private returnUrl: any;
	private roles = null;
	showAdminBoard = false;
	showTeacherBoard = false;
	showParentBoard =false;
	showStudentBoard =false;
	username: string;

	// Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param router: Router
	 * @param auth: AuthService
	 * @param authNoticeService: AuthNoticeService
	 * @param translate: TranslateService
	 * @param store: Store<AppState>
	 * @param fb: FormBuilder
	 * @param cdr
	 * @param route
	 */
	constructor(
		private router: Router,
		private auth: AuthService,
		private authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute,
		private tokenStorage: TokenStorageService
	) {
		this.unsubscribe = new Subject();
	}



	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {

		this.initLoginForm();

	/*	// redirect back to the returnUrl before login
		this.route.queryParams.subscribe(params => {
			this.returnUrl = params.returnUrl || '/';
		});*/

		if (this.tokenStorage.getToken()) {
			this.isLoggedIn = true;
			this.roles = this.tokenStorage.getUser().roles;
		}
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	/**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		// demo message to show


		this.loginForm = this.fb.group({
			username: ['', Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(320) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
			])
			],
			password: ['',Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(100)
			])
			]
		});
	}

	/**
	 * Form Submit
	 */
	submit() {

		const controls = this.loginForm.controls;
		/** check form */
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;

		const authData = {
			username: controls.username.value,
			password: controls.password.value
		};
		console.log('auth',authData);
		this.auth
			.login(authData).subscribe( er => {
				localStorage.clear();
				sessionStorage.clear();
				console.log('data =>', er);
                localStorage.setItem('tokenJWT', er.token);
				localStorage.setItem('role', er.roles);
                this.tokenStorage.saveToken(er.token);
					this.tokenStorage.saveUser(er);
					this.isLoginFailed = false;
					this.isLoggedIn = true;
					console.log('is logged in', this.isLoggedIn);
					if(	this.isLoggedIn = true){
						this.roles = localStorage.getItem('role');
						console.log('roles ===> ', this.roles);
						if (this.roles.includes('ROLE_ADMIN')){
							this.router.navigateByUrl('/admin/dashboard');}
						else if(this.roles.includes('ROLE_TEACHER')){
							this.router.navigateByUrl('/teacher/dashboard');
						}else if(this.roles.includes('ROLE_PARENT')){
							this.router.navigateByUrl('/parent/dashboard');
						}else if(this.roles.includes('ROLE_STUDENT')){
							console.log('student here');
							this.router.navigateByUrl('/student/dashboard');
						}
					}
					console.log('done ==>')
				this.cdr.markForCheck();


				},
				err => {
					this.errorMessage = err.error.message;
					this.isLoginFailed = true;
					this.loading=false;
                    console.log('Error ==> 1')
                    this.cdr.markForCheck();
					}
			);


	}
     reloadPage() {
		 window.location.reload();

}


	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
}
