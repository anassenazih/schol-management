export class Menu4Config {
    public defaults: any = {
        header: {
            self: {},
            items: [
                {
                    title: 'Accueil',
                    root: true,
                    icon: 'flaticon2-architecture-and-city',
                    page: '/student/dashboard',
                    translate: 'MENU.DASHBOARD',
                    bullet: 'dot',
                },

                {section: 'Forum'},
                {
                    title: 'Forums',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-squares-3',
                    submenu: [

                        {
                            title: 'forum',
                            page: '/student/forums/forum'
                        },
                    ]
                },
                {section: 'Emplois du temps'},
                {
                    title: 'Emplois du temps',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-event-calendar-symbol',
                    submenu: [

                        {
                            title: 'emploi',
                            page: '/student/emploi'
                        },

                    ]
                },
                {section: 'Notes'},
                {
                    title: 'Notes',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-edit-1',
                    submenu: [

                        {
                            title: 'notes',
                            page: '/student/notes'
                        },

                    ]
                },




            ]
        },
        aside: {
            self: {},
            items: [
                {
                    title: 'Accueil',
                    root: true,
                    icon: 'flaticon2-architecture-and-city',
                    page: '/student/dashboard',
                    translate: 'MENU.DASHBOARD',
                    bullet: 'dot',
                },


                {section: 'Forum'},
                {
                    title: 'Forum',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-squares-3',
                    page: '/student/forums/forum'
                },
                {
                    title: 'Emplois du temps',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-event-calendar-symbol',
                    page: '/student/emploi'
                },
                {
                    title: 'Notes',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-edit-1',
                    page: '/student/notes'
                },




            ]
        },

    };

    public get configs(): any {
        return this.defaults;
    }
}
