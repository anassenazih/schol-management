import { Action, createReducer, on } from '@ngrx/store';
import * as questionActions from "./question.actions";


export const questionFeatureKey = 'question';

export interface State {
  questions: any;
  loaded: boolean;
}

export const initialState: State = {
  questions: [],
  loaded: false,
};

const questionReducer = createReducer(
  initialState,
    on(questionActions.loadQuestions, (state, {request}) => ({
      ...state,
      request: request,
      loaded: false,
      loading: true
    })),
    on(questionActions.loadQuestionsSuccess, (state, {questions}) => ({
      ...state,
      questions: questions,
      loaded: false,
      loading: false
    })),
    on(questionActions.loadQuestionsFailure, (state, {error}) => ({
      questions: [],
      loaded: false,
      loading: false
    })),

);


export function questionreducer(state: State | undefined, action: Action) {
  return questionReducer(state, action);
}
