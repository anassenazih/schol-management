import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { AnswerEffects } from './answer.effects';

describe('AnswerEffects', () => {
  let actions$: Observable<any>;
  let effects: AnswerEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AnswerEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<AnswerEffects>(AnswerEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
