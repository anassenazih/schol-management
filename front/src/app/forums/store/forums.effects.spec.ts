import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ForumsEffects } from './forums.effects';

describe('ForumsEffects', () => {
  let actions$: Observable<any>;
  let effects: ForumsEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ForumsEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<ForumsEffects>(ForumsEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
