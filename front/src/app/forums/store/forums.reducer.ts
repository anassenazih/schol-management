import { Action, createReducer, on } from '@ngrx/store';
import * as forumsActions from './forums.actions';

export const forumsFeatureKey = 'forums';

export interface State {
  forums: any;
  loaded: boolean;

}

export const initialState: State = {
  forums: [],
  loaded: false,

};

const forumsReducer = createReducer(
  initialState,
    on(forumsActions.loadForumss, (state, {request}) => ({
      ...state,
      request: request,
      loaded: false,
      loading: true
    })),
    on(forumsActions.loadForumssSuccess, (state, {forums}) => ({
      ...state,
      forums: forums,
      loaded: false,
      loading: false
    })),
    on(forumsActions.loadForumssFailure, (state, {error}) => ({
      forums: [],
      loaded: false,
      loading: false
    })),

);

export function reducer(state: State | undefined, action: Action) {
  return forumsReducer(state, action);
}
