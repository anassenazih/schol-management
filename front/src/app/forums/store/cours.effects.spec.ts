import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { CoursEffects } from './cours.effects';

describe('CoursEffects', () => {
  let actions$: Observable<any>;
  let effects: CoursEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CoursEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<CoursEffects>(CoursEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
