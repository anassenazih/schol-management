import { createAction, props } from '@ngrx/store';

export const loadAnswers = createAction(
  '[Answer] Load Answers',
    props<{ request?: any }>()
);

export const loadAnswersSuccess = createAction(
  '[Answer] Load Answers Success',
  props<{ answers: any }>()
);

export const loadAnswersFailure = createAction(
  '[Answer] Load Answers Failure',
  props<{ error: any }>()
);

export const addAnswer = createAction(
    '[Answer] add answer',
    props<{ request: any }>()
);
export const editAnswer = createAction(
    '[Answer] edit answer',
    props<{ request: any }>()
);

export const deleteAnswer = createAction(
    '[Answer] delete answer',
    props<{ id: any }>()
);
