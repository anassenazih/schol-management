import { createAction, props } from '@ngrx/store';

export const loadForumss = createAction(
  '[Forums] Load Forums',
   props<{ request?: any }>()
);

export const loadForumssSuccess = createAction(
  '[Forums] Load Forums Success',
  props<{ forums: any }>()
);

export const loadForumssFailure = createAction(
  '[Forums] Load Forums Failure',
  props<{ error: any }>()
);
export const addForum = createAction(
    '[Forums] add forum',
    props<{ request: any }>()
);
export const editForum = createAction(
    '[Forums] edit forum',
    props<{ request: any }>()
);

export const deleteForum = createAction(
    '[Forums] delete forum',
    props<{ id: any }>()
);