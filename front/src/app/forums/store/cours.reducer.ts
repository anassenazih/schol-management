import { Action, createReducer, on } from '@ngrx/store';
import * as coursActions from "./cours.actions";
import * as forumsActions from "./forums.actions";


export const coursFeatureKey = 'cours';

export interface State {
  cours: any;
    coursbyId:any;
  request:any;
  id:any;
  loaded: boolean;
}

export const initialState: State = {
  cours: [],
    coursbyId:[],
    request:null,
    id:null,
  loaded: false,
};

const coursReducer = createReducer(
  initialState,
    on(coursActions.loadCourss, (state,{request,id}) => ({
      ...state,
        id:id,
     request:request,
      loaded: false,
      loading: true
    })),
    on(coursActions.loadCourssSuccess, (state, {cours}) => ({
      ...state,
      cours: cours,
      loaded: false,
      loading: false
    })),
    on(coursActions.loadCourssFailure, (state, {error}) => ({
        cours: [],
        coursbyId:[],
        request:null,
        id:null,
        loaded: false,
        loading: false
    })),
    on(coursActions.loadCourssById, (state,{request}) => ({
        ...state,
        request:request,
        loaded: false,
        loading: true
    })),
    on(coursActions.loadCourssByIdSuccess, (state, {coursbyId}) => ({
        ...state,
        coursbyId: coursbyId,
        loaded: false,
        loading: false
    })),
    on(coursActions.loadCourssById, (state,{request}) => ({
        ...state,
        request:request,
        loaded: false,
        loading: true
    })),

);

export function coursreducer(state: State | undefined, action: Action) {
  return coursReducer(state, action);
}
