import {Action, createReducer, on} from '@ngrx/store';
import * as answerActions from "./answer.actions";


export const answerFeatureKey = 'answer';


export interface State {
  answers: any;
  loaded: boolean;
}

export const initialState: State = {
  answers: [],
  loaded: false,
};

const answerReducer = createReducer(
    initialState,
    on(answerActions.loadAnswers, (state, {request}) => ({
      ...state,
      request: request,
      loaded: false,
      loading: true
    })),
    on(answerActions.loadAnswersSuccess, (state, {answers}) => ({
      ...state,
      answers: answers,
      loaded: false,
      loading: false
    })),
    on(answerActions.loadAnswersFailure, (state, {error}) => ({
      answers: [],
      loaded: false,
      loading: false
    })),

);


export function answerreducer(state: State | undefined, action: Action) {
  return answerReducer(state, action);
}
