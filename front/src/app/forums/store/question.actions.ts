import { createAction, props } from '@ngrx/store';

export const loadQuestions = createAction(
  '[Question] Load Questions',
    props<{ request?: any }>()
);

export const loadQuestionsSuccess = createAction(
  '[Question] Load Questions Success',
  props<{ questions: any }>()
);

export const loadQuestionsFailure = createAction(
  '[Question] Load Questions Failure',
  props<{ error: any }>()
);

export const addQuestion = createAction(
    '[Question] add question',
    props<{ request: any }>()
);
export const editQuestion = createAction(
    '[Question] edit question',
    props<{ request: any }>()
);

export const deleteQuestion = createAction(
    '[Question] delete question',
    props<{ id: any }>()
);
