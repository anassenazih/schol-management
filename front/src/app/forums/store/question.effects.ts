import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {CoursServiceService} from "../../services/cours-service.service";
import {QuestionServiceService} from "../../services/question-service.service";
import {catchError, exhaustMap, map} from "rxjs/operators";
import {loadQuestionsFailure, loadQuestionsSuccess, loadQuestions} from "./question.actions";
import {of} from "rxjs/internal/observable/of";



@Injectable()
export class QuestionEffects {



  constructor(private actions$: Actions,private coursService : CoursServiceService,private questionService:QuestionServiceService) {}
  questionsList$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Question] Load Questions'),
          exhaustMap((action: any) => {
                if (action.request) {
                  return this.coursService.getQuestions(action.request).pipe(
                      map(questions => loadQuestionsSuccess({questions: JSON.parse(JSON.stringify(questions))})),
                      catchError(error => of(loadQuestionsFailure({error: error}))
                      )
                  )
                }
              }
          )));

  addQuestion$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Question] add question'),
          exhaustMap((action: any) =>
              this.questionService.addQuestion(action.request).pipe(
                  map(questions => {
                    return loadQuestions({request: {}})
                  })
              )
          )));

  removeTeam$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Question] delete question'),
          exhaustMap(action =>
              this.questionService.deleteQuestion(Object.assign(action).id).pipe(
                  map(questions => {
                    return loadQuestions({request: {}})
                  })
              )
          )));

}
