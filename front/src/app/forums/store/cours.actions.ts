import { createAction, props } from '@ngrx/store';

export const loadCourss = createAction(
  '[Cours] Load Courss',
    props<{ request?: number ,id?:number}>()
);

export const loadCourssSuccess = createAction(
  '[Cours] Load Courss Success',
  props<{ cours: any }>()
);

export const loadCourssFailure = createAction(
  '[Cours] Load Courss Failure',
  props<{ error: any }>()
);
export const loadCourssByIdFailure = createAction(
    '[Cours] Load Courss id Failure',
    props<{ error: any }>()
);

export const addCours = createAction(
    '[Cours] add cours',
    props<{ request: any }>()
);
export const editCours = createAction(
    '[Cours] edit cours',
    props<{ request: any }>()
);

export const deleteCours = createAction(
    '[Cours] delete cours',
    props<{ id: any }>()
);
export const loadCourssById = createAction(
    '[Cours] Load Courss By id',
    props<{ request?: number}>(),
);
export const loadCourssByIdSuccess = createAction(
    '[Cours] Load Courss By id Success',
    props<{coursbyId: any }>(),
);


