import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from "rxjs/operators";
import {
    loadCourss,
    loadCourssById,
    loadCourssByIdFailure,
    loadCourssByIdSuccess,
    loadCourssFailure,
    loadCourssSuccess
} from "./cours.actions";
import {of} from "rxjs/internal/observable/of";
import {CoursServiceService} from "../../services/cours-service.service";
import {Forum} from "../../_models/ForumM.model";
import {ForumServiceService} from "../../services/forum-service.service";
import {loadNotes} from "../../notes/store/notes.actions";
import {loadForumssFailure, loadForumssSuccess} from "./forums.actions";



@Injectable()
export class CoursEffects {



  constructor(private actions$: Actions,private coursService :CoursServiceService,private forumService:ForumServiceService) {}

  coursList$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Cours] Load Courss'),
          exhaustMap((action: any) => {
                  if (action.request && action.id) {
                      console.log('action request',action.request); console.log('action id',action.id);
                      return  this.coursService.getCourseByIdForumAndIdUser(action.request,action.id).pipe(
                          map(cours => loadCourssSuccess({cours: JSON.parse(JSON.stringify(cours))})),
                          catchError(error => of(loadCourssByIdFailure({error: error}))
                          )
                      )
                  }
              }
          )));
  coursListbyId$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Cours] Load Courss By id'),
          exhaustMap((action: any) => {
                  if (action.request) {
                      console.log('action request',action.request);
                      return  this.coursService.getCourseById(action.request).pipe(
                          map(coursbyId => loadCourssByIdSuccess({coursbyId: JSON.parse(JSON.stringify(coursbyId))})),
                          catchError(error => of(loadCourssFailure({error: error}))
                          )
                      )
                  }
              }
          )));

  addCours$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Cours] add cours'),
          exhaustMap((action: any) =>
              this.coursService.addCours(action.request).pipe(
                  map(cours => {
                    return loadCourss({request:null})
                  })
              )
          )));

  removeTeam$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Cours] delete cours'),
          exhaustMap(action =>
              this.coursService.deleteCours(Object.assign(action).id).pipe(
                  map(cours => {
                    return loadCourss({request: null})
                  })
              )
          )));

}
