import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from "rxjs/operators";
import {loadForumss, loadForumssFailure, loadForumssSuccess} from "./forums.actions";
import {of} from "rxjs/internal/observable/of";
import {ForumServiceService} from "../../services/forum-service.service";
import {loadNotes} from "../../notes/store/notes.actions";



@Injectable()
export class ForumsEffects {



  constructor(private actions$: Actions,private forumsService :ForumServiceService) {}

  forumsList$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Forums] Load Forums'),
          exhaustMap((action: any) => {
                if (action.request) {
                  return this.forumsService.getAllForums().pipe(
                      map(forums => loadForumssSuccess({forums: JSON.parse(JSON.stringify(forums))})),
                      catchError(error => of(loadForumssFailure({error: error}))
                      )
                  )
                }
              }
          )));

  addForum$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Forums] add forum'),
          exhaustMap((action: any) =>
              this.forumsService.addForum(action.request).pipe(
                  map(forums => {
                    return loadForumss({request: {}})
                  })
              )
          )));

  removeTeam$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Forums] delete forum'),
          exhaustMap(action =>
              this.forumsService.deleteForum(Object.assign(action).id).pipe(
                  map(forums => {
                    return loadForumss({request: {}})
                  })
              )
          )));



}
