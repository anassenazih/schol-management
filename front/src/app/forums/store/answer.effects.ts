import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from "rxjs/operators";
import {loadAnswersFailure,loadAnswersSuccess,loadAnswers} from "./answer.actions";
import {of} from "rxjs/internal/observable/of";
import {AnswerServiceService} from "../../services/answer-service.service";
import {QuestionServiceService} from "../../services/question-service.service";



@Injectable()
export class AnswerEffects {



  constructor(private actions$: Actions,private answerService :AnswerServiceService,private questionService :QuestionServiceService) {}
    answersList$ = createEffect(() =>
        this.actions$.pipe(
            ofType('[Answer] Load Answers'),
            exhaustMap((action: any) => {
                  if (action.request) {
                    return this.questionService.getAnswers(action.request).pipe(
                        map(answers => loadAnswersSuccess({answers: JSON.parse(JSON.stringify(answers))})),
                        catchError(error => of(loadAnswersFailure({error: error}))
                        )
                    )
                  }
                }
            )));

    addAnswer$ = createEffect(() =>
        this.actions$.pipe(
            ofType('[Answer] add answer'),
            exhaustMap((action: any) =>
                this.answerService.addAnswer(action.request).pipe(
                    map(answers => {
                      return loadAnswers({request: {}})
                    })
                )
            )));

    removeTeam$ = createEffect(() =>
        this.actions$.pipe(
            ofType('[Question] delete question'),
            exhaustMap(action =>
                this.answerService.deleteAnswer(Object.assign(action).id).pipe(
                    map(answers => {
                      return loadAnswers({request: {}})
                    })
                )
            )));

  }

