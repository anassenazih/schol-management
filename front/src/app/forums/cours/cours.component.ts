import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {LayoutUtilsService, MessageType} from "../../core/_base/crud";
import {AddCoursComponent} from "./add-cours/add-cours.component";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CoursServiceService} from "../../services/cours-service.service";
import {Cours} from "../../_models/cours.model";
import {InviteUsersComponent} from "../invite-users/invite-users.component";
import {ForumServiceService} from "../../services/forum-service.service";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {EditCoursComponent} from "./edit-cours/edit-cours.component";
import {AgoraClient, ClientEvent, NgxAgoraService, Stream, StreamEvent} from "ngx-agora";
import {TokenStorageService} from "../../services/token-storage.service";
import {UserServiceService} from "../../services/user-service.service";
import {User} from "../../_models/user.model";
import {loadForumss} from "../store/forums.actions";
import {loadCourss} from "../store/cours.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";
import {loadNotes} from "../../notes/store/notes.actions";

const cardWithTitle = {
	beforeCodeTitle: ' ',

};

@Component({
  selector: 'cours',
  templateUrl: './cours.component.html',
  styleUrls: ['./cours.component.scss']
})
export class CoursComponent implements OnInit {

	public exampleCardWithTitle: any;
	public cours:any[];
	public cours1:Observable<Cours[]>;
	public id: number;
	public id2: any;
    video = true;
    audio = true;
    screen = false;
    display = false;
    displayPdf = false;
    displayVideo = false;
    pdfSrc = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";

    //Agora

    title = 'angular-video';
    localCallId = 'agora_local';
    remoteCalls: string[] = [];

    private client2: AgoraClient;
    private localStream: Stream;
    private uid: number;
    private role: any;
    private allCours= [];

	constructor( private coursService : CoursServiceService,
				 private route: ActivatedRoute,
				 private forumService : ForumServiceService,
				public dialog: MatDialog,
				public snackBar: MatSnackBar,
                 private store: Store<AppState>,
                 private ngxAgoraService: NgxAgoraService,
				private layoutUtilsService: LayoutUtilsService,
                 private tokenStorageService : TokenStorageService,
                 private userService : UserServiceService,
                 private cdr: ChangeDetectorRef) {
        this.forumService.componentMethodCalled$.subscribe(
            () => {
                this.ngOnInit();
            }
        );


	}

	ngOnInit() {
         this.id2 = this.tokenStorageService.getUser().id;
		this.id = this.route.snapshot.params['idForum'];
        console.log(this.id);
		//this.cours=this.forumService.getCourses(this.id );
        this.store.dispatch(loadCourss({request:this.id,id:this.id}));
        this.store.select(state => state.cours).subscribe(data => {
            this.allCours = JSON.parse(JSON.stringify(data.cours));
            this.cours =[];
            this.allCours.forEach(e => {
                this.cours.push(
                    {idCourse: e.idCourse,
                        courseName:  e.courseName,
                        subjectName: e.subject.subjectName,
                        idSubject:e.subject.idSubject,
                        forum:e.forum,
                        teacher:e.teacher,
                        subject:e.subject,
                    }
                )
            })
            this.cdr.markForCheck();
        });
        const user = this.tokenStorageService.getUser();
		this.exampleCardWithTitle = cardWithTitle;
		//this.startVideo();
        this.role = this.tokenStorageService.getUser().roles;
	}

	addCours() {
		const newCours = new Cours();
		newCours.clear(); // Set all defaults fields
		const _saveMessage = `Ajouté avec succès.`;
		const _messageType =  MessageType.Create;
		const dialogRef = this.dialog.open(AddCoursComponent, {data: {idForum:this.id }});
		dialogRef.afterClosed().subscribe(res => {
		    this.ngOnInit();
			if (!res) {
				return;
			}
			this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

		});
	}
	inviteUser(idCourse) {

		const dialogRef = this.dialog.open(InviteUsersComponent, {data: {idCourse :idCourse}});
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
		});
	}

	edit(c: Cours){
		const newCours = new Cours();
		newCours.clear(); // Set all defaults fields
		const _saveMessage = `Modifié avec succès.`;
		const _messageType =  MessageType.Update;
		const dialogRef = this.dialog.open(EditCoursComponent, {data: {idForum:this.id ,course :c }});
		dialogRef.afterClosed().subscribe(res => {
            this.ngOnInit();
			if (!res) {
				return;
			}
			this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

		});
	}

	delete(idCourse: number){
		const _cours = 'Cours' ;
		const _description = 'Vous êtes sûr de vouloir supprimer ce cours?';
		const _waitDesciption = 'Suppression de cours ...';
		const _deleteMessage = `Cours a été supprimé`;

		const dialogRef = this.layoutUtilsService.deleteElement(_cours , _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {

			if (res) {
				this.coursService.deleteCours(idCourse).subscribe(data=>{
                    this.ngOnInit()});
				this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
			}


		});
	}

    showDialog() {
        this.display = true;
    }
    showDialogPdf(){
        this.displayPdf = true;
    }

    private assignLocalStreamHandlers(): void {
        this.localStream.on(StreamEvent.MediaAccessAllowed, () => {
            console.log('accessAllowed');
        });

        // The user has denied access to the camera and mic.
        this.localStream.on(StreamEvent.MediaAccessDenied, () => {
            console.log('accessDenied');
        });
    }

    private initLocalStream(onSuccess?: () => any): void {
        this.localStream.init(
            () => {
                // The user has granted access to the camera and mic.
                this.localStream.play(this.localCallId);
                if (onSuccess) {
                    onSuccess();
                }
            },
            err => console.error('getUserMedia failed', err)
        );
    }

    startVideo(){
    let client: AgoraClient;
        this.uid = Math.floor(Math.random() * 100);
        client = this.ngxAgoraService.createClient({ mode: 'live', codec: 'h264' });
       // this.client2 = this.ngxAgoraService.createClient({ mode: 'rtc', codec: 'h264' });
        this.assignClientHandlers(client);
        //this.assignClientHandlers(this.client2);

        // Added in this step to initialize the local A/V stream
        this.localStream = this.ngxAgoraService.createStream({ streamID: this.uid, audio: this.audio, video: this.video, screen: this.screen });
        this.assignLocalStreamHandlers();
        this.initLocalStream();
        this.displayVideo = true;
        }

    private assignClientHandlers(client): void {
        client.on(ClientEvent.LocalStreamPublished, evt => {
            console.log('Publish local stream successfully');
        });

        client.on(ClientEvent.Error, error => {
            console.log('Got error msg:', error.reason);
            if (error.reason === 'DYNAMIC_KEY_TIMEOUT') {
                client.renewChannelKey(
                    '',
                    () => console.log('Renewed the channel key successfully.'),
                    renewError => console.error('Renew channel key failed: ', renewError)
                );
            }
        });

        client.on(ClientEvent.RemoteStreamAdded, evt => {
            const stream = evt.stream as Stream;
            client.subscribe(stream, { audio: true, video: true }, err => {
                console.log('Subscribe stream failed', err);
            });
        });

        client.on(ClientEvent.RemoteStreamSubscribed, evt => {
            const stream = evt.stream as Stream;
            const id = this.getRemoteId(stream);
            if (!this.remoteCalls.length) {
                this.remoteCalls.push(id);
                setTimeout(() => stream.play(id), 1000);
            }
        });

        client.on(ClientEvent.RemoteStreamRemoved, evt => {
            const stream = evt.stream as Stream;
            if (stream) {
                stream.stop();
                this.remoteCalls = [];
                console.log(`Remote stream is removed ${stream.getId()}`);
            }
        });

        client.on(ClientEvent.PeerLeave, evt => {
            const stream = evt.stream as Stream;
            if (stream) {
                stream.stop();
                this.remoteCalls = this.remoteCalls.filter(call => call !== `${this.getRemoteId(stream)}`);
                console.log(`${evt.uid} left from this channel`);
            }
        });
    }

    private getRemoteId(stream: Stream): string {
        return `agora_remote-${stream.getId()}`;
    }

    shareScreen(){
	    this.video = false;
	    this.audio= false;
	    this.screen = true;
	    this.startVideo();
    }

}
