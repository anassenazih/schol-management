import {Component, Inject, OnInit} from '@angular/core';
import {AppState} from "../../../core/reducers";
import {Store} from "@ngrx/store";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Observable} from "rxjs";
import {Cours} from "../../../_models/cours.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CoursServiceService} from "../../../services/cours-service.service";
import {ForumServiceService} from "../../../services/forum-service.service";
import {SubjectServiceService} from "../../../services/subject-service.service";
import {Subject} from "../../../_models/Subject.model";
import {Forum} from "../../../_models/ForumM.model";
import {TokenStorageService} from "../../../services/token-storage.service";
import {UserServiceService} from "../../../services/user-service.service";
import {User} from "../../../_models/user.model";

@Component({
  selector: 'add-cours',
  templateUrl: './add-cours.component.html',
  styleUrls: ['./add-cours.component.scss']
})
export class AddCoursComponent implements OnInit {
	cours: Cours;
	hasFormErrors = false;
	viewLoading = false;
	loadingAfterSubmit = false;
	private addedCours: Object;
	private result: Forum;
	private subjects: Observable<Subject[]>;
	private res: Subject;
	private id: any;
	private user: User;

	constructor(public dialogRef: MatDialogRef<AddCoursComponent>,
				@Inject(MAT_DIALOG_DATA) public data: any,
				private store: Store<AppState>,
				private coursService : CoursServiceService,
				private forumService : ForumServiceService,
				private subjectService : SubjectServiceService,
				private tokenStorageService:TokenStorageService,
				private userService : UserServiceService) { }


	ngOnInit() {
		this.subjects = this.subjectService.getAllSubjects();
		this.forumService.getForumById(this.data.idForum).subscribe(result => {
			this.result = result;
		});
		const user = this.tokenStorageService.getUser();
		this.id = user.id;
		this.userService.getUserById(this.id).subscribe(data=>{
			this.user=data;
		})
	}


	form : FormGroup = new FormGroup({
		idCourse: new FormControl(null),
		courseName: new FormControl('',Validators.required),
		subject : new FormControl('',Validators.required),
	});

	/**
	 * Save data
	 */
	onSubmit() {
		if (this.form.valid) {
			const id = this.form.get('subject').value;
			this.subjectService.getSubjectById(id).subscribe(data => {
				this.res = data;
				const _cours = new Cours();
				_cours.idCourse = null;
				_cours.courseName = this.form.get('courseName').value;
				_cours.forum = this.result;
				_cours.subject = this.res;
				_cours.teacher = this.user;
				this.coursService.addCours(_cours).subscribe();
				this.viewLoading = false;
				this.dialogRef.close({});

			});
			this.form.reset();
		}
	}



	/**
	 * Close alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}



	/**
	 * Returns is title valid
	 */
	isTitleValid(): boolean {
		return (this.cours.courseName && this.cours.courseName.length > 0);
	}



}
