import {Component, Inject, OnInit} from '@angular/core';
import {Cours} from "../../../_models/cours.model";
import {Observable} from "rxjs";
import {Forum} from "../../../_models/ForumM.model";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../../core/reducers";
import {CoursServiceService} from "../../../services/cours-service.service";
import {ForumServiceService} from "../../../services/forum-service.service";
import {FormControl, FormGroup} from "@angular/forms";
import {SubjectServiceService} from "../../../services/subject-service.service";
import {Subject} from "../../../_models/Subject.model";

@Component({
  selector: 'edit-cours',
  templateUrl: './edit-cours.component.html',
  styleUrls: ['./edit-cours.component.scss']
})
export class EditCoursComponent implements OnInit {

  cours: Cours;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  private addedCours: Object;
  private result: Forum;
  private course : Cours
  private subjects: Observable<Subject[]>;
  private res: Subject;

  constructor(public dialogRef: MatDialogRef<EditCoursComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private coursService : CoursServiceService,
              private subjectService : SubjectServiceService) { }


  ngOnInit() {
    this.subjects = this.subjectService.getAllSubjects();
    console.log(this.data.course);
  }
  form : FormGroup = new FormGroup({
    subject : new FormControl(''),
  });


  /**
   * Save data
   */
  onSubmit() {
    const id = this.form.get('subject').value;
    this.subjectService.getSubjectById(id).subscribe(data => {
      this.res = data;
      const _cours = new Cours();
      _cours.idCourse = this.data.course.idCourse;
      _cours.courseName = this.data.course.courseName;
      _cours.forum = this.data.course.forum;
      _cours.subject = this.data.course.subject;
      _cours.teacher = this.data.course.teacher;
      this.coursService.updateCours(_cours).subscribe(data=>{
        this.addedCours=data;
        console.log( this.addedCours);
      });
      this.viewLoading = false;
      this.dialogRef.close({
        isEdit: true
      });

    });

  }



  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }



  /**
   * Returns is title valid
   */
  isTitleValid(): boolean {
    return (this.cours.courseName && this.cours.courseName.length > 0);
  }

}
