import {Component, Inject, OnInit} from '@angular/core';
import {Cours} from "../../../_models/cours.model";
import {User} from "../../../_models/user.model";
import {Question} from "../../../_models/question.model";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../../core/reducers";
import {QuestionServiceService} from "../../../services/question-service.service";
import {TokenStorageService} from "../../../services/token-storage.service";
import {UserServiceService} from "../../../services/user-service.service";
import {FormControl, FormGroup} from "@angular/forms";
import {FileComponent} from "../../../core/file/file.component";
import {Answer} from "../../../_models/answer.model";
import {AnswerServiceService} from "../../../services/answer-service.service";

@Component({
  selector: 'edit-answer',
  templateUrl: './edit-answer.component.html',
  styleUrls: ['./edit-answer.component.scss']
})
export class EditAnswerComponent implements OnInit {



  cours: Cours;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  private addedCours: Object;
  private id: any;
  private user: User;
  private added: Question;
  private file: string;
  private name: string;
  private type: string;

  constructor(public dialogRef: MatDialogRef<EditAnswerComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              public dialog: MatDialog,
              private answerService : AnswerServiceService,
              private tokenStorageService:TokenStorageService,
              private userService : UserServiceService) { }


  ngOnInit() {
    const user = this.tokenStorageService.getUser();
    this.id = user.id;
    this.userService.getUserById(this.id).subscribe(data=>{
      this.user=data;
    })
  }


  form : FormGroup = new FormGroup({
    idForum: new FormControl(null),
    forumName: new FormControl(''),
  });

  /**
   * Save data
   */
  onSubmit() {
    const _qst = new Answer();
    console.log(this.data);
    _qst.idAnswer = this.data.answer.idAnswer;
    _qst.answer = this.data.answer.answer;
    _qst.dateAnswer=new Date();
    _qst.question= this.data.answer.question;
    _qst.url=this.file;
    _qst.type=this.type;
    _qst.name=this.name;
    _qst.user=this.user;
    this.answerService.updateAnswer(_qst).subscribe(res => {
      this.viewLoading = false;
      this.file=null;
      this.type=null;
      this.name=null;
    });
    this.dialogRef.close({});


  }

  openBottomSheet(): void {
    const dialogRef = this.dialog.open(FileComponent, {data: {}});
    dialogRef.afterClosed().subscribe(res => {
      console.log(res.data);  console.log(res.type);
      this.file=res.url;
      this.type=res.type;
      this.name=res.data;
      if (!res) {
        return;
      }
    });
  }



  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }

}
