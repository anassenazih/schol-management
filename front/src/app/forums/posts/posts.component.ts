import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CoursServiceService} from "../../services/cours-service.service";
import {Observable} from "rxjs";
import {Cours} from "../../_models/cours.model";
import {Question} from "../../_models/question.model";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {BottomSheetOverviewExampleSheetComponent} from "../bottom-sheet-overview-example-sheet/bottom-sheet-overview-example-sheet.component";
import {AddForumComponent} from "../forum/add-forum/add-forum.component";
import {Forum} from "../../_models/ForumM.model";
import {FormControl, FormGroup} from "@angular/forms";
import {QuestionServiceService} from "../../services/question-service.service";
import {LayoutUtilsService, MessageType} from "../../core/_base/crud";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AnswerServiceService} from "../../services/answer-service.service";
import {FileComponent} from "../../core/file/file.component";
import {AddCoursComponent} from "../cours/add-cours/add-cours.component";
import {FileServiceService} from "../../services/file-service.service";
import {Answer} from "../../_models/answer.model";
import {TokenStorageService} from "../../services/token-storage.service";
import {UserServiceService} from "../../services/user-service.service";
import {User} from "../../_models/user.model";
import {Token} from "@angular/compiler";
import {loadQuestions} from "../store/question.actions";
import {loadAnswers} from "../store/answer.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";
import {loadCourss, loadCourssById} from "../store/cours.actions";
import {EditForumComponent} from "../forum/edit-forum/edit-forum.component";
import {EditPostComponent} from "./edit-post/edit-post.component";
import {EditAnswerComponent} from "./edit-answer/edit-answer.component";
import {Subscription} from "rxjs/internal/Subscription";

const basicPanel = {
  beforeCodeTitle: ' ',
};



@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']

})
export class PostsComponent implements OnInit {
  public questions: any[];
  public answers=[];
  public id: number;
  panelOpenState: boolean = false;
  exampleBasicPanel;
  exampleDisabledPanel;
  exampleAccordion;
  isDisabled = true;
  step = 0;
  private addedQst: Object;
  private result: Cours;
  private addedAnswer : Object;
  private qst: any;
    fileInfos: Observable<any>;
    private file: string;
    private type: any;
    display = false;
    displayPdf = false;
    private users: User;
    name:  string;
    private role: any;
    private ans: Question;
    private allQst= [];
    private allAnswers= [];
    private username: string;
    private id2: number;


  constructor(private route :ActivatedRoute,
              private fileService: FileServiceService,
              private coursService : CoursServiceService,
              private _bottomSheet: MatBottomSheet,
              private questionService : QuestionServiceService,
              public dialog: MatDialog,
              private layoutUtilsService: LayoutUtilsService,
              private answerService : AnswerServiceService,
              private tokenStorageService:TokenStorageService,
              private userService  :UserServiceService,
              private store: Store<AppState>,
              private cdr: ChangeDetectorRef
              ) {
  }
    ngOnInit() {
        this.role = this.tokenStorageService.getUser().roles;
        const user = this.tokenStorageService.getUser();
        this.username=user.username;
        this.id2 = this.tokenStorageService.getUser().id;
        this.id = this.route.snapshot.params['idCourse'];
        this.store.dispatch(loadQuestions({request: this.id}));
        this.store.dispatch(loadCourssById({request: this.id}));
        this.store.select(state => state.questions).subscribe(data => {
                this.allQst = JSON.parse(JSON.stringify(data.questions));
            });
            //this.questions = this.coursService.getQuestions( this.id );
            this.exampleBasicPanel = basicPanel;
            this.store.select(state => state.coursbyId).subscribe(data => {
                this.result = JSON.parse(JSON.stringify(data.coursbyId));
            });
            this.userService.getUserById(this.id2).subscribe(data => {
                this.users = data;
            });

    }

    show(idQuestion: number){
/*
        this.store.dispatch(loadAnswers({request: idQuestion}));
        let subscription:Subscription =  this.store.select(state => state.answers).subscribe(data => {
            this.questions.forEach(e =>{
                if(e.idQuestion === idQuestion ){
                    e.answers = JSON.parse(JSON.stringify(data.answers));
                }
            })
        });
        //this.questions[i].responses = this.store.select(state => state.answers.answers);
        this.cdr.markForCheck();
        subscription.unsubscribe();*/
    }
    openBottomSheet(): void {
        const dialogRef = this.dialog.open(FileComponent, {data: {}});
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                this.file=null;
                this.type=null;
                this.name=null;
            }else if(res){
                this.file=res.url;
                this.type=res.type;
                this.name=res.data;
            }


        });
    }


    form : FormGroup = new FormGroup({
        idQuestion: new FormControl(null),
        question: new FormControl(''),
        idAnswer: new FormControl(null),
        answer: new FormControl(''),
    });

    Add(){
        this.panelOpenState=true;
        const _qst = new Question();
        const _saveMessage = `Ajouté avec succès.`;
        const _messageType =  MessageType.Create;
        _qst.idQuestion = null;
        _qst.question = this.form.get('question').value;
        _qst.dateQuestion=new Date();
        _qst.course= this.result;
        _qst.url=this.file;
        _qst.type=this.type;
        _qst.name=this.name;
        _qst.user=this.users;
        this.questionService.addQuestion(_qst).subscribe(res => {
            this.ngOnInit();
            this.panelOpenState = !this.panelOpenState;
            this.addedQst = res;
            this.file=null;
            this.type=null;
            this.name=null;
        });

        this.form.reset();
        this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

    }
    togglePanel() {
        this.panelOpenState = !this.panelOpenState;
    }

    Answer (idQuestion) {
        console.log('answer');
        this.questionService.getQuestionById(idQuestion).subscribe(result => {
            this.ans = result;
            console.log('ans',this.ans);
            this.addAnswer( this.ans);
        });
    }


    addAnswer(ans: Question){
        const _saveMessage = `Ajouté avec succès.`;
        const _messageType = MessageType.Create;
        const _answer = new Answer();
        _answer.idAnswer = null;
        _answer.answer = this.form.get('answer').value;
        _answer.question= ans;
        _answer.dateAnswer=new Date();
        _answer.url=this.file;
        _answer.type=this.type;
        _answer.name=this.name;
        _answer.user=this.users;
        this.answerService.addAnswer(_answer).subscribe(data => {
            this.ngOnInit();
            this.addedAnswer = data;
            this.file=null;
            this.type=null;
            this.name=null;
        }, error1 => {
            console.log(error1)
        });

        this.form.reset();
        this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);
    }

    showDialogPdf(){
        this.displayPdf = true;
    }

    editQst(q: any) {
        const newQst = new Question();
        newQst.clear(); // Set all defaults fields
        const _saveMessage = `Modifié avec succès.`;
        const _messageType =  MessageType.Update;
        const dialogRef = this.dialog.open(EditPostComponent, {data: {question:q,course:this.result}});
        dialogRef.afterClosed().subscribe(res => {
            this.store.dispatch(loadQuestions({request: this.id}));
            if (!res) {
                return;
            }
            this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

        });


    }

    deleteQst(idQuestion: any) {
        const _forum = 'Question' ;
        const _description = 'Vous êtes sûr de vouloir supprimer cette question?';
        const _waitDesciption = 'Suppression de question ...';
        const _deleteMessage = `la question a été supprimée`;

        const dialogRef = this.layoutUtilsService.deleteElement(_forum , _description, _waitDesciption);
        dialogRef.afterClosed().subscribe(res => {

            if (res) {
                this.questionService.deleteQuestion(idQuestion).subscribe(data=>{
                    this.ngOnInit();
                });
                this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
            }


        });
    }

    deleteAnswer(idAnswer: any) {
        const _forum = 'Commentaire' ;
        const _description = 'Vous êtes sûr de vouloir supprimer ce commentaire?';
        const _waitDesciption = 'Suppression de commentaire ...';
        const _deleteMessage = `votre commentaire a été supprimé`;

        const dialogRef = this.layoutUtilsService.deleteElement(_forum , _description, _waitDesciption);
        dialogRef.afterClosed().subscribe(res => {

            if (res) {
                this.answerService.deleteAnswer(idAnswer).subscribe(data=>{
                    this.ngOnInit();
                });
                this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
            }


        });

    }

    editAnswer(a: any) {
        const newQst = new Answer();
        newQst.clear(); // Set all defaults fields
        const _saveMessage = `Modifié avec succès.`;
        const _messageType =  MessageType.Update;
        const dialogRef = this.dialog.open(EditAnswerComponent, {data: {answer:a }});
        dialogRef.afterClosed().subscribe(res => {
            this.store.dispatch(loadQuestions({request: this.id}));
            if (!res) {
                return;
            }
            this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

        });

    }
}
