import {Component, Inject, OnInit} from '@angular/core';
import {Cours} from "../../../_models/cours.model";
import {User} from "../../../_models/user.model";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../../core/reducers";
import {ForumServiceService} from "../../../services/forum-service.service";
import {TokenStorageService} from "../../../services/token-storage.service";
import {UserServiceService} from "../../../services/user-service.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Forum} from "../../../_models/ForumM.model";
import {QuestionServiceService} from "../../../services/question-service.service";
import {Question} from "../../../_models/question.model";
import {FileComponent} from "../../../core/file/file.component";

@Component({
  selector: 'edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {



  cours: Cours;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  private addedCours: Object;
  private id: any;
  private user: User;
  private added: Question;
  private file: string;
  private name: string;
  private type: string;

  constructor(public dialogRef: MatDialogRef<EditPostComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              public dialog: MatDialog,
              private questionService : QuestionServiceService,
              private tokenStorageService:TokenStorageService,
              private userService : UserServiceService) { }


  ngOnInit() {
    console.log(this.data.course);
    const user = this.tokenStorageService.getUser();
    this.id = user.id;
    this.userService.getUserById(this.id).subscribe(data=>{
      this.user=data;
    })
  }


  form : FormGroup = new FormGroup({
    idForum: new FormControl(null),
    forumName: new FormControl(''),
  });

  /**
   * Save data
   */
  onSubmit() {
    const _qst = new Question();
    console.log(this.data.question.course);
    _qst.idQuestion = this.data.question.idQuestion;
    _qst.question = this.data.question.question;
    _qst.dateQuestion=new Date();
    _qst.course= this.data.course;
    _qst.url=this.file;
    _qst.type=this.type;
    _qst.name=this.name;
    _qst.user=this.user;
    _qst.answers=this.data.question.answers;
    this.questionService.updateQuestion(_qst).subscribe(res => {
      this.viewLoading = false;
      this.file=null;
      this.type=null;
      this.name=null;
    });
    this.dialogRef.close({});


  }

  openBottomSheet(): void {
    const dialogRef = this.dialog.open(FileComponent, {data: {}});
    dialogRef.afterClosed().subscribe(res => {
      console.log(res.data);  console.log(res.type);
      this.file=res.url;
      this.type=res.type;
      this.name=res.data;
      if (!res) {
        return;
      }
    });
  }



  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }



}
