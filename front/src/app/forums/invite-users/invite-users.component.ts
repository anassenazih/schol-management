import {Component, Inject, OnInit} from '@angular/core';
import {Cours} from "../../_models/cours.model";
import {Observable} from "rxjs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";
import {FormControl, FormGroup} from "@angular/forms";
import {UserServiceService} from "../../services/user-service.service";
import {CoursServiceService} from "../../services/cours-service.service";
import {LayoutUtilsService, MessageType} from "../../core/_base/crud";
import {ActivatedRoute} from "@angular/router";
import {User} from "../../_models/user.model";
import {Registration} from "../../_models/registration.model";

@Component({
  selector: 'invite-users',
  templateUrl: './invite-users.component.html',
  styleUrls: ['./invite-users.component.scss']
})
export class InviteUsersComponent implements OnInit {

  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  private addedCours: Object;
  private id: string;
  private result: Cours;
  private email: string;
  private invit: Object;

  constructor(private route :ActivatedRoute,
              public dialogRef: MatDialogRef<InviteUsersComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private userService : UserServiceService,
              private coursService : CoursServiceService,
              private layoutUtilsService: LayoutUtilsService,) { }


  ngOnInit() {


  }


  form : FormGroup = new FormGroup({
    email: new FormControl(''),
  });


  /**
   * Save data
   */
  onSubmit() {
    const _saveMessage = `Envoyé.`;
    const _messageType =  MessageType.Create;
    this.email=this.form.get('email').value;
    console.log(this.email);
    this.id = this.data.idCourse;
    this.coursService.getCourseById(this.id).subscribe(data =>{
      this.result=data;
      console.log('Course',this.result); console.log('email', this.email);
      this.coursService.inviteUser(this.email,this.result).subscribe();
    });
      this.dialogRef.close({});
    this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);



  }



  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }



}
