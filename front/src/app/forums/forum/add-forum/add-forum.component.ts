import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {Cours} from "../../../_models/cours.model";
import {Observable} from "rxjs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../../core/reducers";
import {CoursServiceService} from "../../../services/cours-service.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Forum} from "../../../_models/ForumM.model";
import {ForumServiceService} from "../../../services/forum-service.service";
import {TokenStorageService} from "../../../services/token-storage.service";
import {User} from "../../../_models/user.model";
import {UserServiceService} from "../../../services/user-service.service";

@Component({
  selector: 'add-forum',
  templateUrl: './add-forum.component.html',
  styleUrls: ['./add-forum.component.scss']
})
export class AddForumComponent implements OnInit {

	cours: Cours;
	hasFormErrors = false;
	viewLoading = false;
	loadingAfterSubmit = false;
	private addedCours: Object;
	private id: any;
	private user: User;


	constructor(public dialogRef: MatDialogRef<AddForumComponent>,
				@Inject(MAT_DIALOG_DATA) public data: any,
				private store: Store<AppState>,
				private forumService : ForumServiceService,
				private tokenStorageService:TokenStorageService,
				private userService : UserServiceService) { }


	ngOnInit() {
		const user = this.tokenStorageService.getUser();
		this.id = user.id;
		this.userService.getUserById(this.id).subscribe(data=>{
			this.user=data;
		})
	}


	form : FormGroup = new FormGroup({
		idForum: new FormControl(null),
		forumName: new FormControl('',Validators.required)
	});


	/**
	 * Save data
	 */
	onSubmit() {
		if (this.form.valid){
			const _forum = new Forum();
			_forum.idForum = null;
			_forum.forumName = this.form.get('forumName').value;
			console.log(this.user);
			_forum.administrator=this.user;
			this.forumService.addForum(_forum).subscribe();
			this.dialogRef.close({
				isEdit: false
			});
			this.form.reset();
		}


	}



	/**
	 * Close alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}



	/**
	 * Returns is title valid
	 */
	isTitleValid(): boolean {
		return (this.cours.courseName && this.cours.courseName.length > 0);
	}



}
