import {Component, Inject, OnInit} from '@angular/core';
import {Cours} from "../../../_models/cours.model";
import {ActivatedRoute} from "@angular/router";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../../core/reducers";
import {UserServiceService} from "../../../services/user-service.service";
import {CoursServiceService} from "../../../services/cours-service.service";
import {LayoutUtilsService, MessageType} from "../../../core/_base/crud";
import {FormControl, FormGroup} from "@angular/forms";
import {ForumServiceService} from "../../../services/forum-service.service";
import {Forum} from "../../../_models/ForumM.model";

@Component({
  selector: 'invit',
  templateUrl: './invit.component.html',
  styleUrls: ['./invit.component.scss']
})
export class InvitComponent implements OnInit {


  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  private addedCours: Object;
  private id: number;
  private result: Forum;
  private email: string;
  private invit: Object;

  constructor(private route :ActivatedRoute,
              public dialogRef: MatDialogRef<InvitComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private forumService : ForumServiceService,
              private coursService : CoursServiceService,
              private layoutUtilsService: LayoutUtilsService,) { }


  ngOnInit() {


  }


  form : FormGroup = new FormGroup({
    email: new FormControl(''),
  });


  /**
   * Save data
   */
  onSubmit() {
    const _saveMessage = `Envoyé.`;
    const _messageType =  MessageType.Create;
    this.email=this.form.get('email').value;
    console.log(this.email);
    this.id = this.data.idForum;
      this.forumService.inviteUser(this.email, this.id).subscribe();
    this.dialogRef.close({});
    this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);



  }



  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }


}
