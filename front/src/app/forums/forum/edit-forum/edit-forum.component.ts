import {Component, Inject, OnInit} from '@angular/core';
import {Cours} from "../../../_models/cours.model";
import {Observable} from "rxjs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../../core/reducers";
import {ForumServiceService} from "../../../services/forum-service.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Forum} from "../../../_models/ForumM.model";
import {TokenStorageService} from "../../../services/token-storage.service";
import {UserServiceService} from "../../../services/user-service.service";
import {User} from "../../../_models/user.model";

@Component({
  selector: 'edit-forum',
  templateUrl: './edit-forum.component.html',
  styleUrls: ['./edit-forum.component.scss']
})
export class EditForumComponent implements OnInit {


  cours: Cours;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  private addedCours: Object;
  private id: any;
  private user: User;

  constructor(public dialogRef: MatDialogRef<EditForumComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private forumService : ForumServiceService,
              private tokenStorageService:TokenStorageService,
              private userService : UserServiceService) { }


  ngOnInit() {
    const user = this.tokenStorageService.getUser();
    this.id = user.id;
    this.userService.getUserById(this.id).subscribe(data=>{
      this.user=data;
    })
  }


  form : FormGroup = new FormGroup({
    idForum: new FormControl(null),
    forumName: new FormControl('',Validators.required),
  });

  /**
   * Save data
   */
  onSubmit() {
      const _forum = new Forum();
      _forum.idForum = this.data.idForum;
      _forum.forumName = this.data.name;
      _forum.administrator = this.user;
      this.forumService.updateForum(_forum).subscribe(data => {
        this.addedCours = data;
        this.dialogRef.close({
          isEdit: false
        });
      }, error1 => {
        console.log(error1)
      });



  }



  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }



  /**
   * Returns is title valid
   */
  isTitleValid(): boolean {
    return (this.cours.courseName && this.cours.courseName.length > 0);
  }

}
