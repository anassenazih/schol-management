import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CoursServiceService} from "../../services/cours-service.service";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {LayoutUtilsService, MessageType} from "../../core/_base/crud";
import {ForumServiceService} from "../../services/forum-service.service";
import {Forum} from "../../_models/ForumM.model";
import {AddForumComponent} from "./add-forum/add-forum.component";
import {InviteUsersComponent} from "../invite-users/invite-users.component";
import {Observable} from "rxjs";
import {EditForumComponent} from "./edit-forum/edit-forum.component";
import {TokenStorageService} from "../../services/token-storage.service";
import {AppState} from "../../core/reducers";
import {loadForumss} from "../store/forums.actions";
import {Store} from "@ngrx/store";
import {loadNotes} from "../../notes/store/notes.actions";
import {InvitComponent} from "./invit/invit.component";

const cardWithTitle = {
	beforeCodeTitle: ' ',

};

@Component({
  selector: 'forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.scss']
})
export class ForumComponent implements OnInit {

	private exampleCardWithTitle: any;
	public forum :Observable<Forum[]>;
	private role: any;
	private allForums = [];
	forums: any[] = [];
	constructor( private forumService : ForumServiceService,
				 public dialog: MatDialog,
				 public snackBar: MatSnackBar,
				 private store: Store<AppState>,
				 private layoutUtilsService: LayoutUtilsService,
				 private tokenStorageService : TokenStorageService,
				 private cdr: ChangeDetectorRef) {
		this.forumService.componentMethodCalled$.subscribe(
			() => {
				this.ngOnInit();
			}
		);



	}

	ngOnInit() {
		this.store.dispatch(loadForumss({request:this.forums}));
		this.store.select(state => state.forums).subscribe(data => {
				console.log('data ==', data.forums);
				this.allForums = JSON.parse(JSON.stringify(data.forums));
				this.forums =[];
				this.allForums.forEach(e => {
					this.forums.push(
						{idForum: e.idForum,
							forumName:  e.forumName,
						}
					)
				});
				this.cdr.markForCheck();
			}
		);
		this.exampleCardWithTitle = cardWithTitle;
	//	this.forum=this.forumService.getAllForums();
		this.role = this.tokenStorageService.getUser().roles;
	}


	addForum() {
		const newForum = new Forum();
		newForum.clear(); // Set all defaults fields
		const _saveMessage = `Ajouté avec succès.`;
		const _messageType =  MessageType.Create;
		const dialogRef = this.dialog.open(AddForumComponent, {data: {}});
		dialogRef.afterClosed().subscribe(res => {
			this.forumService.callComponentMethod();
			if (!res) {
				return;
			}
			this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

		});
	}

	inviteUser(idForum) {

		const dialogRef = this.dialog.open(InvitComponent, {data: {idForum :idForum}});
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
		});
	}

	edit(c: Forum){
		const newForum = new Forum();
		newForum.clear(); // Set all defaults fields
		const _saveMessage = `Modifié avec succès.`;
		const _messageType =  MessageType.Update;
		const dialogRef = this.dialog.open(EditForumComponent, {data: {idForum:c.idForum ,name:c.forumName}});
		dialogRef.afterClosed().subscribe(res => {
			this.forumService.callComponentMethod();
			if (!res) {
				return;
			}
			this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

		});
	}

	delete(idForum: number){
		const _forum = 'Forum' ;
		const _description = 'Vous êtes sûr de vouloir supprimer ce forum?';
		const _waitDesciption = 'Suppression de forum ...';
		const _deleteMessage = `Forum a été supprimé`;

		const dialogRef = this.layoutUtilsService.deleteElement(_forum , _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {

			if (res) {
				this.forumService.deleteForum(idForum).subscribe(data=>{
             this.ngOnInit();
				});
				this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
			}


		});
	}


}
