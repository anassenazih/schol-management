// Angular
import {LOCALE_ID, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// Translate
import { TranslateModule } from '@ngx-translate/core';
import { PartialsModule } from '../views/partials/partials.module';

// Services
import { HttpUtilsService, TypesUtilsService, InterceptService, LayoutUtilsService} from '../core/_base/crud';
// Shared
// Components
import {DialogModule} from 'primeng/dialog';

import { PdfViewerModule } from 'ng2-pdf-viewer';

// Material
import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatExpansionModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule
} from '@angular/material';
import {ActionNotificationComponent} from '../views/partials/content/crud';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatToolbarModule} from "@angular/material/toolbar";
import {ForumsComponent} from "./forums.component";
import {MaterialPreviewModule} from "../views/partials/content/general/material-preview/material-preview.module";
import { CoursComponent } from './cours/cours.component';
import {PostsComponent} from "./posts/posts.component";
import { AddCoursComponent } from './cours/add-cours/add-cours.component';
import { ForumComponent } from './forum/forum.component';
import {AddForumComponent} from "./forum/add-forum/add-forum.component";
import { InviteUsersComponent } from './invite-users/invite-users.component';
import { BottomSheetOverviewExampleSheetComponent } from './bottom-sheet-overview-example-sheet/bottom-sheet-overview-example-sheet.component';
import {MatListModule} from "@angular/material/list";
import {MatBottomSheet, MatBottomSheetModule} from "@angular/material/bottom-sheet";
import {NgbootstrapModule} from "../views/pages/ngbootstrap/ngbootstrap.module";
import { EditCoursComponent } from './cours/edit-cours/edit-cours.component';
import { EditForumComponent } from './forum/edit-forum/edit-forum.component';
import { EditPostComponent } from './posts/edit-post/edit-post.component';
import { FileComponent } from '../core/file/file.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { LiveCourseComponent } from './cours/live-course/live-course.component';
import { ForumsEffects } from './store/forums.effects';
import { CoursEffects } from './store/cours.effects';
import { QuestionEffects } from './store/question.effects';
import { AnswerEffects } from './store/answer.effects';
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import { EditAnswerComponent } from './posts/edit-answer/edit-answer.component';
import { InvitComponent } from './forum/invit/invit.component';
import {CoreModule} from "../core/core.module";

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');

const routes: Routes = [
	{
		path: '',
		component: ForumsComponent,
		children: [
			{
				path: 'forum',
				component: ForumComponent,
			},
			{
				path: 'forum/:idForum/cours',
				component: CoursComponent
			},
			{
				path: 'forum/:idForum/cours/:idCourse/posts',
				component: PostsComponent
			},


			{
				path: 'cours/add',
				component: AddCoursComponent
			},
			{
				path: 'cours/add:id',
				component: AddCoursComponent
			},
			{
				path: 'cours/edit',
				component: EditCoursComponent
			},
			{
				path: 'cours/edit/:id',
				component: EditCoursComponent
			},
			{
				path: 'forum/add',
				component: AddForumComponent
			},
			{
				path: 'forum/add:id',
				component: AddForumComponent
			},
			{
				path: 'forum/edit',
				component: EditForumComponent
			},
			{
				path: 'forum/edit/:id',
				component: EditForumComponent
			},
			{
				path: 'forum/idForum/invite',
				component: InviteUsersComponent
			},
			{
				path: 'upload',
				component: BottomSheetOverviewExampleSheetComponent
			},
			{
				path: 'file',
				component: FileComponent
			},
			{
				path: 'post/edit',
				component: EditPostComponent
			},
			{
				path: 'post/edit/:id',
				component: EditPostComponent
			},
			{
				path: 'answer/edit',
				component: EditAnswerComponent
			},
			{
				path: 'answer/edit/:id',
				component: EditAnswerComponent
			},
		]
	}
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        PdfViewerModule,
        PartialsModule,
		CoreModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        MatButtonModule,
        MatMenuModule,
        MatSelectModule,
        MatInputModule,
        MatTableModule,
        MatAutocompleteModule,
        MatRadioModule,
        MatIconModule,
        MatNativeDateModule,
        MatProgressBarModule,
        MatDatepickerModule,
        MatCardModule,
        MatPaginatorModule,
        MatSortModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatExpansionModule,
        MatTabsModule,
        MatTooltipModule,
        MatDialogModule,
        MatGridListModule,
        MatToolbarModule,
        MaterialPreviewModule,
        MatListModule,
        MatBottomSheetModule,
        NgbootstrapModule,
        DialogModule,
        EffectsModule.forFeature([ForumsEffects, CoursEffects, QuestionEffects, AnswerEffects]),
        NgbDropdownModule
    ],
	providers: [
		{provide: LOCALE_ID, useValue: 'fr' },
		InterceptService,
		{
        	provide: HTTP_INTERCEPTORS,
       	 	useClass: InterceptService,
			multi: true
		},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'kt-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		HttpUtilsService,
		TypesUtilsService,
		LayoutUtilsService
	],
	entryComponents: [
		ActionNotificationComponent,
		EditPostComponent,
		EditAnswerComponent,
		InvitComponent
	],
	declarations: [
	ForumsComponent	,
	PostsComponent,
	CoursComponent,
	AddCoursComponent,
	ForumComponent,
		AddForumComponent,
		InviteUsersComponent,
		BottomSheetOverviewExampleSheetComponent,
		EditCoursComponent,
		EditForumComponent,
		EditPostComponent,
		LiveCourseComponent,
		EditAnswerComponent,
		InvitComponent

	]
})
export class ForumsModule {}
