import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {TokenStorageService} from "../services/token-storage.service";

@Component({
  selector: 'forums',
  templateUrl: './forums.component.html',
  styleUrls: ['./forums.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ForumsComponent implements OnInit {


  constructor() { }

  ngOnInit() {

  }

}
