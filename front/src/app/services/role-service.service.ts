import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {server} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RoleServiceService implements OnInit {

	private componentMethodCallSource = new Subject<any>();
	componentMethodCalled$ = this.componentMethodCallSource.asObservable();

  constructor(private http:HttpClient) { }
	ngOnInit(){

	}

	addRole(role){
		return this.http.post(server + "roles/",role).pipe(map(resp=>resp));
	}
	getAllRoles():Observable<any>{
		return this.http.get(server + "roles").pipe(map(resp=>resp));
	}
	deleteRole(idRole:number){
		return this.http.delete(server + "roles/"+idRole).pipe(map(resp=>resp));
	}
	updateRole(role){
		return this.http.put(server + "roles/",role).pipe(map(resp=>resp));

	}


	getRoleById(idRole):Observable<any>{
		return this.http.get(server + "roles/"+idRole);

	}
	callComponentMethod() {
		this.componentMethodCallSource.next();
	}
}
