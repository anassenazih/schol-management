import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {server} from "../../environments/environment";

@Injectable()
export class NotesService implements OnInit {
	private componentMethodCallSource = new Subject<any>();
	componentMethodCalled$ = this.componentMethodCallSource.asObservable();
	constructor(private http:HttpClient) { }

	ngOnInit(){
	}

	getNotesByStudent(studentId: any){
		return this.http.get(server + 'notes/student/'+ studentId)
	}

	addNote(note){
		console.log('test ok');

		return  this.http.post(server + 'addNoteCustomized',
			{
				idStudent: note.idStudent,
				idCourse: note.idCourse,
				note: note.note,
				remarque: note.remarque
			})
	}

	modifyNote(note){
		return this.http.put(server + 'notes', {});
	}

	deleteNote(id){
		return this.http.delete(server + id);
	}

	getCourseByTeacher(idTeacher): any{
        return this.http.get(server + 'coursesByIdTeacher/'+ idTeacher);
	}

	getStudents(){
        return this.http.get(server + 'students');

    }
	getNotesByCourse(id){
        return this.http.get(server + 'notesByIdCourse/' + id);

    }

	getGroupes(){
        return this.http.get(server + 'groupss' );

    }

	getCourses(){
        return this.http.get(server + 'courses' );

    }

	callComponentMethod() {
		this.componentMethodCallSource.next();
	}

    uploadFile(file): Observable<any>{
		console.log('test ', file);
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('tokenJWT'));
        const httpOptions = {
            headers: headers_object
        };

        const formdata: FormData = new FormData();

        formdata.append('file', file);
        return this.http.post(server + "import-excel/",formdata, httpOptions);
    }
}