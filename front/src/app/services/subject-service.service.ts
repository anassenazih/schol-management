import { Injectable } from '@angular/core';
import {server} from "../../environments/environment";
import {map} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SubjectServiceService {
  private componentMethodCallSource = new Subject<any>();
  componentMethodCalled$ = this.componentMethodCallSource.asObservable();

  constructor(private http:HttpClient) { }
  ngOnInit(){

  }
  addSubject(subject,withBack: boolean = false):Observable<any>{
    return this.http.post(server + "subjects/",subject);


  }
  getAllSubjects():Observable<any>{
    return this.http.get(server + "subjects/");
  }
  deleteSubject(idSubject:number):Observable<any>{
    return this.http.delete(server + "subjects/"+idSubject);
  }
  updateSubject(subject,withBack: boolean = false):Observable<any>{
    return this.http.put(server + "subjects/",subject);

  }

  getSubjectById(idSubject:number): Observable<any>{
    return this.http.get(server + "subjects/"+idSubject);
  }
  callComponentMethod() {
    this.componentMethodCallSource.next();
  }

}
