import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {map} from "rxjs/operators";
import {server} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ForumServiceService {
	private componentMethodCallSource = new Subject<any>();
	componentMethodCalled$ = this.componentMethodCallSource.asObservable();

	constructor(private http:HttpClient) { }
	ngOnInit(){

	}

	addForum(forum,withBack: boolean = false):Observable<any>{
		return this.http.post(server + "forums/",forum);


	}
	getAllForums():Observable<any>{
		return this.http.get(server + "forums/");
	}
	deleteForum(idForum:number):Observable<any>{
		return this.http.delete(server + "forums/"+idForum);
	}
	updateForum(forum,withBack: boolean = false):Observable<any>{
		return this.http.put(server + "forums/",forum);

	}

	getForumById(idForum:number): Observable<any>{
		return this.http.get(server + "forums/"+idForum);
	}

	getCourses(idForum):Observable<any>{
		return this.http.get(server + "forums/courses/"+idForum);

	}
	inviteUser(email,idForum): Observable<any>{
		var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('tokenJWT'));
		const httpOptions = {
			headers: headers_object
		};

		return this.http.put(server + "inviteUserToForum/"+email+"/"+idForum,httpOptions);


	}

	callComponentMethod() {
		this.componentMethodCallSource.next();
	}
}
