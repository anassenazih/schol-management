import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {map} from "rxjs/operators";
import {server} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CoursServiceService {

	private componentMethodCallSource = new Subject<any>();
	componentMethodCalled$ = this.componentMethodCallSource.asObservable();

	constructor(private http:HttpClient) { }
	ngOnInit(){

	}

	addCours(cours,withBack: boolean = false):Observable<any>{
		return this.http.post(server + "courses/",cours);


	}
	getAllCours():Observable<any>{
		return this.http.get(server + "courses/");
	}
	deleteCours(idCours:number){
		return this.http.delete(server + "courses/"+idCours);
	}
	updateCours(cours): Observable<Object>{
		return this.http.put(server + "courses/",cours);

	}
	getQuestions(idCours){
		return this.http.get(server + "questionsByIdCourse/"+idCours);

	}

	getCourseById(idCours): Observable<any>{
		return this.http.get(server + "courses/"+idCours);
	}
	getCourseByIdTeacher(idTeacher): Observable<any>{
		return this.http.get(server + "courseUsersByIdTeacher/"+idTeacher);
	}
	getCourseByIdForumAndIdUser(idForum,idUser): Observable<any>{
		return this.http.get(server + "coursesByIdForumAndIdUser/"+idForum+"/"+idUser);
	}

	inviteUser(email,course): Observable<any>{
		var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('tokenJWT'));
		const httpOptions = {
			headers: headers_object
		};
		let data = {
			"email": email,
			"course": course
		};
		let body = JSON.stringify(data );
		let params = new HttpParams()
			.set('email',email )
			.set('course', course)

		return this.http.put(server + "courses/inviteUser/"+email,course,httpOptions);
	}



	callComponentMethod() {
		this.componentMethodCallSource.next();
	}
}
