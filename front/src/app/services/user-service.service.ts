import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {server} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserServiceService implements OnInit {

	private componentMethodCallSource = new Subject<any>();
	componentMethodCalled$ = this.componentMethodCallSource.asObservable();

	constructor(private http:HttpClient) { }
	ngOnInit(){

	}

	addUser(user){
		return this.http.post(server + "users/",user).pipe(map(resp=>resp));
	}
	getAllUsers():Observable<any>{
		return this.http.get(server + "users/").pipe(map(resp=>resp));
	}
	deleteUser(idUser:number){
		return this.http.delete(server + "users/"+idUser).pipe(map(resp=>resp));
	}
	updateUser(user){
		return this.http.put(server + "users/",user).pipe(map(resp=>resp));

	}
	getUserById(idUser): Observable<any>{
		return this.http.get(server + "users/"+idUser);

	}

	getRolesByUserId(idUser):Observable<any>{
		return this.http.get(server + "user/roles/"+idUser);

	}

	callComponentMethod() {
		this.componentMethodCallSource.next();
	}



}
