import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map} from "rxjs/operators";
import {server} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ScheduleServiceService {
  private componentMethodCallSource = new Subject<any>();
  componentMethodCalled$ = this.componentMethodCallSource.asObservable();

  constructor(private http:HttpClient) { }
  ngOnInit(){

  }

  addSchedule(schedule){
    return this.http.post(server + "schedules/",schedule);

  }
  getAllSchedules():Observable<any>{
    return this.http.get(server + "schedules/");
  }

  downloadScheduleFile(fileName){
    return this.http.get( server +"downloadScheduleFile/"+fileName);

  }
  deleteSchedule(idSchedule:number){
    return this.http.delete(server + "schedules/"+idSchedule).pipe(map(resp=>resp));
  }
  updateSchedule(schedule): Observable<Object>{
    return this.http.put(server + "courses/",schedule);

  }

  uploadFile(file): Observable<any>{
    var headers_object = new HttpHeaders().set("Authorization", "Bearer "+ localStorage.getItem('tokenJWT'));
    const httpOptions = {
      headers: headers_object
    };

    const formdata: FormData = new FormData();

    formdata.append('file', file);
    return this.http.post(server + "uploadFile/",formdata, httpOptions);


  }
  downloadFile(fileName):Observable<any>{
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('tokenJWT'));
    const httpOptions = {
      headers: headers_object
    };

    return this.http.get(server + "downloadFile/"+fileName, httpOptions);


  }

  callComponentMethod() {
    this.componentMethodCallSource.next();
  }
}
