import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {server} from "../../environments/environment";
import {JwtHelperService} from "@auth0/angular-jwt";
import {TokenStorageService} from "./token-storage.service";



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,public jwtHelper: JwtHelperService) { }

  login(credentials): Observable<any> {
    return this.http.post(server + 'api/auth/signin', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  register(user): Observable<any> {
    return this.http.post(server + 'api/auth/signup', {
      username: user.username,
      email: user.email,
      password: user.password,
      role : user.role,
    }, httpOptions);
  }
  public isAuthenticated(): boolean {
    const token =localStorage.getItem('tokenJWT');
    // Check whether the token is expired and return
    // true or false
    return !this.jwtHelper.isTokenExpired(token);
  }
}
