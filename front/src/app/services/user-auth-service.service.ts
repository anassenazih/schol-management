import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {server} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserAuthServiceService {

  constructor(private http: HttpClient) { }

  getPublicContent(): Observable<any> {
    return this.http.get(server + 'all', { responseType: 'text' });
  }

  getStudentBoard(): Observable<any> {
    return this.http.get(server + 'student', { responseType: 'text' });
  }
  getParentBoard(): Observable<any> {
    return this.http.get(server + 'parent', { responseType: 'text' });
  }

  getTeacherBoard(): Observable<any> {
    return this.http.get(server + 'forums', { responseType: 'text' });
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(server + 'admin', { responseType: 'text' });
  }
}
