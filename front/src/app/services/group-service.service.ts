import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {server} from "../../environments/environment";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class GroupServiceService {


  private componentMethodCallSource = new Subject<any>();
  componentMethodCalled$ = this.componentMethodCallSource.asObservable();

  constructor(private http:HttpClient) { }
  ngOnInit(){

  }

  addGroup(group,withBack: boolean = false):Observable<any>{
    return this.http.post(server + "groups/",group);


  }
  getAllGroup():Observable<any>{
    return this.http.get(server + "groupss/");
  }
  deleteGroup(idGroup:number):Observable<any>{
    return this.http.delete(server + "groups/"+idGroup);
  }
  updateGroup(group): Observable<Object>{
    return this.http.put(server + "groups/",group);

  }


  getGroupById(idGroup): Observable<any>{
    return this.http.get(server + "groups/"+idGroup);
  }



  callComponentMethod() {
    this.componentMethodCallSource.next();
  }
}
