import { Injectable } from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import {AuthService} from "./auth.service";
import {ActivatedRouteSnapshot} from "@angular/router";
import decode from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})

@Injectable()
export class RoleGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole;
    const token = localStorage.getItem('tokenJWT');
    // decode the token to get its payload
    const tokenPayload = localStorage.getItem('role');
    if (
        !this.auth.isAuthenticated() ||
        tokenPayload !== expectedRole
    ) {
      console.log('fail');
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
