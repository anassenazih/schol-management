import { Injectable } from '@angular/core';
import {map} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {server} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class QuestionServiceService {
  private componentMethodCallSource = new Subject<any>();
  componentMethodCalled$ = this.componentMethodCallSource.asObservable();

  constructor(private http:HttpClient) { }
  ngOnInit(){

  }

  addQuestion(question):Observable<any>{
    return this.http.post(server + "questions/",question);


  }
  getQuestionById(idQuestion): Observable<any>{
    return this.http.get(server + "questions/"+idQuestion);
  }

  getAnswers(idQuestion):Observable<any>{
    return this.http.get(server + "answersByIdQuestion/"+idQuestion);


  }
  getAllQuestions():Observable<any>{
    return this.http.get(server + "questions/");
  }
  deleteQuestion(idQuestion:number):Observable<any>{
    return this.http.delete(server + "questions/"+idQuestion);
  }
  updateQuestion(question,withBack: boolean = false):Observable<any>{
    return this.http.put(server + "questions/",question);

  }


  callComponentMethod() {
    this.componentMethodCallSource.next();
  }
}
