import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {server} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AnswerServiceService {
  private componentMethodCallSource = new Subject<any>();
  componentMethodCalled$ = this.componentMethodCallSource.asObservable();

  constructor(private http:HttpClient) { }
  ngOnInit(){

  }

  addAnswer(answer):Observable<any>{
    return this.http.post(server + "answers/",answer);


  }
  getAllAnswers():Observable<any>{
    return this.http.get(server + "answers/");
  }
  deleteAnswer(idAnswer:number):Observable<any>{
    return this.http.delete(server + "answers/"+idAnswer);
  }
  updateAnswer(answer,withBack: boolean = false):Observable<any>{
    return this.http.put(server + "answers/",answer);

  }

  getAnswerById(idAnswer): Observable<any>{
    return this.http.get(server + "answers/"+idAnswer);
  }


  callComponentMethod() {
    this.componentMethodCallSource.next();
  }
}
