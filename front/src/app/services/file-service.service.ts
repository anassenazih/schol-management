import { Injectable } from '@angular/core';
import {map} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {server} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class FileServiceService {


  private componentMethodCallSource = new Subject<any>();
  componentMethodCalled$ = this.componentMethodCallSource.asObservable();


  constructor(private http:HttpClient) { }
  ngOnInit() {
  }

    uploadFile(file): Observable<any>{
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('tokenJWT'));
        const httpOptions = {
            headers: headers_object
        };

        const formdata: FormData = new FormData();

      formdata.append('file', file);
    return this.http.post(server + "uploadFile/",formdata, httpOptions);
  }

  downloadFile(fileName):Observable<any>{
      var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('tokenJWT'));
      const httpOptions = {
          headers: headers_object
      };
  
    return this.http.get(server + "downloadFile/"+fileName, httpOptions);
  

  }

  uploadMultipleFiles(files):Observable<any>{
    return this.http.post(server + "uploadMultipleFiles",files);


  }

}
