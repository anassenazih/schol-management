export class Menu3Config {
    public defaults: any = {
        header: {
            self: {},
            items: [
                {
                    title: 'Accueil',
                    root: true,
                    icon: 'flaticon2-architecture-and-city',
                    page: '/teacher/dashboard',
                    translate: 'MENU.DASHBOARD',
                    bullet: 'dot',
                },

                {section: 'Forum'},
                {
                    title: 'Forums',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-squares-3',
                    submenu: [

                        {
                            title: 'forum',
                            page: '/teacher/forums/forum'
                        },

                    ]
                },
                {section: 'Emplois du temps'},
                {
                    title: 'Emplois du temps',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-event-calendar-symbol',
                    submenu: [

                        {
                            title: 'emploi',
                            page: '/teacher/emploi'
                        },

                    ]
                },
                {section: 'Notes'},
                {
                    title: 'Notes',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-edit-1',
                    submenu: [

                        {
                            title: 'notes',
                            page: '/teacher/notes'
                        },

                    ]
                },




            ]
        },
        aside: {
            self: {},
            items: [
                {
                    title: 'Accueil',
                    root: true,
                    icon: 'flaticon2-architecture-and-city',
                    page: '/teacher/dashboard',
                    translate: 'MENU.DASHBOARD',
                    bullet: 'dot',
                },


                {section: 'Forum'},
                {
                    title: 'Forum',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-squares-3',
                    page: '/teacher/forums/forum'
                },
                {
                    title: 'Emplois du temps',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-event-calendar-symbol',
                    page: '/teacher/emploi'
                },
                {
                    title: 'Notes',
                    root: true,
                    bullet: 'dot',
                    icon: 'flaticon-edit-1',
                    page: '/teacher/notes'
                },




            ]
        },

    };

    public get configs(): any {
        return this.defaults;
    }
}
