// Angular
import { Component, Input, OnInit } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
// State
import { AppState } from '../../../../../core/reducers';
import {User} from "../../../../../_models/user.model";
import {TokenStorageService} from "../../../../../services/token-storage.service";
import {Router} from "@angular/router";

@Component({
	selector: 'kt-user-profile2',
	templateUrl: './user-profile2.component.html',
})
export class UserProfile2Component implements OnInit {
	private roles: string[];
	isLoggedIn = false;
	showAdminBoard = false;
	showModeratorBoard = false;
	username: string;
	@Input() avatar = true;
	@Input() greeting = true;
	@Input() badge: boolean;
	@Input() icon: boolean;
	private firsName: string;
	private lastName: string;

	constructor(private tokenStorageService: TokenStorageService,
				private router : Router) { }

	ngOnInit() {
		this.isLoggedIn = !!this.tokenStorageService.getToken();

		if (this.isLoggedIn) {
			const user = this.tokenStorageService.getUser();
			this.roles = user.roles;

			this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
			this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

			this.username = user.username;
			this.firsName = user.firstName;
			this.lastName = user.lastName;
		}
	}

	logout() {
		this.tokenStorageService.signOut();
		this.router.navigateByUrl('/login');
	}
}
