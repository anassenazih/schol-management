import { Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs";
import {LayoutConfigService, MenuConfigService, PageConfigService} from "../core/_base/layout";
import {HtmlClassService} from "../views/theme/html-class.service";
import {Store} from "@ngrx/store";
import {AppState} from "../core/reducers";
import {NgxPermissionsService} from "ngx-permissions";
import {UserAuthServiceService} from "../services/user-auth-service.service";
import {LayoutConfig} from "../core/_config/layout.config";
import {Menu3Config} from "../teacher/config/menu3.config";
import {PageConfig} from "../core/_config/page.config";
import * as objectPath from "object-path";

@Component({
  selector: 'parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {

  content = '';
  // Public variables
  selfLayout: string;
  asideDisplay: boolean;
  asideSecondary: boolean;
  subheaderDisplay: boolean;
  desktopHeaderDisplay: boolean;
  fitTop: boolean;
  fluid: boolean;

  // Private properties
  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/



  constructor(
      private layoutConfigService: LayoutConfigService,
      private menuConfigService: MenuConfigService,
      private pageConfigService: PageConfigService,
      private htmlClassService: HtmlClassService,
      private store: Store<AppState>,
      private permissionsService: NgxPermissionsService,
      private userService: UserAuthServiceService) {

    // register configs by demos
    this.layoutConfigService.loadConfigs(new LayoutConfig().configs);
    this.menuConfigService.loadConfigs(new Menu3Config().configs);
    this.pageConfigService.loadConfigs(new PageConfig().configs);

    // setup element classes
    this.htmlClassService.setConfig(this.layoutConfigService.getConfig());

    const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(layoutConfig => {
      // reset body class based on global and page level layout config, refer to html-class.service.ts
      document.body.className = '';
      this.htmlClassService.setConfig(layoutConfig);
    });
    this.unsubscribe.push(subscr);
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit(): void {
    const config = this.layoutConfigService.getConfig();
    this.selfLayout = objectPath.get(config, 'self.layout');
    this.asideDisplay = objectPath.get(config, 'aside.self.display');
    this.subheaderDisplay = objectPath.get(config, 'subheader.display');
    this.desktopHeaderDisplay = objectPath.get(config, 'header.self.fixed.desktop');
    this.fitTop = objectPath.get(config, 'content.fit-top');
    this.fluid = objectPath.get(config, 'content.width') === 'fluid';

    // let the layout type change
    const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(cfg => {
      setTimeout(() => {
        this.selfLayout = objectPath.get(cfg, 'self.layout');
      });
    });
    this.unsubscribe.push(subscr);
  }

}
