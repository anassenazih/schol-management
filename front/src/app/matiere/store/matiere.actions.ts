import { createAction, props } from '@ngrx/store';

export const loadMatieres = createAction(
  '[Matiere] Load Matieres',
    props<{ request?: any }>()
);

export const loadMatieresSuccess = createAction(
  '[Matiere] Load Matieres Success',
  props<{ matiere: any }>()
);

export const loadMatieresFailure = createAction(
  '[Matiere] Load Matieres Failure',
  props<{ error: any }>()
);
export const addMatiere = createAction(
    '[Matiere] add matiere',
    props<{ request: any }>()
);
export const editMatiere = createAction(
    '[Matiere] edit matiere',
    props<{ request: any }>()
);

export const deleteMatiere = createAction(
    '[Matiere] delete matiere',
    props<{ id: any }>()
);
