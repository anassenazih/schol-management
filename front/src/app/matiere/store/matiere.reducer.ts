import { Action, createReducer, on } from '@ngrx/store';
import * as matiereActions from "../store/matiere.actions";


export const matiereFeatureKey = 'matiere';

export interface State {
  matiere: any;
  loaded: boolean;
}

export const initialState: State = {
  matiere: [],
   loaded: false,

};

const matiereReducer = createReducer(
  initialState,
    on(matiereActions.loadMatieres, (state,{request}) => ({
      ...state,
      request:request,
      loaded: false,
      loading: true
    })),
    on(matiereActions.loadMatieresSuccess, (state, {matiere}) => ({
      ...state,
      matiere: matiere,
      loaded: false,
      loading: false
    })),
    on(matiereActions.loadMatieresFailure, (state, {error}) => ({
      matiere: [],
      loaded: false,
      loading: false
    })),

);


export function matierereducer(state: State | undefined, action: Action) {
  return matiereReducer(state, action);
}
