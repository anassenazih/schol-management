import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from "rxjs/operators";
import {loadMatieresFailure,loadMatieresSuccess,loadMatieres} from "../store/matiere.actions";
import {of} from "rxjs/internal/observable/of";
import {SubjectServiceService} from "../../services/subject-service.service";



@Injectable()
export class MatiereEffects {



  constructor(private actions$: Actions,private subjectService:SubjectServiceService) {}
  subjectsList$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Matiere] Load Matieres'),
          exhaustMap((action: any) => {
                if (action.request) {
                  return this.subjectService.getAllSubjects().pipe(
                      map(matiere =>loadMatieresSuccess({matiere: JSON.parse(JSON.stringify(matiere))})),
                      catchError(error => of(loadMatieresFailure({error: error}))
                      )
                  )
                }
              }
          )));

  addGroup$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Matiere] add matiere'),
          exhaustMap((action: any) =>
              this.subjectService.addSubject(action.request).pipe(
                  map(matiere => {
                    return loadMatieres({request: {}})
                  })
              )
          )));

  removeTeam$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Matiere] delete matiere'),
          exhaustMap(action =>
              this.subjectService.deleteSubject(Object.assign(action).id).pipe(
                  map(matiere => {
                    return loadMatieres({request: {}})
                  })
              )
          )));
  editGroup$ = createEffect(() =>
      this.actions$.pipe(
          ofType('[Matiere] edit matiere'),
          exhaustMap(action =>
              this.subjectService.updateSubject(Object.assign(action).id).pipe(
                  map(matiere => {
                    return loadMatieres({request: {}})
                  })
              )
          )));

}
