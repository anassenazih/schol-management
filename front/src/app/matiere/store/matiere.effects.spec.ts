import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { MatiereEffects } from './matiere.effects';

describe('MatiereEffects', () => {
  let actions$: Observable<any>;
  let effects: MatiereEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MatiereEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<MatiereEffects>(MatiereEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
