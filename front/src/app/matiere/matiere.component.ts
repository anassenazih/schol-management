import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {LayoutUtilsService, MessageType} from "../core/_base/crud";
import {GroupServiceService} from "../services/group-service.service";
import {TokenStorageService} from "../services/token-storage.service";
import {Group} from "../_models/group.model";
import {EditGroupComponent} from "../group/edit-group/edit-group.component";
import {AddGroupComponent} from "../group/add-group/add-group.component";
import {SubjectServiceService} from "../services/subject-service.service";
import {EditMatiereComponent} from "./edit-matiere/edit-matiere.component";
import {AddMatiereComponent} from "./add-matiere/add-matiere.component";
import {Subject} from "../_models/Subject.model";
import {Store} from "@ngrx/store";
import {AppState} from "../core/reducers";
import {loadMatieres} from "./store/matiere.actions";
import {loadForumss} from "../forums/store/forums.actions";
import {loadGroups} from "../group/store/group.actions";

@Component({
  selector: 'matiere',
  templateUrl: './matiere.component.html',
  styleUrls: ['./matiere.component.scss']
})
export class MatiereComponent implements OnInit {


  // Table fields
  listData:MatTableDataSource<any>;
  schedules=[];
  displayedColumns = [ 'matiere', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  private searchKey: string;
  displayPdf = false;
  hasItems :boolean; // Need to show message: 'No records found'
  private file: any;
  private url: any;
  private role: any;
  private matiere=[];
  private allMat=[];
  constructor(		public dialog: MatDialog,
                      public snackBar: MatSnackBar,
                      private layoutUtilsService: LayoutUtilsService,
                      private subjectService : SubjectServiceService,
                      private store: Store<AppState>,
                      private tokenStorageService:TokenStorageService,
                      private cdr: ChangeDetectorRef) {
    this.subjectService.componentMethodCalled$.subscribe(
        () => {
          this.ngOnInit();
        }
    );
  }

  ngOnInit() {

    this.store.dispatch(loadMatieres({request: this.matiere}));
    this.store.select(state => state.matiere).subscribe(data => {
      this.schedules = JSON.parse(JSON.stringify(data.matiere));
      this.listData = new MatTableDataSource(this.schedules);
      this.listData.sort = this.sort;
      this.listData.paginator=this.paginator;
      this.cdr.markForCheck();
    });
    this.role = this.tokenStorageService.getUser().roles;
  }

  onSearchClear(){
    this.searchKey="";
    this.applyFilter();
  }
  applyFilter(){
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }



  editSubject(subject: Subject) {

    const _saveMessage = `Modifié avec succès.`;
    const _messageType =  MessageType.Update;
    const dialogRef = this.dialog.open(EditMatiereComponent, { data: { subjectId: subject.idSubject, name : subject.subjectName} });
    dialogRef.afterClosed().subscribe(res => {
      this.subjectService.callComponentMethod();
      if (!res) {
        return;
      }
      this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

    });

  }
  addSubject() 	{

    const _saveMessage = `Ajouté avec succès.`;
    const _messageType =  MessageType.Create;
    const dialogRef = this.dialog.open(AddMatiereComponent, { data: {  } });
    dialogRef.afterClosed().subscribe(res => {
      this.subjectService.callComponentMethod();
      if (!res) {
        return;
      }
      this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

    });

  }


  deleteSubject(idSubject: number){
    const _cours = 'Matière' ;
    const _description = 'Vous êtes sûr de vouloir supprimer cette matière ?';
    const _waitDesciption = 'Suppression de matière  ...';
    const _deleteMessage = `la matière a été supprimé`;

    const dialogRef = this.layoutUtilsService.deleteElement(_cours , _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {

      if (res) {
        this.subjectService.deleteSubject(idSubject).subscribe(data=>(this.ngOnInit()));
        this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
      }


    });
  }



}
