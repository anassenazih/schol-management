import {Component, Inject, OnInit} from '@angular/core';
import {Group} from "../../_models/group.model";
import {Observable, Subscription} from "rxjs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";
import {GroupServiceService} from "../../services/group-service.service";
import {Subject} from "../../_models/Subject.model";
import {SubjectServiceService} from "../../services/subject-service.service";

@Component({
  selector: 'edit-matiere',
  templateUrl: './edit-matiere.component.html',
  styleUrls: ['./edit-matiere.component.scss']
})
export class EditMatiereComponent implements OnInit {


  // Public properties
  subject: Subject;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  group$: Observable<any>;
  // Private properties
  private componentSubscriptions: Subscription;
  private editedGroup: Object;
  private editRole: any;

  /**
   * Component constructor
   *
   * @param dialogRef: MatDialogRef<RoleAddComponent>
   * @param data: any
   * @param store: Store<AppState>
   */
  constructor(public dialogRef: MatDialogRef<EditMatiereComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private subjectService: SubjectServiceService) {

  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    if (this.componentSubscriptions) {
      this.componentSubscriptions.unsubscribe();
    }
  }


  /**
   * Save data
   */
  onSubmit() {
    this.hasFormErrors = false;
    this.loadingAfterSubmit = false;
    /** check form */
    if (!this.isTitleValid()) {
      this.hasFormErrors = true;
      return;
    }
    this.subject = new Subject();
    this.subject.idSubject = this.data.subjectId;
    this.subject.subjectName = this.data.name;
    this.subjectService.updateSubject(this.subject).subscribe(data => {
      this.editedGroup = data;
      console.log('ediit', this.editedGroup);
      this.viewLoading = false;
      this.dialogRef.close({
        isEdit: true
      });
    }, error1 => {
      console.log(error1)
    });


  }


  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }


  /** UI */
  /**
   * Returns component title
   */
  getTitle(): string {

    return `Modifier subject '${this.data.name}'`;

  }

  /**
   * Returns is title valid
   */
  isTitleValid(): boolean {
    return (this.data.name && this.data.name.length > 0);
  }


}
