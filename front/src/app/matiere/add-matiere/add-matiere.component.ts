import {Component, Inject, OnInit} from '@angular/core';
import {Group} from "../../_models/group.model";
import {Observable, Subscription} from "rxjs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {AppState} from "../../core/reducers";
import {GroupServiceService} from "../../services/group-service.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject} from "../../_models/Subject.model";
import {SubjectServiceService} from "../../services/subject-service.service";

@Component({
  selector: 'add-matiere',
  templateUrl: './add-matiere.component.html',
  styleUrls: ['./add-matiere.component.scss']
})
export class AddMatiereComponent implements OnInit {


  // Public properties
  subject: Subject;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  subject$: Observable<any>;
  // Private properties
  private componentSubscriptions: Subscription;
  private addedGroup: any;


  /**
   * Component constructor
   *
   * @param dialogRef
   * @param data: any
   * @param store: Store<AppState>
   */
  constructor(public dialogRef: MatDialogRef<AddMatiereComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private subjectService : SubjectServiceService) {

  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {

  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    if (this.componentSubscriptions) {
      this.componentSubscriptions.unsubscribe();
    }
  }



  form : FormGroup = new FormGroup({
    idSubject: new FormControl(null),
    subjectName: new FormControl('',Validators.required),
  });

  /**
   * Save data
   */
  onSubmit() {
    if (this.form.valid) {
      this.subject = new Subject();
      this.subject.idSubject = null;
      this.subject.subjectName = this.form.get('subjectName').value;
      this.subjectService.addSubject(this.subject).subscribe(data => {
        this.addedGroup = data;
        this.viewLoading = false;
        this.dialogRef.close({});

      }, error1 => {
        console.log(error1)
      });
      this.form.reset();
    }

  }



  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }



  /** UI */
  /**
   * Returns component title
   */
  getTitle(): string {

    return `Ajouter matière '${this.data.subject}'`;

  }

  /**
   * Returns is title valid
   */
  isTitleValid(): boolean {
    return (this.data.subject && this.data.subject.length > 0);
  }


}
