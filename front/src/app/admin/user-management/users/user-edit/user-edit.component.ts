// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../core/reducers';
// Layout
import { SubheaderService, LayoutConfigService } from '../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../core/_base/crud';
// Services and Models

import {UserServiceService} from "../../../../services/user-service.service";
import {User} from "../../../../_models/user.model";

@Component({
	selector: 'kt-user-edit',
	templateUrl: './user-edit.component.html',
})
export class UserEditComponent implements OnInit{

// Public properties
	user: User;
	userId$: Observable<number>;
	oldUser: User;
	selectedTab = 0;
	loading$: Observable<boolean>;
	rolesSubject = new BehaviorSubject<string[]>([]);
	mdpSubject =  new BehaviorSubject<string>('');
	//addressSubject = new BehaviorSubject<Address>(new Address());
	//soicialNetworksSubject = new BehaviorSubject<SocialNetworks>(new SocialNetworks());
	userForm: FormGroup;
	hasFormErrors = false;
	// Private properties
	private subscriptions: Subscription[] = [];
	private editedUser: Object;

	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param userFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
				private userService: UserServiceService,
				private router: Router,
				private userFB: FormBuilder,
				private subheaderService: SubheaderService,
				private layoutUtilsService: LayoutUtilsService,
				private store: Store<AppState>,
				private layoutConfigService: LayoutConfigService) { }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {

		const routeSubscription =  this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			if (id && id > 0) {
				this.userService.getUserById(id).subscribe(res => {
					if (res) {
						this.user = res;
						this.rolesSubject.next(this.user.role);
						this.mdpSubject.next(this.user.password);
						this.oldUser = Object.assign({}, this.user);
						this.initUser();
					}
				});
			} else {
				this.user = new User();
				this.user.clear();
				this.rolesSubject.next(this.user.role);
				this.mdpSubject.next(this.user.password);
				this.oldUser = Object.assign({}, this.user);
				this.initUser();
			}
		});
		this.subscriptions.push(routeSubscription);
	}


	/**
	 * Init user
	 */
	initUser() {
		this.createForm();
		this.subheaderService.setTitle('Modifier un utilisateur');
		this.subheaderService.setBreadcrumbs([
			{ title: 'User Management', page: `user-management` },
			{ title: 'Users',  page: `user-management/users` },
			{ title: 'Edit user', page: `user-management/users/edit`, queryParams: { id: this.user.idUser } }
		]);
	}

	/**
	 * Create form
	 */
	createForm() {
		this.userForm = this.userFB.group({
			username: [this.user.username, Validators.required],
			firstName: [this.user.firstName, Validators.required],
			lastName: [this.user.lastName, Validators.required],
			email: [this.user.email, Validators.email],
			phoneNumber: [this.user.phoneNumber],
			address: [this.user.address],
			occupation: [this.user.occupation]
		});
	}

	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/user-management/users`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh user
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshUser(isNew: boolean = false, id = 0) {
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/user-management/users/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.user = Object.assign({}, this.oldUser);
		this.createForm();
		this.hasFormErrors = false;
		this.userForm.markAsPristine();
		this.userForm.markAsUntouched();
		this.userForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		this.hasFormErrors = false;
		const controls = this.userForm.controls;
		/** check form */
		if (this.userForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		let editedUser = this.prepareUser();
		console.log('edited',editedUser);
		if (editedUser.idUser > 0) {
			this.updateUser(editedUser, withBack);
			return;
		}
		this.addUser(editedUser, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareUser(): User {
		const controls = this.userForm.controls;
		const _user = new User();
		_user.clear();
		_user.role = this.rolesSubject.value;
		_user.password=this.mdpSubject.value;
		//_user.pic = this.user.pic;
		_user.idUser = this.user.idUser;
		_user.username = controls.username.value;
		_user.email = controls.email.value;
		_user.lastName = controls.lastName.value;
		_user.firstName = controls.firstName.value;
		_user.occupation = controls.occupation.value;
		_user.phoneNumber = controls.phoneNumber.value;
		_user.address = controls.address.value;
		console.log('rolleeeee', this.rolesSubject.value);
		return _user;
	}

	/**
	 * Add User
	 *
	 * @param _user: User
	 * @param withBack: boolean
	 */
	addUser(_user: User, withBack: boolean = false) {
		this.userService.addUser(_user).subscribe(data => {
			this.editedUser = data;
			console.log('add',this.editedUser);
			/*const addSubscription = this.store.pipe(select(selectLastCreatedUserId)).subscribe(newId => {
				const message = `New user successfully has been added.`;
				this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
				if (newId) {
					if (withBack) {
						this.goBackWithId();
					} else {
						this.refreshUser(true, newId);
					}
				}
			});
			this.subscriptions.push(addSubscription);*/
		}, error1 => {
			console.log(error1)
		});
	}

	updateUser(_user: User, withBack: boolean = false) {
		this.userService.updateUser(_user).subscribe(data => {
			this.editedUser = data;
			const message = `User successfully has been saved.`;
			this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
			if (withBack) {
				this.goBackWithId();
			} else {
				this.refreshUser(false);
			}
		}, error1 => {
			console.log(error1)
		});
	}


	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = 'Modifier l`utilisateur ';
		if (!this.user || !this.user.idUser) {
			return result;
		}
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}

}
