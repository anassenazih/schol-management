// Angular
import { Component, OnInit, Input } from '@angular/core';
// RxJS
import { BehaviorSubject, Observable } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
// Lodash
import { each, find, remove } from 'lodash';
// State
import { AppState } from '../../../../../core/reducers';
// Auth
import {RoleServiceService} from "../../../../../services/role-service.service";
import {Role} from "../../../../../_models/role.model";

@Component({
	selector: 'kt-user-roles-list',
	templateUrl: './user-roles-list.component.html'
})
export class UserRolesListComponent implements OnInit {
	// Public properties
	// Incoming data
	@Input() loadingSubject = new BehaviorSubject<boolean>(false);
	@Input() rolesSubject: BehaviorSubject<string[]>;

	// Roles
	allUserRoles$: Observable<Role[]>;
	allRoles: Role[] = [];
	unassignedRoles: Role[] = [];
	assignedRoles: Role[] = [];
	roleIdForAdding: number;
	public roles: Observable<Role[]>;
	role : any;

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 */
	constructor(private store: Store<AppState>,
				private rolesService:RoleServiceService) {}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.roles =this.rolesService.getAllRoles();
		console.log('allRole',this.roles);
	}

	/**
	 * Assign role
	 */
	assignRole() {
console.log('assign');
console.log('this.roleIdForAdding');
		if (this.roleIdForAdding === 0) {
			return;
		}

		this.rolesService.getRoleById(this.roleIdForAdding ).subscribe(data=>{
			this.role=data;
			console.log('roleId',this.role);
			if (this.role) {
				console.log('okk');console.log(this.role.role);
				this.assignedRoles.push(this.role.role);
				remove(this.unassignedRoles, (el) => el.idRole === this.role.idRole);
				this.roleIdForAdding = 0;
				this.updateRoles();
				console.log('Assroles',this.assignedRoles);
			}
		});

	}

	/**
	 * Unassign role
	 *
	 * @param role: Role
	 */
	unassingRole(role: Role) {
		this.roleIdForAdding = 0;
		this.unassignedRoles.push(role);
		remove(this.assignedRoles, el => el.idRole === role.idRole);
		this.updateRoles();
	}

	/**
	 * Update roles
	 */
	updateRoles() {
		const _roles = [];
		each(this.assignedRoles, elem => _roles.push(elem));
		this.rolesSubject.next(_roles);

	}
}
