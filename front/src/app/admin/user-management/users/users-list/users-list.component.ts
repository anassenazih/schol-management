import { AfterViewInit, AfterViewChecked } from '@angular/core';
// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatSnackBar } from '@angular/material';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import { fromEvent, merge, Observable, of, Subscription } from 'rxjs';
// LODASH
import { each, find } from 'lodash';
// NGRX
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';

// Services
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../../core/_base/crud';
// Models

import { SubheaderService } from '../../../../core/_base/layout';
import {UserServiceService} from "../../../../services/user-service.service";
import {MatTableDataSource} from "@angular/material/table";
import {RoleServiceService} from "../../../../services/role-service.service";
import {Question} from "../../../../_models/question.model";
import {User} from "../../../../_models/user.model";
import {Role} from "../../../../_models/role.model";


// Table with EDIT item in MODAL
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
	selector: 'kt-users-list',
	templateUrl: './users-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersListComponent implements OnInit, OnDestroy {
	// Table fields
	listData: MatTableDataSource<any>;
	displayedColumns = ['select', 'idUser', 'username', 'email', 'lastName', 'firstName' , 'phoneNumber','roles','actions'];
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild('sort1', {static: true}) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
	lastQuery: QueryParamsModel;
	// Selection
	selection = new SelectionModel<User>(true, []);
	usersResult: User[] = [];
	allRoles: Role[] = [];
	hasItems = false;
	searchKey: string;
	// Subscriptions
	private subscriptions: Subscription[] = [];
	private users: User[];
	private roles: Observable<Role[]>;
	private devicesJson: Object;
	private titles: string[] = [];


	/**
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param store: Store<AppState>
	 * @param router: Router
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param subheaderService: SubheaderService
	 */
	constructor(
		private userService : UserServiceService,
		private roleService : RoleServiceService,
		private activatedRoute: ActivatedRoute,
		private store: Store<AppState>,
		private router: Router,
		private layoutUtilsService: LayoutUtilsService,
		private subheaderService: SubheaderService,
		private cdr: ChangeDetectorRef) {
		//Pour raffraichir le tableau dans lautre component
		this.userService.componentMethodCalled$.subscribe(
			() => {
				this.ngOnInit();
			}
		);
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.listData!= null){
			this.hasItems = true ;
		}
		this.userService.getAllUsers().subscribe(data=> {this.users=data;
			this.listData = new MatTableDataSource(this.users);
			this.listData.sort = this.sort;
			this.listData.paginator=this.paginator;})
		// load roles list
		this.roleService.getAllRoles().subscribe(data=> {this.allRoles=data;});

/*		const rolesSubscription = this.store.pipe(select(selectAllRoles)).subscribe(res => this.allRoles = res);
		this.subscriptions.push(rolesSubscription);*/

		// If the user changes the sort order, reset back to the first page.
		const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
		this.subscriptions.push(sortSubscription);





		// Set title to page breadCrumbs
		this.subheaderService.setTitle('User management');


	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	/**
	 * Load users list
	 */
/*	loadUsersList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.selection.clear();
	}*/

	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;

		filter.lastName = searchText;

		filter.username = searchText;
		filter.email = searchText;
		filter.fillname = searchText;
		return filter;
	}

	/** ACTIONS */
	/**
	 * Delete user
	 *
	 * @param _item: User
	 */
	deleteUser(id) {
		const _title = 'Supprimer un utilisateur';
		const _description = 'Vous êtes sur de vouloir supprimer cet utilisateur?';
		const _waitDesciption = 'Suppression ...';
		const _deleteMessage = `l'utilisateur a été supprimé`;

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);

		dialogRef.afterClosed().subscribe(res => {

			if (res) {
				this.userService.deleteUser(id).subscribe(data=>(this.ngOnInit()));
				this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
			}


		});
	}

	onSearchClear(){
		this.searchKey="";
		this.applyFilter();
	}
	applyFilter(){
		this.listData.filter = this.searchKey.trim().toLowerCase();
	}
	/**
	 * Fetch selected rows
	 */
	fetchUsers() {
		const messages = [];
		this.selection.selected.forEach(elem => {
			messages.push({
				text: `${elem.lastName}, ${elem.email}`,
				id: elem.idUser.toString(),
				status: elem.username
			});
		});
		this.layoutUtilsService.fetchElements(messages);
	}

	/**
	 * Check all rows are selected
	 */
	isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.usersResult.length;
		return numSelected === numRows;
	}

	/**
	 * Toggle selection
	 */
	masterToggle() {
		if (this.selection.selected.length === this.usersResult.length) {
			this.selection.clear();
		} else {
			this.usersResult.forEach(row => this.selection.select(row));
		}
	}

	/* UI */
	private objArray: any;
	/**
	 * Returns user roles string
	 *
	 * @param user: User
	 */
	getUserRolesStr(id) {
		console.log('roles1', this.roles);
		console.log('id', id);
		console.log('role by id');
		this.roles=this.userService.getRolesByUserId(id);
			console.log('roles2', this.roles);


	}


	/**
	 * Redirect to edit page
	 *
	 * @param id
	 */
	editUser(id) {
		this.router.navigate(['../users/edit', id], { relativeTo: this.activatedRoute });
	}


}
