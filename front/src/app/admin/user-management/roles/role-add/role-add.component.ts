import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Observable, of, Subscription} from "rxjs";
import {select, Store} from "@ngrx/store";
import {AppState} from "../../../../core/reducers";
import {RoleServiceService} from "../../../../services/role-service.service";
import {each, find, some} from "lodash";
import {Update} from "@ngrx/entity";
import {delay} from "rxjs/operators";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Role} from "../../../../_models/role.model";


@Component({
  selector: 'role-add',
  templateUrl: './role-add.component.html',
  styleUrls: ['./role-add.component.scss'],
	changeDetection: ChangeDetectionStrategy.Default,
})
export class RoleAddComponent implements OnInit, OnDestroy {

	// Public properties
	role: Role;
	hasFormErrors = false;
	viewLoading = false;
	loadingAfterSubmit = false;
	role$: Observable<any>;
	// Private properties
	private componentSubscriptions: Subscription;
	private editedRole: Object;
	private editRole: any;

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<RoleAddComponent>
	 * @param data: any
	 * @param store: Store<AppState>
	 */
	constructor(public dialogRef: MatDialogRef<RoleAddComponent>,
				@Inject(MAT_DIALOG_DATA) public data: any,
				private store: Store<AppState>,
				private roleService : RoleServiceService) {

	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.roleService.getAllRoles();
	/*		console.log('id',this.data.roleId);
			if (this.data.roleId) {
				this.roleService.getRoleById(this.data.roleId).subscribe(res => {
					this.editRole = res;
					console.log('editRole',this.editRole);
				});


	}*/

	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
		if (this.componentSubscriptions) {
			this.componentSubscriptions.unsubscribe();
		}
	}




	/**
	 * Save data
	 */
	onSubmit() {
		this.hasFormErrors = false;
		this.loadingAfterSubmit = false;
		/** check form */
		if (!this.isTitleValid()) {
			this.hasFormErrors = true;
			return;
		}
		this.role=new Role();
		this.role.idRole=this.data.roleId;
		this.role.role= this.data.role;
		this.roleService.updateRole(this.role).subscribe(data => {
				this.editedRole = data;
			    this.viewLoading = false;
				this.dialogRef.close({
					isEdit: true
				});
				this.roleService.callComponentMethod();
			}, error1 => {
				console.log(error1)
			});


	}





	/**
	 * Close alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}



	/** UI */
	/**
	 * Returns component title
	 */
	getTitle(): string {

			return `Modifier role '${this.data.role}'`;

	}

	/**
	 * Returns is title valid
	 */
	isTitleValid(): boolean {
		return (this.data.role && this.data.role.length > 0);
	}
}
