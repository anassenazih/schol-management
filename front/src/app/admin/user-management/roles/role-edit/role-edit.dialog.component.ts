// Angular
import { Component, OnInit, Inject, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
// RxJS
import { Observable, of, Subscription} from 'rxjs';
// Lodash
import { each, find, some } from 'lodash';
// NGRX
import { Update } from '@ngrx/entity';
import { Store, select } from '@ngrx/store';
// State

// Services and Models

import { delay } from 'rxjs/operators';
import {AppState} from '../../../../core/reducers';
import {LayoutUtilsService, MessageType} from "../../../../core/_base/crud";
import {RoleServiceService} from "../../../../services/role-service.service";
import {LayoutConfigService} from "../../../../core/_base/layout";
import {FormGroup} from "@angular/forms";
import {Role} from "../../../../_models/role.model";

@Component({
	selector: 'kt-role-edit-dialog',
	templateUrl: './role-edit.dialog.component.html',
	changeDetection: ChangeDetectionStrategy.Default,
})
export class RoleEditDialogComponent implements OnInit, OnDestroy {
	// Public properties
	role: Role;
	role$: Observable<Role>;
	hasFormErrors = false;
	viewLoading = false;
	loadingAfterSubmit = false;
	// Private properties
	private componentSubscriptions: Subscription;
	private editedRole: Object;

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<RoleEditDialogComponent>
	 * @param data: any
	 * @param store: Store<AppState>
	 */
	constructor(public dialogRef: MatDialogRef<RoleEditDialogComponent>,
				@Inject(MAT_DIALOG_DATA) public data: any,
				private store: Store<AppState>,
				private roleService : RoleServiceService) { }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.roleService.getAllRoles();
		if (this.data.roleId) {
			this.role$ = this.roleService.getRoleById(this.data.roleId);
		} else {
			const newRole = new Role();
			newRole.clear();
			this.role$ = of(newRole);
		}

		this.role$.subscribe(res => {
			if (!res) {
				return;
			}

			this.role = new Role();
			this.role.idRole = res.idRole;
			this.role.role = res.role;

			//this.allPermissions$ = this.store.pipe(select(selectAllPermissions));
			//this.loadPermissions();
		});
	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
		if (this.componentSubscriptions) {
			this.componentSubscriptions.unsubscribe();
		}
	}


	/**
	 * Returns role for save
	 */
	prepareRole(): Role {
		const _role = new Role();
		_role.idRole = this.role.idRole;
		//	_role.permissions = this.preparePermissionIds();
		// each(this.assignedRoles, (_role: Role) => _user.roles.push(_role.id));
		_role.role = this.role.role;
		//_role.isCoreRole = this.role.isCoreRole;
		return _role;
	}

	/**
	 * Save data
	 */
	onSubmit() {
		this.hasFormErrors = false;
		this.loadingAfterSubmit = false;
		/** check form */
		if (!this.isTitleValid()) {
			this.hasFormErrors = true;
			return;
		}

		const editedRole = this.prepareRole();
		if (editedRole.idRole > 0) {
			this.roleService.updateRole(editedRole).subscribe(data => {
				this.editedRole = data;
				this.viewLoading = false;
				this.dialogRef.close({
					editedRole,
					isEdit: true
				});
				this.roleService.callComponentMethod();
			}, error1 => {
				console.log(error1)
			});
		} else {
			//this.createRole(editedRole);
			this.roleService.addRole(editedRole).subscribe(data => {
				this.editedRole = data;
				this.viewLoading = false;
				this.roleService.callComponentMethod();
				this.dialogRef.close({
					editedRole,
					isEdit: false
				});
			}, error1 => {
				console.log(error1)
			});


		}
	}



	/**
	 * Close alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}



	/** UI */
	/**
	 * Returns component title
	 */
	getTitle(): string {
		if (this.role && this.role.idRole) {
			// tslint:disable-next-line:no-string-throw
			return `Modifier role '${this.role.role}'`;
		}



		// tslint:disable-next-line:no-string-throw
		return 'Nouveau role';
	}

	/**
	 * Returns is title valid
	 */
	isTitleValid(): boolean {
		return (this.role && this.role.role && this.role.role.length > 0);
	}
}
