// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import { fromEvent, merge, Observable, of, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
// NGRX
import { Store } from '@ngrx/store';
// Services
import { LayoutUtilsService, MessageType } from '../../../../core/_base/crud';
// Models
import { AppState } from '../../../../core/reducers';
import { QueryParamsModel } from '../../../../core/_base/crud';

// Components
import { RoleEditDialogComponent } from '../role-edit/role-edit.dialog.component';
import {RoleServiceService} from "../../../../services/role-service.service";
import {RoleAddComponent} from "../role-add/role-add.component";
import {MatTableDataSource} from "@angular/material/table";
import {Role} from "../../../../_models/role.model";

// Table with EDIT item in MODAL
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
	selector: 'kt-roles-list',
	templateUrl: './roles-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class RolesListComponent implements OnInit, OnDestroy {
	// Table fields
	listData: MatTableDataSource<any>;
	displayedColumns = ['select' , 'idRole', 'role', 'actions'];
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
	// Selection
	selection = new SelectionModel<Role>(true, []);
	rolesResult: Role[] = [];
	searchKey: string;
	// Subscriptions
	private subscriptions: Subscription[] = [];
	roles = [];
	hasItems :boolean; // Need to show message: 'No records found'

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 * @param dialog: MatDialog
	 * @param snackBar: MatSnackBar
	 * @param layoutUtilsService: LayoutUtilsService
	 */
	constructor(
		private roleService : RoleServiceService,
		public dialog: MatDialog,
		public snackBar: MatSnackBar,
		private layoutUtilsService: LayoutUtilsService) {
		//Pour raffraichir le tableau dans lautre component
		this.roleService.componentMethodCalled$.subscribe(
			() => {
				this.ngOnInit();
			}
		);
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {

		if(this.listData!= null){
			this.hasItems = true ;
		}
		this.roleService.getAllRoles().subscribe(data=> {this.roles=data;
			this.listData = new MatTableDataSource(this.roles);
			this.listData.sort = this.sort;
			this.listData.paginator=this.paginator;
		console.log(this.roles);
		})



	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}


	/**
	 * Returns object for filter
	 */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter.role = searchText;
		return filter;
	}

	/** ACTIONS */
	/**
	 * Delete role
	 *
	 * @param _item: Role
	 */
	deleteRole(id) {
		const _role = 'Role de l`utilisateur' ;
		const _description = 'Vous êtes sûr de vouloir supprimer ce role?';
		const _waitDesciption = 'Suppression de role ...';
		const _deleteMessage = `Role a été supprimé`;

		const dialogRef = this.layoutUtilsService.deleteElement(_role, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {

			if (res) {
				this.roleService.deleteRole(id).subscribe(data=>(this.ngOnInit()));
				this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
			}


		});
	}

	/** Fetch */
	/**
	 * Fetch selected rows
	 */
	fetchRoles() {
		const messages = [];
		this.selection.selected.forEach(elem => {
			messages.push({
				text: `${elem.role}`,
				id: elem.idRole.toString(),
				// status: elem.username
			});
		});
		this.layoutUtilsService.fetchElements(messages);
	}

	onSearchClear(){
		this.searchKey="";
		this.applyFilter();
	}
	applyFilter(){
		this.listData.filter = this.searchKey.trim().toLowerCase();
	}


	/**
	 * Add role
	 */
	addRole() {
		const newRole = new Role();
		newRole.clear(); // Set all defaults fields
		const _saveMessage = `Ajouté avec succès.`;
		const _messageType =  MessageType.Create;
		const dialogRef = this.dialog.open(RoleEditDialogComponent, { data: { roleId: newRole.idRole } });
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

		});
	}

	/**
	 * Edit role
	 *
	 * @param role: Role
	 */
	editRole(role: Role) {
		const _saveMessage = `Modifié avec succès .`;
		const _messageType = MessageType.Update ;
		const dialogRef = this.dialog.open(RoleAddComponent, { data: { roleId: role.idRole, role : role.role}});
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 10000, true, true);

		});
	}


	/**
	 * Check all rows are selected
	 */
	isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.rolesResult.length;
		return numSelected === numRows;
	}

	/**
	 * Toggle selection
	 */
	masterToggle() {
		if (this.selection.selected.length === this.rolesResult.length) {
			this.selection.clear();
		} else {
			this.rolesResult.forEach(row => this.selection.select(row));
		}
	}
}
