export class Menu2Config {
	public defaults: any = {
		header: {
			self: {},
			items: [
				{
					title: 'Accueil',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: '/admin/dashboard',
					translate: 'MENU.DASHBOARD',
					bullet: 'dot',
				},

				{section: 'Comptes'},
				{
					title: 'Comptes',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-user-outline-symbol',
					submenu: [


						{
							title: 'Utilisateurs',
							page: '/admin/user-management/users'
						},
						{
							title: 'Roles',
							page: '/admin/user-management/roles'
						}
					]
				},
				{section: 'Forum'},
				{
					title: 'Forums',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-squares-3',
					submenu: [

						{
							title: 'forum',
							page: '/admin/forums/forum'
						},

					]
				},
				{section: 'Emplois du temps'},
				{
					title: 'Emplois du temps',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-event-calendar-symbol',
					submenu: [

						{
							title: 'emploi',
							page: '/admin/emploi'
						},

					]
				},
				{section: 'Notes'},
				{
					title: 'Notes',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-edit-1',
					submenu: [

						{
							title: 'notes',
							page: '/admin/notes'
						},

					]
				},
				{section: 'Groupes'},
				{
					title: 'Groupes',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-group',
					submenu: [

						{
							title: 'group',
							page: '/admin/group'
						},

					]
				},
				{section: 'Matières'},
				{
					title: 'Matières',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-edit-1',
					submenu: [

						{
							title: 'matiere',
							page: '/admin/matiere'
						},

					]
				},




			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Accueil',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: '/admin/dashboard',
					translate: 'MENU.DASHBOARD',
					bullet: 'dot',
				},

				{section: 'Comptes'},
				{
					title: 'Comptes',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-user-outline-symbol',
					submenu: [


						{
							title: 'Utilisateurs',
							page: '/admin/user-management/users'
						},
						{
							title: 'Roles',
							page: '/admin/user-management/roles'
						}
					]
				},

				{section: 'Forum'},
				{
					title: 'Forum',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-squares-3',
					page: '/admin/forums/forum'
				},
				{
					title: 'Emplois du temps',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-event-calendar-symbol',
					page: '/admin/emploi'
				},
				{
					title: 'Notes',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-edit-1',
					page: '/admin/notes'
				},
				{
					title: 'Groupes',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-group',
					page: '/admin/group'
				},
				{
					title: 'Matières',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-files-and-folders',
					page: '/admin/matiere'
				},




			]
		},

	};

	public get configs(): any {
		return this.defaults;
	}
}
