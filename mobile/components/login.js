import React from "react";
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TextInput,
  Animated,
  Dimensions,
  TouchableOpacity,
  Image
} from "react-native";
import {getLevels} from '../API/LevelAPI';

class Login extends React.Component{
  constructor(props){
    super(props);
    this.state={
      typing_email: false,
      typing_password: false,
      animation_login : new Animated.Value(width-40),
      enable:true
    }
  }

  _loadLevels(){
    getLevels().then(data => console.log(data))
  }

  render(){
    const width = this.state.animation_login;
    return(
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
          <View style={styles.header}>
              <Image 
                style={styles.logo}
                source={require("../logo.png")}
               />
          </View>
          <View style={styles.footer}>
                <Text style={[styles.title,{
                  marginTop:50
                }]}>E-mail</Text>
                <View style={styles.action}>
                    <TextInput 
                      placeholder="Your email.."
                      style={styles.textInput}
                    />
                </View>

                <Text style={[styles.title,{
                  marginTop:20
                }]}>Password</Text>
                <View style={styles.action}>
                    <TextInput 
                      secureTextEntry
                      placeholder="Your password.."
                      style={styles.textInput}
                    />
                </View>
                
                <TouchableOpacity
                onPress={()=>this._loadLevels()}>
                  <View style={styles.button_container}>
                        <Animated.View style={[styles.animation,{
                          width
                        }]}>
                            <Text style={styles.textLogin}>Login</Text>
                        </Animated.View >
                  </View>
                </TouchableOpacity>

                <View style={styles.signUp}>
                      <Text style={{color:'#2d5428'}}> Sign up?</Text>
                </View>
          </View>
      </View>
    )
  }
}

const width = Dimensions.get("screen").width;

var styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'white',
    justifyContent:'center'
  },
  header: {
    paddingTop:60,
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,
  },
  logo: {
    width: 120,
    height: 70,
},
  footer: {
    flex:2,
    padding:20
  },
  title: {
    color:'black',
    fontWeight:'bold'
  },
  action: {
    flexDirection:'row',
    borderBottomWidth:1,
    borderBottomColor:'#2d5428'
  },
  textInput: {
    flex:1,
    marginTop:5,
    paddingBottom:5,
    color:'gray'
  },
  button_container: {
    alignItems: 'center',
    justifyContent:'center'
  },
  animation: {
    backgroundColor:'#2d5428',
    paddingVertical:10,
    marginTop:30,
    borderRadius:100,
    justifyContent:'center',
    alignItems:'center'
  },
  textLogin: {
    color:'white',
    fontWeight:'bold',
    fontSize:18
  },
  signUp: {
    flexDirection:'row',
    justifyContent:'center',
    marginTop:20
  }
});

export default Login