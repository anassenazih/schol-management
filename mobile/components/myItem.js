import React from 'react';
import { StyleSheet, View, Text} from 'react-native';
import {
    CardThree,CardSeven
  } from "react-native-card-ui";
class MyItem extends React.Component {
   render(){
    //const course=this.props.course
    return (
        <CardSeven
        title={"title"}
        subTitle={"sub tile "}
        image={{
          uri:
            "https://pluspng.com/img-png/png-lesson-plan-free-lesson-plan-300.png"
        }}
        icon1={"share"}
        iconColor1={"#fff"}
        iconBackground1={"purple"}
        onClicked1={() => {
          alert("Hello!");
        }}
        icon2={"heart"}
        iconColor2={"#fff"}
        iconBackground2={"red"}
        onClicked2={() => {
          alert("Hello!");
        }}
      />
      );
   }
}

export default MyItem