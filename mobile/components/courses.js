import React from 'react';
import { StyleSheet, View, FlatList, ActivityIndicator} from 'react-native';
import MyItem from './myItem';
import {getLevels} from '../API/LevelAPI';
class Courses extends React.Component{
    constructor(props){
        super(props)
        this.state = { 
            data: [],
            isLoading: false
      }
        
    }
    /*componentWillMount(){
        this.setState({data : inputObject});
    }*/
    _loadLevels(){
        getLevels().then(data => console.log(data))
      }
   
  
   
    render(){
        console.log(this.state.isLoading)
        return (
            <View style={styles.main_style}>
             <FlatList 
             data={[{key: 'a'} , {key: 'b'},{key: 'c'} , {key: 'd'},{key: 'e'} , {key: 'f'},{key: 'g'} , {key: 'h'}]}
             renderItem={({item})=> <MyItem onPress={()=>this._loadLevels()}/>}
             />
             {/*this._displayLoading()*/}
            </View>
          )
    }
}

const styles = StyleSheet.create({
    main_style:{
        marginTop: 20,
        flex:1
    },
   
  });

export default Courses
